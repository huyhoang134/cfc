var copie_ext_cible_structure="";
var copie_ext_cible_structure_default="";
jQuery(document).ready(function() {
	

	
	init_copie_externe_cible_autocomplete();
	close_copie_ext_cible()
	display_default_add_copie_externe_cible_button();

	if($('.copie_externe_cible').length){
        $('.dob').keyup(function(e) {
            var fieldval=$(this).val();
            val= fieldval.replace(/[^0-9\.]/g, '');
            var arrChar = val.split('');
            var dob="";
            for (var i = 0; i < arrChar.length; i++) {
                dob +=arrChar[i];
                if(i==1 || i==3 ){
                    dob +="/";
                }
            }
            $(this).val(dob);
        });

	}

    setTimeout(function () {
    	copie_ext_cible_structure_default=$('#container_copie_ext_cible > div').eq($('#container_copie_ext_cible > div').length-1).clone();
    }, 1000);

	
	$('form').submit(function( event ) {
		if($('.copie_externe_cible').length){

			//si formulaire vide
			if($('.copie_externe_cible').length<2){
				var data_index=0;
				var copie_publication=$('.copie_externe_cible').eq(data_index).find('.copie_externe_publication').eq(0).val().trim();
				var copie_titre=$('.copie_externe_cible').eq(data_index).find('.copie_externe_titre').eq(0).val().trim();
				var copie_auteur=$('.copie_externe_cible').eq(data_index).find('.copie_externe_auteur').eq(0).val().trim();
				var copie_parution=$('.copie_externe_cible').eq(data_index).find('.copie_externe_parution').eq(0).val().trim();
				
				if(copie_publication.length==0 && copie_titre.length==0 && copie_auteur.length==0 && copie_parution.length==0){
					if(confirm("Vous n'avez pas déclaré de publications. Confirmez vous cette déclaration ?"))
					{
						$('.copie_externe_cible').eq(0).remove();
						if($('#container_copie_ext_cible > div').length<1){
							$("#default_panorama_press_button").show();
						}else{
							$("#default_panorama_press_button").hide();
						}
						
					}
				}
	
			
			}
			
			$('.copie_externe_cible').each(function(){
				var data_index=$(this).data('index');
				$(this).find('.copie_externe_publication').attr('name','wcpextc['+data_index+'][publication]');
				$(this).find('.copie_externe_titre').attr('name','wcpextc['+data_index+'][titre]');
				$(this).find('.copie_externe_auteur').attr('name','wcpextc['+data_index+'][auteur]');
				$(this).find('.copie_externe_parution').attr('name','wcpextc['+data_index+'][parution]');
				$(this).find('.copie_externe_destinataires').attr('name','wcpextc['+data_index+'][destinataires]');
				
				var copie_publication=$('.copie_externe_cible').eq(data_index).find('.copie_externe_publication').eq(0).val().trim();
				var copie_titre=$('.copie_externe_cible').eq(data_index).find('.copie_externe_titre').eq(0).val().trim();
				var copie_auteur=$('.copie_externe_cible').eq(data_index).find('.copie_externe_auteur').eq(0).val().trim();
				var copie_parution=$('.copie_externe_cible').eq(data_index).find('.copie_externe_parution').eq(0).val().trim();
				var copie_destinataire=$('.copie_externe_cible').eq(data_index).find('.copie_externe_destinataires').eq(0).val().trim();
				
				$('.copie_externe_cible[data-index='+data_index+'] .error').html('');
	
				
				if(copie_publication.length==0){
					$('.copie_externe_cible[data-index='+data_index+'] .error').html("Merci de renseigner le titre de la publication ou de supprimer la ligne.");
	
					event.preventDefault();
					return;
				}else if(copie_titre.length==0){
						$('.copie_externe_cible[data-index='+data_index+'] .error').html("Merci de renseigner le titre de l'article ou de supprimer la ligne.");
						event.preventDefault();
				}else if(copie_auteur.length==0){
					$('.copie_externe_cible[data-index='+data_index+'] .error').html("Merci de renseigner l'auteur de l'article ou de supprimer la ligne.");
					event.preventDefault();
				}else if(copie_parution.length==0){
					$('.copie_externe_cible[data-index='+data_index+'] .error').html("Merci de renseigner la date de parution de l'article ou de supprimer la ligne.");
					event.preventDefault();
				}
				
	
				/*$(this).find('.copie_externe_publications > div').each(function(i,k){
					$(this).find('.wpplp_label').attr('name','wpanoramapress_liste['+data_index+'][publication]['+i+'][wpplp_label]');
					$(this).find('.wpplp_total').attr('name','wpanoramapress_liste['+data_index+'][publication]['+i+'][wpplp_total]');
	
	
				});*/
			});
		}
		/*$('.copie_externe').each(function(){
			var data_index=$(this).data('index');
			console.log($(this).find('.copie_externe_publication > div').length);
			console.log($(this).find('.copie_externe_titre').val().length);
			console.log($(this).find('.copie_externe_auteur').val().length);
			console.log($(this).find('.copie_externe_parution').val().length);
			if($(this).find('.copie_externe_publication > div').length < 1 && ( $(this).find('.copie_externe_titre').val().length < 1 && $(this).find('.copie_externe_auteur').val().length <1))
			{
				$(this).find('.error').html("Vous devez reseigner au moins une publication ou charger un fichier pour ce panorama, ou supprimer celui-ci.");
				event.preventDefault();
			}
		});*/
		

		
	});
	
	/*$('.copie_externe').each(function(){
		var data_index=$(this).data('index');
		if($(this).find('.wppl_fichier_media_key').length){
			if($(this).find('.copie_externe_publications > div').length < 1 && $(this).find('.wppl_fichier_media_key').val().length>32)
			{
				$(this).find('.custom-file-upload-filename-text').val($(this).find('.wppl_fichier_name').val());
				$('.copie_externe[data-index='+data_index+'] .row-add-first-publication-press').show();
				$(this).find('.delete_file').show();
			}
		}

	});*/
	//console.log(copie_ext_cible_structure_default);
});

function add_default_copie_externe_cible()
{
	var clone=$(copie_ext_cible_structure_default).clone(true, true);
	$('#container_copie_ext_cible').append(clone);
	display_default_add_copie_externe_cible_button()
	init_copie_externe_cible_autocomplete();
}

function init_copie_externe_cible_autocomplete(){
	$('.copie_externe_publication').devbridgeAutocomplete({
		serviceUrl: '/api-repertoire-cne-presse-ciblees/?',
	    onSelect: function (suggestion) {}
	});
	

	display_default_add_copie_externe_cible_button()
}


function display_default_add_copie_externe_cible_button()
{

	if($('.copie_externe_cible').length){
		$('#default_copie_externe_cible_button').hide();
	}else{
		$('#default_copie_externe_cible_button').show();
	}
}


function close_copie_ext_cible()
{
	return;
	$('.copie_externe_cible .panorama_content:visible').each(function(){
		 if($(this).closest('.copie_externe_cible').find('.panorama_content').is(":visible")){
		    	$(this).closest('.copie_externe_cible').find(".ico-expend-collapse").removeClass('ico-expend');
		    	$(this).closest('.copie_externe_cible').find(".ico-expend-collapse").addClass('ico-collapse');
		    }else{
		    	$(this).closest('.copie_externe_cible').find(".ico-expend-collapse").removeClass('ico-collapse');
		    	$(this).closest('.copie_externe_cible').find(".ico-expend-collapse").addClass('ico-expend');
		    }
		$(this).closest('.copie_externe_cible').find('.panorama_content').toggle("blind", {},500, function() {});
	});
}

function toggle_copie_externe_cible(item){
	

	//close_copie_ext_web_reaseau();

	if($(item).closest('.copie_externe_cible').find('.copie_externe_content').is(":visible")==false){
		
		if($(item).closest('.copie_externe_cible').find('.copie_externe_content').is(":visible")){
	    	$(item).parent().removeClass('ico-expend');
	    	$(item).parent().addClass('ico-collapse');
	    }else{
	    	$(item).parent().removeClass('ico-collapse');
	    	$(item).parent().addClass('ico-expend');
	    }
		
		$(item).closest('.copie_externe_cible').find('.copie_externe_content').toggle("blind", {},500, function() {
		    
		    if($(item).closest('.copie_externe_cible').find('input:invalid').length)
		    {
		    	$( $(item).closest('.copie_externe_cible').find('input:invalid').eq(0) )[0].scrollIntoView(); 
		    }
		    
		});
	}else{
		$(item).parent().removeClass('ico-expend');
    	$(item).parent().addClass('ico-collapse');
    	$(item).closest('.copie_externe_cible').find('.copie_externe_content').toggle("blind", {},500, function() {
		    
		    if($(item).closest('.copie_externe_cible').find('input:invalid').length)
		    {
		    	$( $(item).closest('.copie_externe_cible').find('input:invalid').eq(0) )[0].scrollIntoView(); 
		    }
		    
		});
	}



}
function add_copie_externe_cible(item){
	
	var copie_index=$(item).closest('.copie_externe_cible').data('index');

	var copie_publication=$(item).closest('.copie_externe_cible').find('.copie_externe_publication').eq(0).val();
	var copie_titre=$(item).closest('.copie_externe_cible').find('.copie_externe_titre').eq(0).val();
	var copie_auteur=$(item).closest('.copie_externe_cible').find('.copie_externe_auteur').eq(0).val();
	var copie_parution=$(item).closest('.copie_externe_cible').find('.copie_externe_parution').eq(0).val();

	if(copie_publication==""){
		$('.copie_externe_cible[data-index='+copie_index+'] .error').html("Merci de renseigner le titre de la publication ou de supprimer la ligne.");
	}else if(copie_titre==""){
			$('.copie_externe_cible[data-index='+copie_index+'] .error').html("Merci de renseigner le titre de l'article ou de supprimer la ligne.");
	}else if(copie_auteur==""){
		$('.copie_externe_cible[data-index='+copie_index+'] .error').html("Merci de renseigner l'auteur de l'article ou de supprimer la ligne.");
	}else if(copie_parution==""){
		$('.copie_externe_cible[data-index='+copie_index+'] .error').html("Merci de renseigner la date de parution de l'article ou de supprimer la ligne.");
	}else{
		$('.copie_externe_cible[data-index='+copie_index+'] .error').html("");
		
		if($('#container_copie_ext_cible > div').length){
			copie_ext_cible_structure=$('#container_copie_ext_cible > div').eq($('#container_copie_ext_cible > div').length-1).clone()
		}else{
			copie_ext_cible_structure=copie_ext_cible_structure_default;
		}
		
		var clone=$(copie_ext_cible_structure).clone(true, true);
		clone.find('input[type=radio]').each(function (index) {
	        var name = $(this).prop('name');
	        $(this).prop('name', name + $('#container_copie_ext_cible > div').length);
	    });

		$('#container_copie_ext_cible').append(clone);
		$('#container_copie_ext_cible > div').eq($('#container_copie_ext_cible > div').length - 1).attr("data-index",$('#container_copie_ext_cible > div').length - 1)
		
		
	    $(".copie_externe_cible[data-index="+($('#container_copie_ext_cible > div').length - 1)+"] .copie_externe_publication").val('');
		$(".copie_externe_cible[data-index="+($('#container_copie_ext_cible > div').length - 1)+"] .copie_externe_titre").val('');
		$(".copie_externe_cible[data-index="+($('#container_copie_ext_cible > div').length - 1)+"] .copie_externe_auteur").val('');
		$(".copie_externe_cible[data-index="+($('#container_copie_ext_cible > div').length - 1)+"] .copie_externe_parution").val('');

		

		init_copie_externe_cible_autocomplete();
		
	}
	
	
	
	
	
	
	
	
	
	
	



	//display_default_add_copie_externe_button();
}
function delete_copie_externe_cible(item){
	if(confirm('Voulez-vous supprimer cette publication web ?')){
		$(item).closest('.copie_externe_cible').remove();
		if($('#container_copie_ext_cible > div').length<1){
			$("#default_panorama_press_button").show();
		}else{
			$("#default_panorama_press_button").hide();
		}
		var i = 1

	}
	display_default_add_copie_externe_cible_button()
}



/*function add_pano_pub(item){
	var panorama_index=$(item).closest('.copie_externe').data('index');
	
	var compos_title=$(item).closest('.copie_ext_web_reaseau_line').find('input').eq(0).val();
	var compos_num=$(item).closest('.copie_ext_web_reaseau_line').find('input').eq(1).val();
	if(compos_num==""){
		$('.copie_externe[data-index='+panorama_index+'] .error').html("Merci de renseigner le nombre d'articles utilisés ou de supprimer la ligne.");
	}else if(compos_title==""){
		$('.copie_externe[data-index='+panorama_index+'] .error').html("Merci de renseigner l'intitulé de la publication ou de supprimer la ligne.");
	}else{
		$('.copie_externe[data-index='+panorama_index+'] .copie_externe_publications').append(pano_pub_line);
		$('.copie_externe[data-index='+panorama_index+'] .error').html("");
	}
	init_ppp_autocomplete();
}

function remove_pano_pub(item)
{
	var panorama_index=$(item).closest('.copie_externe').data('index');
	$(item).parent().parent().parent().remove();
	if($('.copie_externe[data-index='+panorama_index+'] .copie_externe_publications > div').length<1){
		$('.copie_externe[data-index='+panorama_index+'] .row-add-first-publication-press').show();
	}
	$('.copie_externe[data-index='+panorama_index+'] .error').html('');
	
}

*/

