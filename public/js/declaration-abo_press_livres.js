var abo_press_valid_Exts = new Array(".xlsx", ".xls", ".csv", ".xl");


jQuery(document).ready(function() {
	
	
	$('form').submit(function( event ) {

		
		if($('.abo-press-livres').length && $('.abo-press-livres-titre-line').length==0 && $('.abo-press-livres-site-line').length==0 && $('.abo-press-livres-line').length==0 
			&&	$('.wabo_fichier_name').length && $('.wabo_fichier_name').val().length < 1 && $('.wabo_fichier_media_key').val().length <1	&& $("input[type=file]").val().length <1
		){
			
			if(!confirm("Vous n'avez pas déclaré de publications. Confirmez vous cette déclaration ?"))
			{
				event.preventDefault();
			}
			
		}
		
		if($('.abo-press-livres').length && ( $('.abo-press-livres-titre-line').length + $('.abo-press-livres-site-line').length + $('.abo-press-livres-line').length)>0 &&  $('.custom-file-upload-filename-text').val().length>1		
		)
		{
			if(!confirm("Vous avez à la fois chargé un fichier Excel et saisi des titres. C'est votre fichier Excel qui sera pris en compte. \n(si vous supprimez votre fichier Excel, c'est votre saisie qui sera prise en compte)"))
			{
				event.preventDefault();
			}
		}
		
		
		
		
	});
	
	

	
	if($('.abo-press-livres').length){
		titre_press_init();

		
		
		if($('.wabo_fichier_media_key').length){
			if($('.wabo_fichier_media_key').val().length>32)
			{
				$('.custom-file-upload-filename-text').val($('.wabo_fichier_name').val());
			
				$('.delete_file').show();
			}
		}
	
	}
	if($('#wpresta_prestataire_non').length){
		$('#wpresta_prestataire_non').on('click', function(){
			$('#wpresta_prestataire_non').closest('.contained').find(".line_wpresta_prestataire_label2").hide();
		});
	}
	
	
});

function add_titre_livre(item){
	$('#err-livre').html('');
	console.log($(item).closest('.abo-press-livres-line'));
	if( 
			$(item).closest('.abo-press-livres-line').find('.wabop_livre').val().length==0 || 
			$(item).closest('.abo-press-livres-line').find('.wabop_auteur').val().length==0 ||
			$(item).closest('.abo-press-livres-line').find('.wabop_editeur').val().length==0
		){
			$(item).tooltip({
	    		'title':$(item).data('tooltip'),
	    		'placement': 'right'
	    		});
			$(item).tooltip('show');
		
			//$('#err-livre').html("Vous devez reseigner un titre de livre, son auteur et son éditeur, ou supprimer celui-ci.");
		}else{
			$('.abo-press-livres-container-line').append(abo_livre_line);
		}
	
}

function add_first_livre()
{
	reset_abo_press_upload();
	if(($("input[type=file]").val().length  + $("input[name=wabo_fichier_name]").val().length)==0)
	{
		$('.abo-press-livres-container-line').append(abo_livre_line);
		titre_press_init();
	}

}


function  delete_titre_livre(item){
	$('#err-livre').html('');
	
	if(
			$(item).closest('.abo-press-livres-line').find('.wabop_livre').val().length>0 || 
			$(item).closest('.abo-press-livres-line').find('.wabop_auteur').val().length>0 ||
			$(item).closest('.abo-press-livres-line').find('.wabop_editeur').val().length>0
	){
		if(confirm('Voulez-vous supprimer ce livre ?')){
			$(item).closest('.abo-press-livres-line').remove();
		}
	}else{
		$(item).closest('.abo-press-livres-line').remove();
	}
	titre_press_init();
}


function add_titre_press(item){
	if($(item).closest('.abo-press-livres-titre-line').find('input').val().length){
		$('.abo-press-livres-titre-container-line').append(abo_livre_titre_line);
		titre_press_init();
	}else{
		$(item).tooltip({
    		'title':$(item).data('tooltip'),
    		'placement': 'right'
    		});
		$(item).tooltip('show');
		//$('#err-presse').html("Vous devez reseigner un titre de presse, ou supprimer celui-ci.");
	}
	
	
}

function add_site_press(item){
	
	if($(item).closest('.abo-press-livres-site-line').find('input').val().length){
		$('.abo-press-livres-site-container-line').append(abo_livre_site_line);
		titre_press_init();
	}else{
		$(item).tooltip({
    		'title':$(item).data('tooltip'),
    		'placement': 'right'
    		});
		$(item).tooltip('show');
		//$('#err-presse').html("Vous devez reseigner un site de presse, ou supprimer celui-ci.");
	}

}

function add_first_press()
{
	reset_abo_press_upload()
	if(($("input[type=file]").val().length  + $("input[name=wabo_fichier_name]").val().length)==0)
	{
		$('.abo-press-livres-titre-container-line').append(abo_livre_titre_line);
		titre_press_init();
	}

	
}

function add_first_site()
{
	reset_abo_press_upload()
	if(($("input[type=file]").val().length  + $("input[name=wabo_fichier_name]").val().length)==0)
	{
		$('.abo-press-livres-site-container-line').append(abo_livre_site_line);
		titre_press_init();
	}

	
}



function delete_titre_press(item){
	$('#err-presse').html('');
	if($(item).closest('.abo-press-livres-titre-line').find('input').val().length){
		if(confirm('Voulez-vous supprimer ce titre de presse ?')){
			$(item).closest('.abo-press-livres-titre-line').remove();
			titre_press_init();
			
		}
	}else{
		$(item).closest('.abo-press-livres-titre-line').remove();
		titre_press_init();
	}
	
}


function delete_site_press(item){
	$('#err-presse').html('');
	if($(item).closest('.abo-press-livres-site-line').find('input').val().length){
		if(confirm('Voulez-vous supprimer ce site de presse ?')){
			$(item).closest('.abo-press-livres-site-line').remove();
			titre_press_init();
		}
	}else{
		$(item).closest('.abo-press-livres-site-line').remove();
		titre_press_init();
	}

}

function titre_press_init()
{

	if($('.abo-press-livres-site-line:visible').length==0)
	{
		$('.add-first-site').show();
	}else{
		$('.add-first-site').hide();
	}
	if($('.abo-press-livres-titre-line:visible').length==0)
	{
		$('.add-first-press').show();
	}else{
		$('.add-first-press').hide();
	}

	if($('.abo-press-livres-line:visible').length==0)
	{
		$('.add-first-livre').show();
	}else{
		$('.add-first-livre').hide();
	}
	
	$('.wabop_site').devbridgeAutocomplete({
		serviceUrl: '/api-panorama-press-publications-site/?',
	    onSelect: function (suggestion) {}
	});
	
	$('.wabop_titre').devbridgeAutocomplete({
		serviceUrl: '/api-panorama-press-publications-ipro/?',
	    onSelect: function (suggestion) {}
	});
	
	
	
	$('input[type="file"]').change(function(e){
		var fileName = e.target.files[0].name;
	    var sender = e.target.files[0];
	    
		//var panorama_index=$(this).closest('.panorama_press_numerique').data('index');

	    fileName = fileName.substring(fileName.lastIndexOf('.'));
	    if (abo_press_valid_Exts.indexOf(fileName) < 0) {
	    	alert("Type de fichier invalide. Veuillez séléctionner un fichier de type " + abo_press_valid_Exts.toString() + ".");
	    	$(e.target).parent().find(".custom-file-upload-filename input").val(abo_press_valid_Exts.toString());
	    	$(e.target).parent().find(".delete_file").hide();
	    }else{

	    	if($('.abo-press-livres-titre-line').length || $('.abo-press-livres-site-line').length || $('.abo-press-livres-line').length){
	    		
	    		if(confirm("Le chargement d'un fichier pour vos abonnements et achats de presse ou de livres nécessite la suppression des publications précédement renseignées.\r\nConfirmez-vous la supression des publications déclarées  ?"))
		    	{
		    		$(".custom-file-upload-filename input").val(e.target.files[0].name);
		    		$(".delete_file").show();
		    		$(".abo-press-livres-titre-container-line").html("");
		    		$(".abo-press-livres-site-container-line").html("");
		    		$(".abo-press-livres-container-line").html("");
		    		
		    		$('#err-livre').html("");
		    		$('#err-presse').html("");
		    		
		    		$('.add-first-press').show();
		    		$('.add-first-site').show();
		    		$('.add-first-livre').show();
		    	}else{
			    	$(e.target).parent().find(".custom-file-upload-filename input").val(abo_press_valid_Exts.toString());
			    	$(e.target).parent().find(".delete_file").hide();
		    	}
	    	}else{
	    		$(e.target).parent().find(".custom-file-upload-filename input").val(e.target.files[0].name);
	    		$(e.target).parent().find(".delete_file").show();
	    	}
	    	
			
	    }
	});
}

function reset_abo_press_upload()
{
	/*if(($("input[type=file]").val().length > 0 || $("input[name=wabo_fichier_name]").val().length ==0) ){
		return true;
	}*/
	if(
			($("input[type=file]").val().length > 0 || $("input[name=wabo_fichier_name]").val().length > 0) 
			
			&& confirm("L'ajout d'une publication nécessite la suppresion du fichier précédement chargé.\r\nConfirmez-vous la supression du fichier chargé ?"))
	{
		$(".custom-file-upload-filename input").val(panorama_valid_Exts.toString());
		$("input[type=file]").val('');
		$(".delete_file").hide();
		$("input[name=wabo_fichier_media_key]").val("");
		$("input[name=wabo_fichier_name]").val("");
		//$('.wabo_fichier_name').val().length < 1 && $('.wabo_fichier_media_key').val()
		return true;
	}else{
		return false;
	}
	
}
