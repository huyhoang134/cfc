var selected_service=false;
var gestion_engagement=false;
var gestion_service=false;
var gestion_service_engagement=false;
var service_gestion_egmt=false;

jQuery(document).ready(function() {
	
selected_service=$('#wde_service_code').val();

$('form').submit(function( event ) {
	if($('#chorus_pro_service').length && $('#chorus_pro_service').attr("data-service-gestion-egmt")=="true"){
		if($('#chorus_pro_service').val()=="" && $('#chorus_pro_bdc').val()=="")
		{	
			alert("Veuillez renseigner un code service ou un n° de bon de commande ou d'engagement.");
			event.preventDefault();
		}
	}
});	
	
if($('#chorus_pro_siret').length)	{
	$('#wde_is_chorus_pro_oui').on('click',function(){
		$('#chorus_pro_siret').prop('required',true);
		$.chorus_pro_check($('#chorus_pro_siret').val());
		$('.chorus_pro_bloc').show();
	})
	$('#wde_is_chorus_pro_non').on('click',function(){
		$('#chorus_pro_siret').prop('required',false);
		$('#chorus_pro_service').prop('required',false);
		$('#chorus_pro_bdc').prop('required',false);
		$('.chorus_pro_bloc').hide();
	})	

	if($('input[name=wde_is_chorus_pro]:checked').val()==1)
	{
		$('#chorus_pro_siret').prop('required',true);
		$('.chorus_pro_bloc').show();
	}else{
		$('.chorus_pro_bloc').hide();	
	}
}


	
	$.chorus_pro_check = function(siret) {
		if(typeof(siret)=="undefined")
		{
			return;
		}
		console.log("####################");
		console.log("chorus_pro_check");
		console.log("####################");
		$('#chorus_pro_bdc').prop('required',false);
		$('#chorus_pro_service').prop('required',false);

		
		if( siret.length==14){
			$.ajax({
				  url: "/api-chorus-pro-siret",
				  data: { query: siret },
				  context: document.body
				}).done(function(data) {				 
				  if(data.suggestions.length==1){
					  var params=data.suggestions[0];
					  
					  console.log("Traitement des services pour le siret");

					  $('#chorus_pro_service').editableSelect({ filter: false });
					  $('#chorus_pro_service').editableSelect('clear');
					  $('#chorus_pro_service').attr("oninvalid","this.setCustomValidity('Merci de renseigner le code service.')");
					  $('#chorus_pro_service').attr("oninput","this.setCustomValidity('')");
					  $('#chorus_pro_service').attr("maxlength",100);
					  $('#chorus_pro_bdc').prop('required',false);
					  $('#chorus_pro_service').prop('required',false);
					  
					  $(params.services).each(function(key, value){
						  console.log('==== Service :'+value.service_code);
	    	        		$('#chorus_pro_service').editableSelect('add', value.service_code + ' (' + value.service_nom +')'  ,key, 
	    	        				[
	    	        				 {name:'data-service',value:value.service_code},
	    	        				 {name:'data-service-gestion-egmt',value:value.service_gestion_Egmt},
	    	        				 {name:'data-gestion-engagement',value:value.gestion_engagement},
	    	        				 {name:'data-gestion-service',value:value.gestion_service},
	    	        				 {name:'data-gestion-service-engagement',value:value.gestion_service_engagement}
	    	        				 ])
	    	        				 /*.on('select.editable-select', function (e, li) {
	    	        					 $.chorus_pro_check($('#chorus_pro_siret').val());
	    	        					
	    	        	    });*/
					  })
					  
					  console.log(params);
					  if(params.gestion_engagement==true){
						  console.log("Gestion engagement : oui => BDC obligatoire");
						  console.log("chorus-pro-bdc-mandatory : "+$('#chorus_pro_bdc').data('chorus-pro-bdc-mandatory'));
						  if($('#chorus_pro_bdc').data('chorus-pro-bdc-mandatory')==1){
							  $('#chorus_pro_bdc').prop('required',true);
						  }else{
							  $('#chorus_pro_bdc').prop('required',false);
							  console.log("BDC pas obligatoire");
						  }
						  
						  
					  }else{
						  console.log("Gestion engagement : non");
					  }
					  
					  if(params.gestion_service==true){
						  console.log("Gestion service : oui => Code service obligatoire");
						  $('#chorus_pro_service').prop('required',true);
					  }else{
						  console.log("Gestion service : non");
					  }
					  
					  if(params.gestion_service_engagement==true){
						  console.log("Gestion service engagement : oui => le code service ou le bon de commande est obligatoire");
						  $('#chorus_pro_service').attr('data-service-gestion-egmt', 'true');
					  }else{
						  console.log("Gestion service engagement : non");
					  }
					  console.log("=== Verification du service :")
					  service=$('#chorus_pro_service').val();
					  var service_param=false;
					  if(service.length){
						  console.log('===Service trouvé :'+service)
						  //console.log(params.services);
						  
						  for (var i = 0; i < params.services.length; i++) {
							  if(params.services[i].service_code==service){
								  service_param=params.services[i];
								  console.log(params.services[i]);
							  }
							    
							}
						  
						  if(service_param) {
							  console.log("===Le service est présent dans chorus pro");
							  if(service_param.service_gestion_Egmt==true){
								  console.log("===le champs GestionEgmt du CppService est à oui le bon de commande est obligatoire pour le service.");
								  
								  console.log("chorus-pro-bdc-mandatory : "+$('#chorus_pro_bdc').data('chorus-pro-bdc-mandatory'));
								  if($('#chorus_pro_bdc').data('chorus-pro-bdc-mandatory')==1){
									  $('#chorus_pro_bdc').prop('required',true);
								  }else{
									  $('#chorus_pro_bdc').prop('required',false);
									  console.log("BDC pas obligatoire");
								  }
							  }else{
								  console.log("===le champs GestionEgmt :  non.")
							  }
							  
							} else {
							    console.log("===Le service n'est pas présent dans la liste chorus pros");
							}
					  }else{
						  console.log("=== pas de service selectionné")
					  }

					 
					  
				  }else{
					  console.log("Le siret n'a pas été trouvé en base");
				  }
				  
				});
		}else{
			$("#chorus_pro_bdc").trigger("input");
			console.log("Code siret invalide : "+siret);
		}
		console.log("####################");
	}


	

	
	$.chorus_pro_check_input = function() {
		$('#chorus_pro_siret, #chorus_pro_service').on('input keyup', function (e) {
			var val=$.trim($('#chorus_pro_siret').val());
			
				switch (e.keyCode) {
					case 38: // Up
						return;
						break;
					case 40: // Down
						break;
					case 13: // Enter
						
						$.chorus_pro_check(val);
						e.preventDefault();
						break;
					case 9:  // Tab
					case 27: // Esc
						break;
					default:
						$.chorus_pro_check(val);
						break;
				}
		
			
		});
	};
	
	$('#chorus_pro_siret').devbridgeAutocomplete({
		serviceUrl: '/api-chorus-pro-siret/?',
	    onSelect: function (suggestion) {
	    	if( chorus_pro_siret_selected!=suggestion.data){
	    		var content ='<span class="font-weight-bold text-uppercase">'+suggestion.raison_sociale+'</span>';
	    		    content+='<br><span class="text-uppercase">'+suggestion.adresse+' ' + suggestion.code_postal+' ' + suggestion.ville+'</span>';
	    		    var promise = $.delefconfirm('Ce numero de siret/ridet corresponds à:',content,'Modifier le n° de siret/ridet','valider',true);
	    	        promise.done(function() {
	    	        	$.chorus_pro_check($('#chorus_pro_siret').val());
	    	        });
	    	        promise.fail(function() {
	    	        });
	    	        
			}

	    	$('#chorus_pro_siret').val(suggestion.data);
	    	chorus_pro_siret_selected=suggestion.data;
	    }
	});
	
	

	
	$( document ).on( "EditableSelect.created", function( event  ) {
		if(selected_service){
			$('#chorus_pro_service ').val(selected_service);
		}
		if($('#chorus_pro_siret').length){
			$.chorus_pro_check_input();
			$.chorus_pro_check($('#chorus_pro_siret').val());
		}

	});
	
	$('#chorus_pro_service ').editableSelect({ filter: false }).on('select.editable-select', function (e, li) {
		selected_service=$(li).attr("data-service");
		$('#chorus_pro_service ').val(selected_service);
		$('#chorus_pro_service').trigger("input");
		//console.log(li);
		//console.log("==>service selectionné=>traitement pour "+$('#chorus_pro_siret').val());
		$.chorus_pro_check($('#chorus_pro_siret').val());
	});
	

	
	
	
});