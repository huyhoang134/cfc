jQuery(document).ready(function() {
	
	$('.field-contact').each(function(){
		
		var fieldContact=$(this);
			$(fieldContact).find('select').editableSelect({ filter: false }).on('select.editable-select', function (e, li) {
			
			/*var title=$(fieldContact).find('.es-input').data('last-title');
			var original_value=$(fieldContact).find('.es-input').data('value');
			
			if(original_value!=$(li).attr('value')){
				var content ='<span class="">Modification du contact : </span><span class="font-weight-bold">'+original_value+'</span>';
			    content+='<br><span class="">Cette personne exerce-t-elle toujours une fonction dans votre établissement/entreprise/adminitration?</span>';
			    var promise = $.delefconfirm('<span class="font-weight-bold text-uppercase">'+title+'</span>',content,'non, supprimez ce contact','oui',true);
		        promise.done(function() {
		        	//Contact still in charge. Nothing to do.
		        });
		        promise.fail(function() {
		        	//contact should be erased from directory
		        	$.post( "/api-contact-remove", { name: original_value } );
		        });
		        
		        
			}*/

			$(fieldContact).find('.es-input').data('value',$(li).attr('value'));
			
			var target_civilite=$(fieldContact).find('.es-input').data("field-civilite");
			var target_function=$(fieldContact).find('.es-input').data("field-function");
			var target_tel=$(fieldContact).find('.es-input').data("field-tel");
			var target_fax=$(fieldContact).find('.es-input').data("field-fax");
			var target_email=$(fieldContact).find('.es-input').data("field-email");
			var target_contact_id=$(fieldContact).find('.es-input').data('contact-field-name');


			if(target_civilite.length)
			{
				if($(li).data('civilite')=="Madame" || $(li).data('civilite')=="Mademoiselle" || $(li).data('civilite')=="Soeur" ){
					$('#'+target_civilite+'_Madame').prop( "checked", true );
				}else{
					console.log('#'+target_civilite+'_Monsieur');
					$('#'+target_civilite+'_Monsieur').prop( "checked", true );
				}
			}
			
			
			if(target_function.length)
			{
				$('input[name='+target_function+']').val($(li).data('function'));
			}
			
			if(target_tel.length)
			{
				$('input[name='+target_tel+']').val($(li).data('tel'));
			}
			
			if(target_fax.length)
			{
				$('input[name='+target_fax+']').val($(li).data('fax'));
			}
			
			
			if(target_email.length)
			{
				$('input[name='+target_email+']').val($(li).data('email'));
			}

			
			
			$('input[name='+target_contact_id+']').val($(li).data('id'));
				
	    });
		
			
		
		
		$(fieldContact).find('.icon-contact').click(function(){
			$(fieldContact).find('.es-input').focus();
			console.log($(fieldContact));
			
		});
	})


	
});

$( document ).on( "EditableSelect.created", function(event, item) {
	if(item.data("type")=="contact"){
		var field=item;
		$(field).on('blur', function (e) {
			
			var title=$(field).data('last-title');
			var original_value=$(field).data('original-value');

			
			if(original_value.length && original_value!=$(field).val()){
				var content ='<span class="">Modification du contact : </span><span class="font-weight-bold">'+original_value+'</span>';
			    content+='<br><span class="">Cette personne exerce-t-elle toujours une fonction dans votre établissement/entreprise/administration?</span>';
			    var promise = $.delefconfirm('<span class="font-weight-bold text-uppercase">'+title+'</span>',content,'non, supprimez ce contact','oui',true);
		        promise.done(function() {
		        	//Contact still in charge. Nothing to do.
		        	
		        	field_id_name=$(field).data('contact-field-name');
		        	$('#'+field_id_name+'_question').val('1');
		        });
		        promise.fail(function() {
		        	//contact should be erased from directory
		        	//$.post( "/api-contact-remove", { name: original_value } );
		        	field_id_name=$(field).data('contact-field-name');
		        	//$('#'+field_id_name).val('0');
		        	$('#'+field_id_name+'_question').val('0');
		        	
		        });
		        $(field).data('original-value','')
			}
		});
	
	}

});