var panorama_structure="";
var panorama_valid_Exts = new Array(".xlsx", ".xls", ".csv", ".xl");
var panorama_structure_default="";
jQuery(document).ready(function() {
	

	
	init_ppp_autocomplete();
	close_panorama_all()
	display_default_add_press_numerique_button();

	//$('.custom-file-upload-filename-text').val(panorama_valid_Exts.join(", "));
	//$(" input[type=file]").val('')
		
	

    setTimeout(function () {
    	panorama_structure_default=$('#container_panorama_press_numerique > div').eq($('#container_panorama_press_numerique > div').length-1).clone();
    }, 1000);

	

	
	$('.panorama_press_numerique').each(function(){
		var data_index=$(this).data('index');
		if($(this).find('.wppl_fichier_media_key').length){

			if($(this).find('.panorama_press_numerique_publications > div').length < 1 && $(this).find('.wppl_fichier_media_key').val().length>32)
			{
				/*console.log($(this).find('.panorama_press_numerique_publications > div').length);
				console.log($(this).find('.wppl_fichier_media_key').val().length);
				console.log($(this).find('.wppl_fichier_name').val());
				console.log($(this).find('.custom-file-upload-filename-text'));*/
				setTimeout(() => {
					$(this).find('.custom-file-upload-filename-text').val($(this).find('.wppl_fichier_name').val());
				}, 500);
	
				
				$('.panorama_press_numerique[data-index='+data_index+'] .row-add-first-publication-press').show();
				$(this).find('.delete_file').show();
			}
		}

	});
	console.log(panorama_structure_default);
});

function init_ppp_autocomplete(){
	$('.wpplp_label').devbridgeAutocomplete({
		serviceUrl: '/api-panorama-press-publications/?',
	    onSelect: function (suggestion) {}
	});
	
	$('.panorama_press_numerique input[type="file"]').change(function(e){
		var fileName = e.target.files[0].name;
	    var sender = e.target.files[0];
	    
		var panorama_index=$(this).closest('.panorama_press_numerique').data('index');

	    fileName = fileName.substring(fileName.lastIndexOf('.'));
	    if (panorama_valid_Exts.indexOf(fileName) < 0) {
	    	alert("Type de fichier invalide. Veuillez séléctionner un fichier de type " + panorama_valid_Exts.toString() + ".");
	    	$(e.target).parent().find(".custom-file-upload-filename input").val(panorama_valid_Exts.toString());
	    	$(e.target).parent().find(".delete_file").hide();
	    }else{

	    	if($('.panorama_press_numerique[data-index='+panorama_index+'] .panorama_press_numerique_publications > div').length){
	    		
	    		if(confirm("Confirmez-vous le chargement d’un fichier Excel ?"))
		    	{
		    		$(e.target).parent().find(".custom-file-upload-filename input").val(e.target.files[0].name);
		    		$(e.target).parent().find(".delete_file").show();
		    		$(".panorama_press_numerique[data-index="+panorama_index+"] .panorama_press_numerique_publications").html("");
		    		$('.panorama_press_numerique[data-index='+panorama_index+'] .error').html("");
		    		$('.panorama_press_numerique[data-index='+panorama_index+'] .row-add-first-publication-press').show();
		    		$(e.target).parent().parent().parent().find(".title-ou-sasir-publication, .row-add-first-publication-press").hide();
		    		
		    	}else{
		    		$(e.target).parent().find('input').val(panorama_valid_Exts.toString());
		    		$(e.target).parent().parent().find("file").val('');
		    		$(e.target).parent().find(".delete_file").hide();

		    	}
	    	}else{
	    		$(e.target).parent().find(".custom-file-upload-filename input").val(e.target.files[0].name);
	    		$(e.target).parent().find(".delete_file").show();
	    	}
	    	
			
	    }
	});
	display_default_add_press_numerique_button()
}


function display_default_add_press_numerique_button()
{
	return;
	if($('.panorama_press_numerique').length){
		$('#default_panorama_press_button').hide();
	}else{
		$('#default_panorama_press_button').show();
	}
}


function close_panorama_all()
{
	return;
	$('.panorama_press_numerique .panorama_content:visible').each(function(){
		 if($(this).closest('.panorama_press_numerique').find('.panorama_content').is(":visible")){
		    	$(this).closest('.panorama_press_numerique').find(".ico-expend-collapse").removeClass('ico-expend');
		    	$(this).closest('.panorama_press_numerique').find(".ico-expend-collapse").addClass('ico-collapse');
		    }else{
		    	$(this).closest('.panorama_press_numerique').find(".ico-expend-collapse").removeClass('ico-collapse');
		    	$(this).closest('.panorama_press_numerique').find(".ico-expend-collapse").addClass('ico-expend');
		    }
		$(this).closest('.panorama_press_numerique').find('.panorama_content').toggle("blind", {},500, function() {});
	});
}

function toggle_panorama(item){
	

	//close_panorama_all();

	if($(item).closest('.panorama_press_numerique').find('.panorama_content').is(":visible")==false){
		
		if($(item).closest('.panorama_press_numerique').find('.panorama_content').is(":visible")){
	    	$(item).parent().removeClass('ico-expend');
	    	$(item).parent().addClass('ico-collapse');
	    }else{
	    	$(item).parent().removeClass('ico-collapse');
	    	$(item).parent().addClass('ico-expend');
	    }
		
		$(item).closest('.panorama_press_numerique').find('.panorama_content').toggle("blind", {},500, function() {
		    
		    if($(item).closest('.panorama_press_numerique').find('input:invalid').length)
		    {
		    	$( $(item).closest('.panorama_press_numerique').find('input:invalid').eq(0) )[0].scrollIntoView(); 
		    }
		    
		});
	}else{
		$(item).parent().removeClass('ico-expend');
    	$(item).parent().addClass('ico-collapse');
    	$(item).closest('.panorama_press_numerique').find('.panorama_content').toggle("blind", {},500, function() {
		    
		    if($(item).closest('.panorama_press_numerique').find('input:invalid').length)
		    {
		    	$( $(item).closest('.panorama_press_numerique').find('input:invalid').eq(0) )[0].scrollIntoView(); 
		    }
		    
		});
	}



}
function add_panorama_press(){
	var last_press = $(".panorama_press_numerique[data-index="+($('#container_panorama_press_numerique > div').length - 1)+"]");
	
	
	
	if( $(last_press).length && $(last_press).find('.panorama_press_numerique_publications_line').length  == 0 && 
			 ( $(last_press).find('.wppl_fichier_name').length ==0 || $(last_press).find('.wppl_fichier_name').val().length < 1)	&& 
			 
			$(last_press).find('.wppl_fichier_media_key').length  && $(last_press).find('.wppl_fichier_media_key').val().length <1	&& 
			$(last_press).find("input[type=file]").val().length <1
			
					){
			
			$(last_press).find('.error').html("Vous devez renseigner au moins une publication ou charger un fichier Excel pour ce panorama.");
			return;

		}else{
			$(last_press).find('.error').html("");
		}
	var publicationIsValid=true;
	$(last_press).find('.wpplp_label, .wpplp_total').each(function(){
		if($(this)[0].checkValidity()==false){
			publicationIsValid=false;
			$(last_press).find('.error').html("Vous devez renseigner la publication et le nombre d'articles ou supprimer la ligne");
			return; 
		}
		
	});
	
	if(publicationIsValid==false){return;}
	
	$('#panorama-error').html("");
	if($('#container_panorama_press_numerique > div').length){
		panorama_structure=$('#container_panorama_press_numerique > div').eq($('#container_panorama_press_numerique > div').length-1).clone()
	}else{
		panorama_structure=panorama_structure_default;
	}
	
	var clone=$(panorama_structure).clone(true, true);
	clone.find('input[type=radio]').each(function (index) {
        var name = $(this).prop('name');
        $(this).prop('name', name + $('#container_panorama_press_numerique > div').length);
    });

	$('#container_panorama_press_numerique').append(clone);
	
	$('#container_panorama_press_numerique > div').eq($('#container_panorama_press_numerique > div').length - 1).attr("data-index",$('#container_panorama_press_numerique > div').length - 1)

	$(".panorama_press_numerique[data-index="+($('#container_panorama_press_numerique > div').length - 1)+"] .custom-file-upload-filename input").val(panorama_valid_Exts.toString());
	$(".panorama_press_numerique[data-index="+($('#container_panorama_press_numerique > div').length - 1)+"] .delete_file").hide();
	$(".panorama_press_numerique[data-index="+($('#container_panorama_press_numerique > div').length - 1)+"] .wppl_fichier").val('');
	$(".panorama_press_numerique[data-index="+($('#container_panorama_press_numerique > div').length - 1)+"] .wppl_fichier_media_key").val('');
	$(".panorama_press_numerique[data-index="+($('#container_panorama_press_numerique > div').length - 1)+"] .wppl_fichier_name").val('');
	
	
	
	/*if($(".panorama_press_numerique[data-index="+($('#container_panorama_press_numerique > div').length - 1)+"] .wppl_label").val().trim()==""){
		$(".panorama_press_numerique[data-index="+($('#container_panorama_press_numerique > div').length - 1)+"] .wppl_label").val('Panorama de Presse ' + $('#container_panorama_press_numerique > div').length);
	}*/
	
	$(".panorama_press_numerique[data-index="+($('#container_panorama_press_numerique > div').length - 1)+"] .wppl_total").val('');
	$(".panorama_press_numerique[data-index="+($('#container_panorama_press_numerique > div').length - 1)+"] .wppl_periode").val('');
	
	$(".panorama_press_numerique[data-index="+($('#container_panorama_press_numerique > div').length - 1)+"] .wppl_order_number").val('');
	
	$(".panorama_press_numerique[data-index="+($('#container_panorama_press_numerique > div').length - 1)+"] .block_add_delete").show();
	$(".panorama_press_numerique[data-index="+($('#container_panorama_press_numerique > div').length - 1)+"] .wpplp_total").each(function(){
		$(this).val('');
		
	});
	
	//$(".panorama_press_numerique[data-index="+($('#container_panorama_press_numerique > div').length - 1)+"] .wppl_total").val('')

	$(".panorama_press_numerique[data-index="+($('#container_panorama_press_numerique > div').length - 1)+"] .row-add-first-publication-press").show();
	$(".panorama_press_numerique[data-index="+($('#container_panorama_press_numerique > div').length - 1)+"] .title-ou-sasir-publication").show();
	
	/*if($(".panorama_press_numerique[data-index="+($('#container_panorama_press_numerique > div').length - 1)+"] .panorama_press_numerique_publications > div").length <1){
		$(".panorama_press_numerique[data-index="+($('#container_panorama_press_numerique > div').length - 1)+"] .row-add-first-publication-press > div").show();
	}*/
	
	var i = 1
	$('#container_panorama_press_numerique > div').each(function(){
		$(this).find('.label-panorama-press').html("Panorama de presse "+i);
		$(this).find('label.custom-file-upload').attr('for','file-upload-'+i);
		$(this).find('.wppl_fichier').attr('id','file-upload-'+i);
		i++;
	});
	init_ppp_autocomplete();
	$( document ).trigger( "chorus_pro_multi.init", [  ] );
	$(".panorama_press_numerique[data-index="+($('#container_panorama_press_numerique > div').length - 1)+"] .wppl_service_code").val('');
	display_default_add_press_numerique_button();
	display_delete_panorama_button();
}

function display_delete_panorama_button(){
	if($('#container_panorama_press_numerique > div').length==1){
		$('#container_panorama_press_numerique > div').each(function(){
			$(this).find(".block_add_delete").hide();
		});
	}else{
		$('#container_panorama_press_numerique > div').each(function(){
			$(this).find(".block_add_delete").show();
		});
	}

}

function delete_panorama_press(item){
	if(confirm('Voulez-vous supprimer ce panorama ?')){
		$(item).closest('.panorama_press_numerique').remove();
		/*if($('#container_panorama_press_numerique > div').length<1){
			$("#default_panorama_press_button").show();
		}else{
			$("#default_panorama_press_button").hide();
		}*/
		var i = 1
		$('#container_panorama_press_numerique > div').each(function(){
			$(this).find('.label-panorama-press').html("Panorama de presse "+i);
			i++;
		});
	}
	display_default_add_press_numerique_button();
	display_delete_panorama_button();
}

function add_first_pano_pub(item){
	var panorama_index=$(item).closest('.panorama_press_numerique').data('index');

	
	if(
			$(".panorama_press_numerique[data-index="+panorama_index+"]  input[type=file]").val().length==0 && 
			$(".panorama_press_numerique[data-index="+panorama_index+"]  .wppl_fichier_name").val().length==0
			
	){
		$('.panorama_press_numerique[data-index='+panorama_index+'] .panorama_press_numerique_publications').append(pano_pub_line);
		$('.panorama_press_numerique[data-index='+panorama_index+'] .row-add-first-publication-press').hide();
	}
	if(
			($(".panorama_press_numerique[data-index="+panorama_index+"]  input[type=file]").val().length > 0 || $(".panorama_press_numerique[data-index="+panorama_index+"]  .wppl_fichier_name").val().length > 0) 
			
			&& confirm("Confirmez-vous la suppression du fichier Excel ?"))
	{
		$(".panorama_press_numerique[data-index="+panorama_index+"] .custom-file-upload-filename input").val(panorama_valid_Exts.toString());
		$(".panorama_press_numerique[data-index="+panorama_index+"]  input[type=file]").val('');
		$(".panorama_press_numerique[data-index="+panorama_index+"] .delete_file").hide();
		$(".panorama_press_numerique[data-index="+panorama_index+"]  .wppl_fichier_media_key").val("");
		$(".panorama_press_numerique[data-index="+panorama_index+"]  .wppl_fichier_name").val("");
		$('.panorama_press_numerique[data-index='+panorama_index+'] .panorama_press_numerique_publications').append(pano_pub_line);
		$('.panorama_press_numerique[data-index='+panorama_index+'] .row-add-first-publication-press').hide();
	}
	init_ppp_autocomplete();
}

function add_pano_pub(item){
	var panorama_index=$(item).closest('.panorama_press_numerique').data('index');
	
	var compos_title=$(item).closest('.panorama_press_numerique_publications_line').find('input').eq(0).val();
	var compos_num=$(item).closest('.panorama_press_numerique_publications_line').find('input').eq(1).val();
	if(compos_title==""){
		$('.panorama_press_numerique[data-index='+panorama_index+'] .error').html("Merci de renseigner l'intitulé de la publication ou de supprimer la ligne.");
	}else if(compos_num==""){
		$('.panorama_press_numerique[data-index='+panorama_index+'] .error').html("Merci de renseigner le nombre d'articles utilisés ou de supprimer la ligne.");
	}else{
		$('.panorama_press_numerique[data-index='+panorama_index+'] .panorama_press_numerique_publications').append(pano_pub_line);
		$('.panorama_press_numerique[data-index='+panorama_index+'] .error').html("");
	}
	init_ppp_autocomplete();
}

function remove_pano_pub(item)
{
	var panorama_index=$(item).closest('.panorama_press_numerique').data('index');
	$(item).parent().parent().parent().remove();
	if($('.panorama_press_numerique[data-index='+panorama_index+'] .panorama_press_numerique_publications > div').length<1){
		$('.panorama_press_numerique[data-index='+panorama_index+'] .row-add-first-publication-press').show();
	}
	$('.panorama_press_numerique[data-index='+panorama_index+'] .error').html('');
	
}


function reset_file_upload(el){
	$(el).parent().find('input').val(panorama_valid_Exts.toString());
	$(el).parent().parent().find(".wppl_fichier").val('');
	$(el).parent().parent().find(".wppl_fichier_media_key").val('');
	$(el).parent().parent().find(".wppl_fichier_name").val('');
	$(el).parent().find(".delete_file").hide();
	$(el).parent().parent().parent().parent().find(".title-ou-sasir-publication, .row-add-first-publication-press").show();
	console.log($(el).parent().parent().parent().parent());
}
