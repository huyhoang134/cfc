jQuery(document).ready(function() {
  $('#aide>.toggler').click(function() {
    $('#aide').toggleClass('modal-open');
    $('#aide-accordion').toggleClass('d-none');
    if ($(this).hasClass('opened')) {
      $(this).removeClass('opened');
    } else {
      $(this).addClass('opened');
    }
  })
});
