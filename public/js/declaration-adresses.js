jQuery(document).ready(function() {
	
	$('.field-adresses').each(function(){
		
		var fieldAdresses=$(this);
			$(fieldAdresses).find('select').editableSelect({ filter: false }).on('select.editable-select', function (e, li) {
			
			/*var title=$(fieldAdresses).find('.es-input').data('last-title');
			var original_value=$(fieldAdresses).find('.es-input').data('value');
			
			if(original_value!=$(li).attr('value')){
				var content ='<span class="">Modification du contact : </span><span class="font-weight-bold">'+original_value+'</span>';
			    content+='<br><span class="">Cette personne exerce-t-elle toujours une fonction dans votre établissement/entreprise/adminitration?</span>';
			    var promise = $.delefconfirm('<span class="font-weight-bold text-uppercase">'+title+'</span>',content,'non, supprimez ce contact','oui',true);
		        promise.done(function() {
		        	//Contact still in charge. Nothing to do.
		        });
		        promise.fail(function() {
		        	//contact should be erased from directory
		        	$.post( "/api-contact-remove", { name: original_value } );
		        });
		        
		        
			}*/

			$(fieldAdresses).find('.es-input').data('value',$(li).attr('value'));
			
			var target_intitule=$(fieldAdresses).find('.es-input').data("field-intitule");
			var target_nom1=$(fieldAdresses).find('.es-input').data("field-nom1");
			var target_nom2=$(fieldAdresses).find('.es-input').data("field-nom2");
			var target_nom3=$(fieldAdresses).find('.es-input').data("field-nom3");
			var target_adresse1=$(fieldAdresses).find('.es-input').data("field-adresse1");
			var target_adresse2=$(fieldAdresses).find('.es-input').data("field-adresse2");
			var target_adresse3=$(fieldAdresses).find('.es-input').data("field-adresse3");
			var target_code_postal=$(fieldAdresses).find('.es-input').data("field-code-postal");
			var target_ville=$(fieldAdresses).find('.es-input').data("field-ville");
			var target_web=$(fieldAdresses).find('.es-input').data("field-web");
			var target_email=$(fieldAdresses).find('.es-input').data("field-email");
			var target_tel=$(fieldAdresses).find('.es-input').data("field-tel");
			var target_fax=$(fieldAdresses).find('.es-input').data("field-fax");
			var target_adress_id=$(fieldAdresses).find('.es-input').data('adress-field-name');

			
			if(target_intitule.length)
			{
				$('input[name='+target_intitule+']').val($(li).data('intitule'));
			}
			
			if(target_nom1.length)
			{
				$('input[name='+target_nom1+']').val($(li).data('nom1'));
			}
			
			if(target_nom2.length)
			{
				$('input[name='+target_nom2+']').val($(li).data('nom2'));
			}
			
			if(target_nom3.length)
			{
				$('input[name='+target_nom3+']').val($(li).data('nom3'));
			}
			
			if(target_adresse1.length)
			{
				$('input[name='+target_adresse1+']').val($(li).data('adresse1'));
			}
			if(target_adresse2.length)
			{
				$('input[name='+target_adresse2+']').val($(li).data('adresse2'));
			}
			if(target_adresse3.length)
			{
				$('input[name='+target_adresse3+']').val($(li).data('adresse3'));
			}
			
			if(target_code_postal.length)
			{
				$('input[name='+target_code_postal+']').val($(li).data('code-postal'));
			}
			
			
			if(target_ville.length)
			{
				$('input[name='+target_ville+']').val($(li).data('ville'));
			}
			
			if(target_web.length)
			{
				$('input[name='+target_web+']').val($(li).data('web'));
			}
			


			
			if(target_email.length)
			{
				$('input[name='+target_email+']').val($(li).data('email'));
			}

			if(target_tel.length)
			{
				$('input[name='+target_tel+']').val($(li).data('tel'));
			}
			
			if(target_fax.length)
			{
				$('input[name='+target_fax+']').val($(li).data('fax'));
			}

			$('input[name='+target_adress_id+']').val($(li).data('id'));
	    });
		
			
		
		
		$(fieldAdresses).find('.icon-contact').click(function(){
			$(fieldAdresses).find('.es-input').focus();
			console.log($(fieldAdresses));
			
		});
	})


	
});

$( document ).on( "EditableSelect.created", function(event, item) {
	if(item.data("type")=="adresses"){
		var field=item;
		$(field).on('blur', function (e) {
			
			var title=$(field).data('last-title');
			var original_value=$(field).data('original-value');
			
			if(original_value.length && original_value!=$(field).val()){
				var content ='<span class="">Modification de l\'adresse : </span><span class="font-weight-bold">'+original_value+'</span>';
			    content+='<br><span class="">Cette adresse correspond-elle toujours à votre établissement/entreprise/adminitration?</span>';
			    var promise = $.delefconfirm('<span class="font-weight-bold text-uppercase">'+title+'</span>',content,'non, supprimez cette adresse','oui',true);
		        promise.done(function() {
		        	//Contact still in charge. Nothing to do.
		        	field_id_name=$(field).data('adress-field-name');
		        	$('#'+field_id_name+'_question').val('1');
		        });
		        promise.fail(function() {
		        	//contact should be erased from directory
		        	//$.post( "/api-contact-remove", { name: original_value } );
		        	field_id_name=$(field).data('adress-field-name');
		        	$('#'+field_id_name+'_question').val('0');
		        	
		        });
		        $(field).data('original-value','')
			}
		});
	
	}

});