<?php

namespace App\Source;

use Sonata\Exporter\Exception\InvalidMethodCallException;
use Sonata\Exporter\Source\SourceIteratorInterface;

class DBALStatementSourceIterator implements SourceIteratorInterface
{
    /**
     * @var \Doctrine\DBAL\Statement
     */
    protected $statement;

    /**
     * @var mixed
     */
    protected $current;

    /**
     * @var int
     */
    protected $position;

    /**
     * @var bool
     */
    protected $rewinded;

    /**
     * @param \Doctrine\DBAL\Statement $statement
     */
    public function __construct(\Doctrine\DBAL\Statement $statement)
    {
        $this->statement = $statement;
        $this->position = 0;
        $this->rewinded = false;
    }

    /**
     * {@inheritdoc}
     */
    public function current()
    {
        return $this->current;
    }

    /**
     * {@inheritdoc}
     */
    public function next()
    {
        $this->current = $this->statement->fetch(\Doctrine\DBAL\FetchMode::ASSOCIATIVE);
        ++$this->position;
    }

    /**
     * {@inheritdoc}
     */
    public function key()
    {
        return $this->position;
    }

    /**
     * {@inheritdoc}
     */
    public function valid()
    {
        return \is_array($this->current);
    }

    /**
     * {@inheritdoc}
     */
    public function rewind()
    {
        if ($this->rewinded) {
            throw new InvalidMethodCallException('Cannot rewind a PDOStatement');
        }

        $this->current = $this->statement->fetch(\Doctrine\DBAL\FetchMode::ASSOCIATIVE);
        $this->rewinded = true;
    }
}
