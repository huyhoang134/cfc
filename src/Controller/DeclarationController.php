<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;


use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use App\Entity\Wcocon;
use App\Entity\Wdeclar;
use App\Entity\Wtype;
use App\Entity\Chorus;
use App\Entity\Publication;
use App\Entity\Wcontact;
use App\Entity\Wadresses;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Sonata\MediaBundle\Entity\MediaManager; 
use Sonata\MediaBundle\Provider\FileProvider;
use Symfony\Component\HttpKernel\KernelInterface;
use Psr\Container\ContainerInterface;

class DeclarationController extends AbstractController
{
   
    
    /**
     * @Route("/declaration/{wtype_id}/{step}", requirements={"wtype_id"="\d+","step"="\d+"}, methods={"GET","POST"}, name="declaration-independant")
     *
     * @param int $step the step of the declaration
     *
     * @return array
     */
    
    public function indexIndependant($wtype_id, $step=1, Request $request, UserInterface $wcocon = NULL, ParameterBagInterface $params, \Swift_Mailer $mailer, KernelInterface $kernel, ContainerInterface $container, FileProvider $provider)
    {
        //$wcocon=$wdeclar->getWdeDossier();
       
        $WcoconRepository = $this->getDoctrine()->getRepository(Wcocon::class);
        $dconf = $WcoconRepository->getDeclarationConfiguration($wcocon, $wtype_id);
          
        if(empty($dconf)){
            return $this->redirectToRoute('index');
        }
        
        $coordonee=false;
        $declaration_conf=[];
        foreach($dconf as $d){
            if($d[0]->type_etape=='coordonnees' && $coordonee==false)
            {
                $declaration_conf[]=$d;
                $coordonee=true;
            }elseif(in_array($d[0]->wde_etat_declar, ['N','V', 'A']) && $d[0]->type_etape!='coordonnees'){
                $declaration_conf[]=$d;
            }
            
        }


        if(!$declaration_conf){
            return $this->redirectToRoute('index');
        }
        
        $this->request=$request;
        $this->wcocon=$wcocon;
        $this->params=$params;
        $this->mailer=$mailer;
        $this->kernel=$kernel;
        $this->container=$container;
        $this->provider=$provider;
    
        return $this->declaration($step, $declaration_conf, $wtype_id );

    }
    
    /**
     * @Route("/declarations/{step}", requirements={"step"="\d+"}, methods={"GET","POST"}, name="declaration")
     *
     * @param int $step the step of the declaration
     *
     * @return array
     */
    public function index($step=1, Request $request, UserInterface $wcocon = NULL, ParameterBagInterface $params, \Swift_Mailer $mailer, KernelInterface $kernel, ContainerInterface $container, FileProvider $provider)
    {
     
        //get the list of the configurations based on the wcocon object
        /** @array [conf1,conf2..] */
        $WcoconRepository = $this->getDoctrine()->getRepository(Wcocon::class);
        $dconf=$WcoconRepository->getDeclarationConfiguration($wcocon,false,false);

        $declaration_conf=[];
        $coordonnes=false;


        if(count($dconf)>1){
            foreach($dconf as $i=>$d){
                if(empty($coordonnes)&&$d[0]->type_etape=='coordonnees')
                {

                    $declaration_conf[]=$d;
                    $coordonnes=true;
                }elseif(in_array($d[0]->wde_etat_declar, ['N','V', 'A'])){
                    $declaration_conf[]=$d;
                }
            
            }
           
            $primary_declaration = $this->getDoctrine()->getRepository(Wdeclar::class)->findOneBy(["wde_declar" => $declaration_conf[1][0]->wde_declar]);
        }else{
            $primary_declaration = $this->getDoctrine()->getRepository(Wdeclar::class)->findOneBy(["wde_declar" => $dconf[0][0]->wde_declar]);
            $declaration_conf[]=$dconf[0];
        }
        

        
        $contrats=$WcoconRepository->getListContrats($declaration_conf);

        $this->request=$request;
        $this->wcocon=$wcocon;
        $this->params=$params;
        $this->mailer=$mailer;
        $this->kernel=$kernel;
        $this->container=$container;
        $this->provider=$provider;
        return $this->declaration($step, $declaration_conf);
 
    }

    private function declaration($step, $declaration_conf, $wtype_id=false){
        
        $request    = $this->request;
        $wcocon     = $this->wcocon;
        $params     = $this->params;
        $mailer     = $this->mailer;
        $kernel     = $this->kernel;
        $container  = $this->container;
        $provider   = $this->provider;
        
        $WcoconRepository = $this->getDoctrine()->getRepository(Wcocon::class);
        if(count($declaration_conf)>1){
            $primary_declaration = $this->getDoctrine()->getRepository(Wdeclar::class)->findOneBy(["wde_declar" => $declaration_conf[1][0]->wde_declar]);
        }else{
            $primary_declaration = $this->getDoctrine()->getRepository(Wdeclar::class)->findOneBy(["wde_declar" => $declaration_conf[0][0]->wde_declar]);
        }
       
        
        $contrats=$WcoconRepository->getListContrats($declaration_conf);
        
        //set the number of steps
        $step_num=count($declaration_conf);

        //check if the requested step is within the availlable steps
        if($step> ($step_num+1) || $step_num<1){
            return $this->redirectToRoute('index');
        }

        //check if the status of the declaration is N or V
        if(!in_array($primary_declaration->getWdeEtatDeclar(),['N','V','A'])){
            return $this->redirectToRoute('index');
        }

        
    if($step<=$step_num){
            /** @var FormInterface $form */
            $f = $this->createFormBuilder([]);
            
            $wde_dossier = $declaration_conf[$step-1][0]->wde_dossier;
            $wde_declar = $declaration_conf[$step-1][0]->wde_declar;
            $submit_data = $request->request->all();
            

            foreach($declaration_conf[$step-1] as $i=>$c){
                

                
                $db_data[$c->getSynchroField()]=$WcoconRepository->getValue($c->getSynchroField(), $wde_dossier, $wde_declar);
                $object_conf=$c->getTypeObjetConf();
                
                //populate data with db or submited data
                if (!empty($c->getSynchroField())) {
                    $data=empty($submit_data)?$db_data[$c->getSynchroField()]:@$submit_data[$c->getSynchroField()];
                }
               /* $db_data[$i]["wco_id_ct_legal"]=$WcoconRepository->getValue("wco_id_ct_legal", $c->wde_dossier, $c->wde_declar);
                $db_data[$i]["wco_id_ct_president"]=$WcoconRepository->getValue("wco_id_ct_president", $c->wde_dossier, $c->wde_declar);
                $db_data[$i]["wco_id_adr_siege"]=$WcoconRepository->getValue("wco_id_adr_siege", $c->wde_dossier, $c->wde_declar);
                $db_data[$i]["wde_id_ct_fact"]=$WcoconRepository->getValue("wde_id_ct_fact", $c->wde_dossier, $c->wde_declar);
                $db_data[$i]["wde_id_ct_nom_sec_gen"]=$WcoconRepository->getValue("wde_id_ct_nom_sec_gen", $c->wde_dossier, $c->wde_declar);
                $db_data[$i]["wde_id_ct_resp"]=$WcoconRepository->getValue("wde_id_ct_resp", $c->wde_dossier, $c->wde_declar);
                $db_data[$i]["wde_id_adr_fact"]=$WcoconRepository->getValue("wde_id_adr_fact", $c->wde_dossier, $c->wde_declar);
                */
                
                
                if(in_array($c->getTypeObjet(), ['text_max_25', "text_max_50", 'text_max_40', 'text_max_80', 'text_max_100', 'email','telephone','list','civilite','checkbox', 'radio'])){
                    $f->add($c->getSynchroField(), TextType::class, ['data'=>$data, 'required' => $c->getMandatory()]); 
                    if(!empty($object_conf['contact_field_id'])){
                        $db_data[$object_conf['contact_field_id']]=$WcoconRepository->getValue($object_conf['contact_field_id'], $c->wde_dossier, $c->wde_declar);
                        $db_data[$object_conf['contact_field_id'].'_question']=$WcoconRepository->getValue($object_conf['contact_field_id'], $c->wde_dossier, $c->wde_declar);
                    }
                    if(!empty($object_conf['adress_field_id'])){
                        $db_data[$object_conf['adress_field_id']]=$WcoconRepository->getValue($object_conf['adress_field_id'], $c->wde_dossier, $c->wde_declar);
                        $db_data[$object_conf['adress_field_id'].'_question']=$WcoconRepository->getValue($object_conf['adress_field_id'], $c->wde_dossier, $c->wde_declar);
                    }
                    
                }


                if(in_array($c->getTypeObjet(), ['num_2', "num_3", 'num_4', 'num_5','num_6','num_7', 'title_num_5','title_num_6','title_num_7','subtitle_num_5','subtitle_num_7', 'rgpd'])){ 
                    if(empty($data)){
                        $data=null;
                    }
                    $f->add($c->getSynchroField(), IntegerType::class, ['data'=>$data, 'required' => $c->getMandatory()]);
                }
                
                if(in_array($c->getTypeObjet(), ['zipcode'])){
                    if(empty($data)){
                        $data=null;
                    }
                    $f->add($c->getSynchroField(), TextType::class, ['data'=>$data, 'required' => $c->getMandatory()]);
                }
                
                
                if($c->getTypeObjet()=="chorus_pro" || $c->getTypeObjet()=="chorus_pro_extended"){
                    $db_data["wde_is_chorus_pro"]=$WcoconRepository->getValue("wde_is_chorus_pro", $wde_dossier, $wde_declar);
                    $data=empty($submit_data)?$db_data["wde_is_chorus_pro"]:@$submit_data["wde_is_chorus_pro"];
                    $f->add("wde_is_chorus_pro", TextType::class, ['data'=>$data, 'required' => $c->getMandatory()]);

                    $db_data["wde_siret"]=$WcoconRepository->getValue("wde_siret", $wde_dossier, $wde_declar);
                    $data=empty($submit_data)?$db_data["wde_siret"]:@$submit_data["wde_siret"];
                    $f->add("wde_siret", TextType::class, ['data'=>$data, 'required' => $c->getMandatory()]);
                    
                    $db_data["wde_service_code"]=$WcoconRepository->getValue("wde_service_code", $wde_dossier, $wde_declar);
                    $data=empty($submit_data)?$db_data["wde_service_code"]:@$submit_data["wde_service_code"];
                    $f->add("wde_service_code", TextType::class, ['data'=>$data, 'required' => $c->getMandatory()]);

                    $db_data["wde_is_order_number"]=$WcoconRepository->getValue("wde_is_order_number", $wde_dossier, $wde_declar);
                    $data=empty($submit_data)?$db_data["wde_is_order_number"]:@$submit_data["wde_is_order_number"];
                    $f->add("wde_is_order_number", TextType::class, ['data'=>$data, 'required' => $c->getMandatory()]);
                    
                    $db_data["wde_order_number"]=$WcoconRepository->getValue("wde_order_number", $wde_dossier, $wde_declar);
                    $data=empty($submit_data)?$db_data["wde_order_number"]:@$submit_data["wde_service_code"];
                    $f->add("wde_order_number", TextType::class, ['data'=>$data, 'required' => $c->getMandatory()]);
                
                }elseif($c->getTypeObjet()=='bon_commande'){
                    
                    $db_data["wde_is_order_number"]=$WcoconRepository->getValue("wde_is_order_number", $wde_dossier, $wde_declar);
                    $data=empty($submit_data)?$db_data["wde_is_order_number"]:@$submit_data["wde_is_order_number"];
                    $f->add("wde_is_order_number", TextType::class, ['data'=>$data, 'required' => $c->getMandatory()]);
                    
                    $db_data["wde_order_number"]=$WcoconRepository->getValue("wde_order_number", $wde_dossier, $wde_declar);
                    $data=empty($submit_data)?$db_data["wde_order_number"]:@$submit_data["wde_service_code"];
                    $f->add("wde_order_number", TextType::class, ['data'=>$data, 'required' => $c->getMandatory()]);
                    
                }elseif($c->getTypeObjet()=="panorama_press_numerique" || $c->getTypeObjet()=="panorama_press_papier"){
                   
                }elseif($c->getTypeObjet()=="file_upload")
                {
                    if(!empty($data)){
                        $mediaManager = $container->get('sonata.media.manager.media');
                        $media=$mediaManager->find($data);
                        $file_name=$media->getName();
                        $file_reference=$provider->getReferenceFile($media,'reference')->getName();
                        $media_key=md5($data.$WcoconRepository->download_key).'%'.$data;
                        $db_data['file_upload'][$c->getSynchroField()]=[
                            'file_upload_id'=>$data,
                            'file_upload_fichier_name'=>$file_name,
                            'file_upload_fichier_reference'=>$file_reference,
                            'file_upload_fichier_media_key'=>$media_key,
                        ];
                    }else{
                        $db_data['file_upload'][$c->getSynchroField()]=[
                            'file_upload_id'=>'',
                            'file_upload_fichier_name'=>"",
                            'file_upload_fichier_reference'=>"",
                            'file_upload_fichier_media_key'=>"",
                        ];
                    }
                }elseif($c->getTypeObjet()=="accordion_options")
                {
                        $TypeObjetConf=$c->getTypeObjetConf();
                        $db_data[$TypeObjetConf['accordion_synchro_field_1']]=$WcoconRepository->getValue($TypeObjetConf['accordion_synchro_field_1'], $wde_dossier, $wde_declar);
                        $db_data[$TypeObjetConf['accordion_synchro_field_2']]=$WcoconRepository->getValue($TypeObjetConf['accordion_synchro_field_2'], $wde_dossier, $wde_declar);
                }

            }
            
            if(!empty($db_data["wde_siret"])){
                $ChorusRepository = $this->getDoctrine()->getRepository(Chorus::class);
                $chorus_pro_service_list=$ChorusRepository->findServicesBySiret( $db_data["wde_siret"] );
            }else{
                $chorus_pro_service_list=[];
            }

            $f->add('submit', SubmitType::class, ['label' => 'VALIDER']);
            $form = $f->getForm();
            
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $submit_data = $request->request->all();
                $WcoconRepository->updateForm($submit_data, $declaration_conf[$step-1]);
                if(!empty($wtype_id)){
                    return $this->redirectToRoute('declaration-independant', ['wtype_id'=>$wtype_id, 'step' => $step+1]);
                }else{
                    return $this->redirectToRoute('declaration', ['step' => $step+1]);
                }
                
            }

        }else{
            //place validation proccessing declaration here. Change status of declaratio + send email confirmation

            foreach($declaration_conf as $i=>$conf){
                foreach($conf as $j=>$c){      
                    if($c->getTypeObjet()=="chorus_pro" || $c->getTypeObjet()=="chorus_pro_extended"){
                        $db_data_conf[$i]["wde_is_chorus_pro"]=$WcoconRepository->getValue("wde_is_chorus_pro", $c->wde_dossier, $c->wde_declar);
                        $db_data_conf[$i]["wde_siret"]=$WcoconRepository->getValue("wde_siret", $c->wde_dossier, $c->wde_declar);
                        $db_data_conf[$i]["wde_service_code"]=$WcoconRepository->getValue("wde_service_code", $c->wde_dossier, $c->wde_declar);
                        $db_data_conf[$i]["wde_is_order_number"]=$WcoconRepository->getValue("wde_is_order_number", $c->wde_dossier, $c->wde_declar);
                        $db_data_conf[$i]["wde_order_number"]=$WcoconRepository->getValue("wde_order_number", $c->wde_dossier, $c->wde_declar);
                    }elseif($c->getTypeObjet()=='bon_commande'){
                        $db_data_conf[$i]["wde_is_order_number"]=$WcoconRepository->getValue("wde_is_order_number", $c->wde_dossier, $c->wde_declar);
                        $db_data_conf[$i]["wde_order_number"]=$WcoconRepository->getValue("wde_order_number", $c->wde_dossier, $c->wde_declar);
                    }elseif($c->getTypeObjet()=="panorama_press_numerique"){    
                        $db_data_conf[$i]["panorama_press"]=$WcoconRepository->getValue("panorama_press", $c->wde_dossier, $c->wde_declar);
                    }elseif($c->getTypeObjet()=="panorama_press_papier"){
                        $db_data_conf[$i]["panorama_press"]=$WcoconRepository->getValue("panorama_press", $c->wde_dossier, $c->wde_declar);
                    }elseif($c->getTypeObjet()=="file_upload")
                    {
                        $db_data_conf[$i][$c->getSynchroField()]=$WcoconRepository->getValue($c->getSynchroField(), $c->wde_dossier, $c->wde_declar);
                        if(!empty($db_data_conf[$i][$c->getSynchroField()])){
                            $mediaManager = $container->get('sonata.media.manager.media');
                            $media=$mediaManager->find($db_data_conf[$i][$c->getSynchroField()]);
                            $file_name=$media->getName();
                            $file_reference=$provider->getReferenceFile($media,'reference')->getName();
                            $media_key=md5($db_data_conf[$i][$c->getSynchroField()].$WcoconRepository->download_key).'%'.$db_data_conf[$i][$c->getSynchroField()];
                            $db_data_conf[$i]['file_upload'][$c->getSynchroField()]=[
                                'file_upload_id'=>$db_data_conf[$i][$c->getSynchroField()],
                                'file_upload_fichier_name'=>$file_name,
                                'file_upload_fichier_reference'=>$file_reference,
                                'file_upload_fichier_media_key'=>$media_key,
                            ];
                        }else{
                            $db_data_conf[$i]['file_upload'][$c->getSynchroField()]=[
                                'file_upload_id'=>'',
                                'file_upload_fichier_name'=>"",
                                'file_upload_fichier_reference'=>"",
                                'file_upload_fichier_media_key'=>"",
                            ];
                        }
                    }elseif($c->getTypeObjet()=="accordion_options")
                    {
                        $TypeObjetConf=$c->getTypeObjetConf();
                        $db_data_conf[$i][$TypeObjetConf['accordion_synchro_field_1']]=$WcoconRepository->getValue($TypeObjetConf['accordion_synchro_field_1'], $c->wde_dossier, $c->wde_declar);
                        $db_data_conf[$i][$TypeObjetConf['accordion_synchro_field_2']]=$WcoconRepository->getValue($TypeObjetConf['accordion_synchro_field_2'], $c->wde_dossier, $c->wde_declar);
                    }else{
                        if (!empty($c->getSynchroField())) {
                           $db_data_conf[$i][$c->getSynchroField()]=$WcoconRepository->getValue($c->getSynchroField(), $c->wde_dossier, $c->wde_declar);
                        }
                    }
                }
            }
           
            
            $f = $this->createFormBuilder(['multipart'=>true]);
            $f->add('submit', SubmitType::class, ['label' => 'Envoyer']);
            $form = $f->getForm();
            
            
            
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                //update wdeclar set wde_etat_declar='V', wde_email_sent=0,wde_date_validation=now() where wde_dossier =?
                $em=$this->getDoctrine()->getManager();

                $qb=$this->getDoctrine()->getRepository(Wdeclar::class)->createQueryBuilder("d")
                
                ->where('d.wde_dossier = :wde_dossier')
                ->setParameter('wde_dossier', $declaration_conf[0][0]->wde_dossier);
                $declars=$qb->getQuery()->getResult();
                $arr_wde_contrat=[];
                foreach($declaration_conf as $c){
                    $arr_wde_contrat[]=$c[0]->wde_contrat;
                   
                }
                
                
                foreach($declars as $declar){

                    if(in_array($declar->getWdeContrat(), $arr_wde_contrat)){
                        $declar->setWdeEtatDeclar('V');
                        $declar->setWdeEmailSent(0);
                        $declar->setWdeDateValidation(new \DateTime());
                        $em->persist($declar);
                        $em->flush();
                    }
                    
                }
                
                $wtype = $this->getDoctrine()
                ->getRepository(Wtype::class)
                ->findOneBy(['label' => $primary_declaration->getWdeTypcont()]);

                $mail_content = $this->renderView(
                    'emails/accuse_reception.html.twig', [
                        'signature' => $wtype->getEmailSignature(),
                        'declaration_conf'=>$declaration_conf,
                        'db_data_conf'=>$db_data_conf,
                        'contrats'=>$contrats,
                        'primary_declaration'=>$primary_declaration
                    ]
                    );
                
                foreach($declaration_conf as $i=>$conf){
                    foreach($conf as $j=>$c){
                        switch ($c->getValidationEmail()) {
                            case 1:
                                $primary = $this->cleanEmail($db_data_conf[$i][$c->getSynchroField()]);
                                break;
                            case 2:
                                $secondary = $this->cleanEmail($db_data_conf[$i][$c->getSynchroField()]);
                                break;
                            case 3:
                                $secondary_bis = $this->cleanEmail($db_data_conf[$i][$c->getSynchroField()]);
                                break;
                        }
                    }
                }
                

                $recipients = [];
                //if primary is filled => primary
                if (!empty($primary)) {
                    $recipients[] = $primary;
                }else{
                    //if primary is empty => secondary + secondary bis
                    if(!empty($secondary)){
                        $recipients[] = $secondary;
                    }
                    if(!empty($secondary_bis)){
                        $recipients[] = $secondary_bis;
                    }
                }


                
                 $from_email = $params->get('delef_from_email');
                 $from_name = $params->get('delef_from_name');
                 $message = (new \Swift_Message('Declaration CFC – Accusé de réception'))
                 ->setFrom($from_email, $from_name)
                 ->setTo($recipients)
                 ->setBcc([
                     /*'tbourdin@partitech.com' => 'Thomas Bourdin',*/
                     'f.zelie@cfcopies.com' => 'Frantz ZELIE',
                     'e.kamm@cfcopies.com' => 'Elise KAMM'
                    ])
                 ->setBody($mail_content, 'text/html')
                 ;
                 //sm($db_data_conf);
                 $upload_files_arr=[];
                 foreach($db_data_conf as $conf){
                     if(!empty($conf['panorama_press'])){
                         foreach($conf['panorama_press']["wpanoramapress_liste"] as $pano){
                             if(!empty($pano['wppl_fichier'])){
                                 $filename=$pano['wppl_fichier_name'];
                                 $upload_files_arr[]=['filename'=>$filename,'file_ref'=>$pano['wppl_fichier_media_reference']];
                                 $projectRoot = $kernel->getProjectDir();
                                 $message->attach(
                                     \Swift_Attachment::fromPath($projectRoot.'/public/upload/media/'.$pano['wppl_fichier_media_reference'])->setFilename($filename)
                                     );
                             }
                         }
                     }
                    /* if(!empty($conf['panorama_press'])){
                         foreach($conf['panorama_press']["wpanoramapress_liste"] as $pano){
                             if(!empty($pano['wppl_fichier'])){
                                 $filename=$pano['wppl_fichier_name'];
                                 $projectRoot = $kernel->getProjectDir();
                                 $message->attach(
                                     \Swift_Attachment::fromPath($projectRoot.'/public/upload/media/'.$pano['wppl_fichier_media_reference'])->setFilename($filename)
                                     );
                             }
                         }
                     }*/
                     if(!empty($conf['abo_press_livres'])){
                         if(!empty($conf['abo_press_livres']['wabo_fichier'])){
                             $filename=$conf['abo_press_livres']['wabo_fichier_name'];
                             $upload_files_arr[]=['filename'=>$filename,'file_ref'=> $conf['abo_press_livres']['wabo_fichier_media_reference']];
                             $projectRoot = $kernel->getProjectDir();
                             $message->attach(
                                 \Swift_Attachment::fromPath($projectRoot.'/public/upload/media/'.$conf['abo_press_livres']['wabo_fichier_media_reference'])->setFilename($filename)
                                 );
                         }
                     }
                
                     if(!empty($conf["file_upload"])){
                         foreach($conf["file_upload"] as $file)
                         {
                             $filename=$file['file_upload_fichier_name'];
                             $upload_files_arr[]=['filename'=>$filename,'file_ref'=> $file['file_upload_fichier_reference']];
                             $projectRoot = $kernel->getProjectDir();
                             $message->attach(
                                 \Swift_Attachment::fromPath($projectRoot.'/public/upload/media/'.$file['file_upload_fichier_reference'])->setFilename($filename)
                                 );
                         }
                     }
                  }
                 $ret=$mailer->send($message);
                 
                 
                 /* Envois de la copie de la déclaration au responsable du contrat*/
                 if($wtype->getSendValidationEmail())
                 {
                     $from_email = $params->get('delef_from_email');
                     $from_name = $params->get('delef_from_name');
                     $message = (new \Swift_Message('Declaration CFC - Nouvelle déclaration'))
                     ->setFrom($from_email, $from_name)
                     ->setTo([$wtype->getContactEmail()])
                     ->setBody($mail_content, 'text/html');
                     if(!empty($wtype->getContactEmail2())){
                         $message->setCc([$wtype->getContactEmail2()]);
                     }
                     foreach($declars as $declar){
                         //$pdf_file_path = $WcoconRepository->GeneratePdf($declar->getWdeDeclar(), true);
                         $pdf_file_path = $WcoconRepository->GenerateExcell($declar->getWdeDeclar());
                         
                         if ($pdf_file_path) {
                             $message->attach(\Swift_Attachment::fromPath($pdf_file_path['path'])->setFilename($declaration_conf[0][0]->wde_dossier.'_'.date('Y-m-d').'.xlsx'));
                         }

                     }

                     
                     foreach($upload_files_arr as $file)
                     {
                         $projectRoot = $kernel->getProjectDir();
                         $message->attach(
                             \Swift_Attachment::fromPath($projectRoot.'/public/upload/media/'.$file['file_ref'])->setFilename($file['filename'])
                             );
                     }
                     
                     
                     

                     
                     $ret=$mailer->send($message);
                    
                 }
                 
                
                 
                 
                 
                 
                 
                 
                 
                 
                 if(!empty($wtype_id)){
                     return $this->redirectToRoute('validation-success', ['wtype_id'=>$wtype_id]);
                 }else{
                     return $this->redirectToRoute('validations-success');
                 }

            }
            
        }
            /*sm($db_data);
            die();*/

            if($step<=$step_num){
                $conf=$declaration_conf[$step-1];

                $wde_dossier=(!empty($declaration_conf[1])?$declaration_conf[1][0]->wde_dossier:$declaration_conf[0][0]->wde_dossier);
                return $this->render($conf[0]->type_etape=='coordonnees'?'interface/declaration/coordinate.html.twig':'interface/declaration/declaration.html.twig', [
                    'wtype_id'=>!empty($wtype_id)?$wtype_id:false,
                    'primary_declaration'=>$primary_declaration,
                    'wcocon' => $wcocon,
                    'db_data'    => $db_data,
                    'WcoconRepository' => $WcoconRepository,
                    'conf'      => $conf,
                    'form'      => $form->createView(),
                    'contrats'  => $contrats,
                    'step'      => $step,
                    'declaration_conf'=>$declaration_conf,
                    'chorus_pro_service_list'=>$chorus_pro_service_list,
                    'WtypeCont'     => $declaration_conf[0][0]->getWconfTypcont(),
                    'contacts' =>  $this->getDoctrine()->getRepository(Wcontact::class)->findBy(["fkcocontractant" => $wde_dossier, 'actif'=>'O']),
                    'adresses' =>  $this->getDoctrine()->getRepository(Wadresses::class)->findBy(["fkcocontractant" => $wde_dossier /*, 'actif'=>'O'*/])
                ]);
            }else{

                
                return $this->render('interface/declaration/validation.html.twig', [
                    'form'      => $form->createView(),
                    'db_data_conf'=>$db_data_conf,
                    'primary_declaration'=>$primary_declaration,
                    'WcoconRepository' => $WcoconRepository,
                    'wcocon'    => $wcocon,
                    'conf'      => $declaration_conf,
                    'declaration_conf'      => $declaration_conf,
                    'contrats'  => $contrats,
                    'step'      => $step,
                    'WtypeCont'     => $declaration_conf[0][0]->getWconfTypcont(),
                    'wtype_id' => $wtype_id
                ]);
            }
        

    }
 

        /**
         * @Route("/declaration/validation-de-votre-declaration/{wtype_id}", requirements={"wtype_id"="\d+"}, methods={"GET"}, name="validation-success")
         *
         */
        public function ValidationSuccess($wtype_id, Request $request, UserInterface $wcocon = NULL)
        {
            //$wcocon=$wdeclar->getWdeDossier();
            $WcoconRepository = $this->getDoctrine()->getRepository(Wcocon::class);
            $dconf = $WcoconRepository->getDeclarationConfiguration($wcocon, $wtype_id);
            
            $coordonee=false;
            $declaration_conf=[];
            foreach($dconf as $d){
                if($d[0]->type_etape=='coordonnees' && $coordonee==false)
                {
                    $declaration_conf[]=$d;
                    $coordonee=true;
                }elseif(in_array($d[0]->wde_etat_declar, ['N','V', 'A']) && $d[0]->type_etape!='coordonnees'){
                    $declaration_conf[]=$d;
                }
            
            }
            
            
            $primary_declaration = $this->getDoctrine()->getRepository(Wdeclar::class)->findOneBy(["wde_declar" => $declaration_conf[1][0]->wde_declar]);
            $contrats=$WcoconRepository->getListContrats($declaration_conf);
            
            
            
            
            $primary_declaration = $this->getDoctrine()
            ->getRepository(Wdeclar::class)
            ->findOneBy(["wde_declar" => $declaration_conf[1][0]->wde_declar]);
            
            $contract=$WcoconRepository->getContract($wcocon->getUsername());
            $wtype=$contract['wtype'];
            
            
            return $this->render('interface/declaration/validation-success.html.twig', [
                'wtype_id'=>!empty($wtype_id)?$wtype_id:false,
                'contrats'=>$contrats,
                'primary_declaration'=>$primary_declaration,
                'wcocon'    => $wcocon,
                'declaration_conf'=>$declaration_conf,
                'step'=>count($declaration_conf)+1,
                'wtype'=>$wtype,
                'wdeclar' => $primary_declaration,
                'WtypeCont'     => $declaration_conf[0][0]->getWconfTypcont()
                
            ]);
        }

        /**
         * @Route("/declaration/validation-de-vos-declaration", methods={"GET"}, name="validations-success")
         *
         */
        public function ValidationsSuccess(Request $request, UserInterface $wcocon = NULL)
        {
            //$wcocon=$wdeclar->getWdeDossier();
            $WcoconRepository = $this->getDoctrine()->getRepository(Wcocon::class);
            $declaration_conf = $WcoconRepository->getChainedDeclarationConfiguration($wcocon);
            $wde_declar=(!empty($declaration_conf[1])?$declaration_conf[1][0]->wde_declar:$declaration_conf[0][0]->wde_declar);
            $primary_declaration = $this->getDoctrine()->getRepository(Wdeclar::class)->findOneBy(["wde_declar" => $wde_declar]);
            $contrats=$WcoconRepository->getListContrats($declaration_conf);
        
            $primary_declaration = $this->getDoctrine()
            ->getRepository(Wdeclar::class)
            ->findOneBy(["wde_declar" => $wde_declar]);
        
            $contract=$WcoconRepository->getContract($wcocon->getUsername());
            $wtype=$contract['wtype'];
        
        
            return $this->render('interface/declaration/validation-success.html.twig', [
                'contrats'=>$contrats,
                'primary_declaration'=>$primary_declaration,
                'wcocon'    => $wcocon,
                'declaration_conf'=>$declaration_conf,
                'step'=>count($declaration_conf)+1,
                'wtype'=>$wtype,
                'wdeclar' => $primary_declaration,
                'WtypeCont'     => $declaration_conf[0][0]->getWconfTypcont()
        
            ]);
        }
        
    
        /**
         * @Route("/api-chorus-pro-siret", methods={"GET","POST"}, name="api-chorus-pro-siret")
         *
         *
         * @return array
         */
        public function ApiChorusproSiret($query=false, Request $request, UserInterface $wcocon = NULL)
        {         
            $keyword=$request->query->get("query");
            
            
            $ChorusRepository = $this->getDoctrine()->getRepository(Chorus::class);
            $declaration_conf = $ChorusRepository->findBySiret($keyword);

            $suggestions=[];
            foreach($declaration_conf as $c){
                $suggestions[]=[
                    "value"=>$c->getIdentifiant().' - '.$c->getRaisonSociale().' - '.$c->getCodePostal().' '.$c->getVille(), 
                    "data"=> $c->getIdentifiant(),
                    "raison_sociale"=>$c->getRaisonSociale(),
                    "adresse"=>$c->getAdresse(),
                    "code_postal"=>$c->getCodePostal(),
                    "ville"=>$c->getVille(),
                    "gestion_engagement"=>$c->getGestionEngagement(),
                    "gestion_service"=>$c->getGestionService(),
                    "gestion_service_engagement"=>$c->getGestionServiceEngagement(),
                    "service_gestion_egmt"=>$c->getserviceGestionEgmt(),
                    "services"=>$ChorusRepository->findServicesBySiret( $c->getIdentifiant() )
                ];
            }
            return new JsonResponse( (object) [
              "query"=> $keyword,
                "suggestions"=>$suggestions
            ]);
        }

        
        /**
         * @Route("/api-panorama-press-publications-ipro", methods={"GET","POST"}, name="api-panorama-press-publications-ipro")
         *
         *
         * @return array
         */
        public function ApiPanoramaPressPublicationsIpro($query=false, Request $request, UserInterface $wcocon = NULL)
        {
            $keyword=$request->query->get("query");
        
        
            $PublicationRepository = $this->getDoctrine()->getRepository(Publication::class);
            $Publications = $PublicationRepository->findPublicationFusionIproBykeyword($keyword);
            $pub=[];
            foreach($Publications as $p)
            {
                $domain_name = $p['value'];
            
                if(!$this->is_valid_domain($domain_name))
                {
                    $pub[]=['value'=>$domain_name];
                }
            }
            return new JsonResponse( (object) [
                "query"=> $keyword,
                "suggestions"=>$pub
            ]);
        }
        
        
        
        /**
         * @Route("/api-panorama-press-publications", methods={"GET","POST"}, name="api-panorama-press-publications")
         *
         *
         * @return array
         */
        public function ApiPanoramaPressPublications($query=false, Request $request, UserInterface $wcocon = NULL)
        {
            $keyword=$request->query->get("query");
        
        
            $PublicationRepository = $this->getDoctrine()->getRepository(Publication::class);
            $Publications = $PublicationRepository->findPublicationBykeyword($keyword);
            $pub=[];
            foreach($Publications as $p)
            {
                $domain_name = $p['value'];
            
                if(!$this->is_valid_domain($domain_name))
                {
                    $pub[]=['value'=>$domain_name];
                }
            }
            return new JsonResponse( (object) [
                "query"=> $keyword,
                "suggestions"=>$pub
            ]);
        }       
      
                
         
        /**
         * @Route("/api-panorama-press-publications-site", methods={"GET","POST"}, name="api-panorama-press-publications-site")
         *
         *
         * @return array
         */
        public function ApiPanoramaPressPublicationsSite($query=false, Request $request, UserInterface $wcocon = NULL)
        {
            $keyword=$request->query->get("query");
        
        
            $PublicationRepository = $this->getDoctrine()->getRepository(Publication::class);
            $Publications = $PublicationRepository->findPublicationBykeyword($keyword);
        

            $pub=[];
            foreach($Publications as $p)
            {
                $domain_name = $p['value'];
                
                if($this->is_valid_domain($domain_name))
                {
                    $pub[]=['value'=>$domain_name];
                }
            }
            return new JsonResponse( (object) [
                "query"=> $keyword,
                "suggestions"=>$pub
            ]);
        }
        
        
        /**
         * @Route("/api-repertoire-cne-presse-ciblees", methods={"GET","POST"}, name="api-repertoire-cne-presse-ciblees")
         *
         *
         * @return array
         */
        public function ApiRepertoireCneCiblees($query=false, Request $request, UserInterface $wcocon = NULL)
        {
            $keyword=$request->query->get("query");
        
        
            $PublicationRepository = $this->getDoctrine()->getRepository(Publication::class);
            $Publications = $PublicationRepository->findPublicationCopieNumCibleBykeyword($keyword);
        
        
            $pub=[];
            foreach($Publications as $p)
            {
                $pub[]=['value'=>$p['value']];
               
            }
            return new JsonResponse( (object) [
                "query"=> $keyword,
                "suggestions"=>$pub
            ]);
        }
        
        /**
         * @Route("/api-repertoire-cne-web", methods={"GET","POST"}, name="api-repertoire-cne-web")
         *
         *
         * @return array
         */
        public function ApiRepertoireCneWeb($query=false, Request $request, UserInterface $wcocon = NULL)
        {
            $keyword=$request->query->get("query");
        
        
            $PublicationRepository = $this->getDoctrine()->getRepository(Publication::class);
            $Publications = $PublicationRepository->findPublicationCopieNumWebBykeyword($keyword);
        
        
            $pub=[];
            foreach($Publications as $p)
            {
                $pub[]=['value'=>$p['value']];
            }
            return new JsonResponse( (object) [
                "query"=> $keyword,
                "suggestions"=>$pub
            ]);
        }
        
        /**
         * @Route("/api-contact-remove", methods={"POST"}, name="api-contact-remove")
         *
         *
         * @return array
         */
        public function ApiContactRemove(Request $request, UserInterface $wcocon = NULL)
        {
            //$wcocon=$wdeclar->getWdeDossier();
            
            $posts=$request->request->all();
            if(!empty($posts) && !empty($posts['name']))
            {
                $cn = $this->getDoctrine()->getConnection();
                $stmt = $cn->prepare("update wContact set Actif='N'  where  FkCocontractant=:FkCocontractant and  NomPrenom=:NomPrenom ;");
                 
                $rows=$stmt->execute(array(
                    'FkCocontractant'=>$wcocon->getWcoDossier(),
                    'NomPrenom'=>$posts['name'],
                ));
                return new JsonResponse( (object) [
                    "return"=> $rows,
                ]);
            }
            
        }
        
        
        
        
        
        
      

    /**
     * @Route("/pdf/declar/{WdeDeclar}", name="app_pdf_declaration")
     */
    public function Pdf(int $WdeDeclar)
    {
        $session= $this->get('session')->all();
        if(!empty($session['_security_user'])){
            $user=unserialize($session['_security_user']);
            if(in_array('ROLE_USER', $user->getRoleNames())){
                if ($WdeDeclar) {
                    $WcoconRepository = $this->getDoctrine()->getRepository(Wcocon::class);
                    $pdf_file = $WcoconRepository->GeneratePdf($WdeDeclar);
                    if (isset($pdf_file['path'])) {
                        return new BinaryFileResponse($pdf_file['path']);
                    }
                }
            }
        }
        return $this->redirectToRoute('index');
        
    }
    
    /**
     * @Route("/pdf/excell/{WdeDeclar}", name="app_excell_declaration")
     */
    public function Excell(int $WdeDeclar)
    {
        $session= $this->get('session')->all();
        if(!empty($session['_security_user'])){
            $user=unserialize($session['_security_user']);
            if(in_array('ROLE_USER', $user->getRoleNames())){
                if ($WdeDeclar) {
                    $WcoconRepository = $this->getDoctrine()->getRepository(Wcocon::class);
                    $ExcellFile = $WcoconRepository->GenerateExcell($WdeDeclar);
                    
                    
                    header('Content-Type: application/vnd.ms-excel');
                    header("Content-Transfer-Encoding: Binary");
                    header("Content-disposition: attachment; filename=\"" . $ExcellFile['name'] . "\"");
                    //header("Content-Length: ".$media->getSize());
                    readfile($ExcellFile['path']);
                    exit();
                    
                   /* if (isset($pdf_file['path'])) {
                        return new BinaryFileResponse($pdf_file['path']);
                    }*/
                }
            }
        }
        return $this->redirectToRoute('index');
    
    }
    
    
    /**
     * @Route("/declaration/admin-abo-presse-livre/{wde_declar}", name="declaration_admin_abo_presse_livre_download")
     */
    public function AdminAboPresseLivreDownload(int $wde_declar, MediaManager $mediaManager, FileProvider $provider, KernelInterface $kernel)
    {
    
         
    
        $session= $this->get('session')->all();
        if(!empty($session['_security_user'])){
            $user=unserialize($session['_security_user']);
            if(in_array('ROLE_USER', $user->getRoleNames())){
                 
                $WcoconRepository = $this->getDoctrine()->getRepository(Wcocon::class);
                $ExcellFile = $WcoconRepository->GenerateExcellAboPresseLivre($wde_declar);
    
                header('Content-Type: application/vnd.ms-excel');
                header("Content-Transfer-Encoding: Binary");
                header("Content-disposition: attachment; filename=\"" . $ExcellFile['name'] . "\"");
                //header("Content-Length: ".$media->getSize());
                readfile($ExcellFile['path']);
                exit();
            }
    
        }
    
        return $this->redirectToRoute('index');
    }
    
    /**
     * @Route("/declaration/admin-abo-presse/{wde_declar}", name="declaration_admin_abo_presse_download")
     */
    public function AdminAboPresseDownload(int $wde_declar, MediaManager $mediaManager, FileProvider $provider, KernelInterface $kernel)
    {
    
         
    
        $session= $this->get('session')->all();
        if(!empty($session['_security_user'])){
            $user=unserialize($session['_security_user']);
            if(in_array('ROLE_USER', $user->getRoleNames())){
                 
                $WcoconRepository = $this->getDoctrine()->getRepository(Wcocon::class);
                $ExcellFile = $WcoconRepository->GenerateExcellAboPresse($wde_declar);
    
                header('Content-Type: application/vnd.ms-excel');
                header("Content-Transfer-Encoding: Binary");
                header("Content-disposition: attachment; filename=\"" . $ExcellFile['name'] . "\"");
                //header("Content-Length: ".$media->getSize());
                readfile($ExcellFile['path']);
                exit();
            }
    
        }
    
        return $this->redirectToRoute('index');
    }
    
    /**
     * @Route("/declaration/admin-relation-publique/{wde_declar}", name="declaration_admin_relation_publique_download")
     */
    public function AdminRelationPubliqueDownload(int $wde_declar, MediaManager $mediaManager, FileProvider $provider, KernelInterface $kernel)
    {
        
   
        
        $session= $this->get('session')->all();
        if(!empty($session['_security_user'])){
            $user=unserialize($session['_security_user']);
            if(in_array('ROLE_USER', $user->getRoleNames())){
                 
                $WcoconRepository = $this->getDoctrine()->getRepository(Wcocon::class);
                $ExcellFile = $WcoconRepository->GenerateExcellRelationPublique($wde_declar);
    
                header('Content-Type: application/vnd.ms-excel');
                header("Content-Transfer-Encoding: Binary");
                header("Content-disposition: attachment; filename=\"" . $ExcellFile['name'] . "\"");
                //header("Content-Length: ".$media->getSize());
                readfile($ExcellFile['path']);
                exit();
            }
    
        }
    
        return $this->redirectToRoute('index');
    }
    
    /**
     * @Route("/declaration/admin-cma-btp/{wde_declar}", name="declaration_admin_cma_btp_download")
     */
    public function AdminCmaBtpDownload(int $wde_declar, MediaManager $mediaManager, FileProvider $provider, KernelInterface $kernel)
    {
    
         
    
        $session= $this->get('session')->all();
        if(!empty($session['_security_user'])){
            $user=unserialize($session['_security_user']);
            if(in_array('ROLE_USER', $user->getRoleNames())){
                 
                $WcoconRepository = $this->getDoctrine()->getRepository(Wcocon::class);
                $ExcellFile = $WcoconRepository->GenerateExcellCmaBtp($wde_declar);
    
                header('Content-Type: application/vnd.ms-excel');
                header("Content-Transfer-Encoding: Binary");
                header("Content-disposition: attachment; filename=\"" . $ExcellFile['name'] . "\"");
                //header("Content-Length: ".$media->getSize());
                readfile($ExcellFile['path']);
                exit();
            }
    
        }
    
        return $this->redirectToRoute('index');
    }
    
    /**
     * @Route("/declaration/admin-media/{id}", name="declaration_admin_media_download")
     */
    public function AdminMediaDownload(int $id, MediaManager $mediaManager, FileProvider $provider, KernelInterface $kernel)
    {
         $session= $this->get('session')->all();
        if(!empty($session['_security_user'])){
            $user=unserialize($session['_security_user']);
            if(in_array('ROLE_USER', $user->getRoleNames())){
                $media=$mediaManager->find($id);
                $file_name=$provider->getReferenceFile($media,'reference')->getName();
                $projectRoot = $kernel->getProjectDir();
                header('Content-Type: '.$media->getContentType());
                header("Content-Transfer-Encoding: Binary");
                header("Content-disposition: attachment; filename=\"" . $media->getName() . "\"");
                header("Content-Length: ".$media->getSize());
                readfile($projectRoot.'/public/upload/media/'.$file_name);
                exit();
            }
            
        }

        return $this->redirectToRoute('index');
    }
    
    
    /**
     * @Route("/declaration/media/{id}", name="declaration_media_download")
     */
    public function MediaDownload(int $id, MediaManager $mediaManager, FileProvider $provider, KernelInterface $kernel)
    {
        if (in_array($id,[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,1061])) {
            $media=$mediaManager->find($id); 
            $file_name=$provider->getReferenceFile($media,'reference')->getName();
            $projectRoot = $kernel->getProjectDir();
            header('Content-Type: '.$media->getContentType());
            header("Content-Transfer-Encoding: Binary");
            header("Content-disposition: attachment; filename=\"" . $media->getName() . "\"");
            header("Content-Length: ".$media->getSize());
            readfile($projectRoot.'/public/upload/media/'.$file_name);
            exit();
        }
        //seriously ? wanna grab uploaded xls files from users ?
        //FU !
        return $this->redirectToRoute('index');
    }
    
    
    /**
     * @Route("/declaration/admin-composante/{wde_declar}", name="declaration_admin_composantes_download")
     */
    public function AdminComposantesDownload(int $wde_declar, MediaManager $mediaManager, FileProvider $provider, KernelInterface $kernel)
    {
        $session= $this->get('session')->all();
        if(!empty($session['_security_user'])){
            $user=unserialize($session['_security_user']);
            if(in_array('ROLE_USER', $user->getRoleNames())){
                 
                $WcoconRepository = $this->getDoctrine()->getRepository(Wcocon::class);
                $ExcellFile = $WcoconRepository->GenerateExcellComposantes($wde_declar);
    
                header('Content-Type: application/vnd.ms-excel');
                header("Content-Transfer-Encoding: Binary");
                header("Content-disposition: attachment; filename=\"" . $ExcellFile['name'] . "\"");
                //header("Content-Length: ".$media->getSize());
                readfile($ExcellFile['path']);
                exit();
            }
    
        }
    
        return $this->redirectToRoute('index');
    }
    /**
     * @Route("/declaration/admin-composante-stages/{wde_declar}", name="declaration_admin_composantes_stages_download")
     */
    public function AdminComposantesStagesDownload(int $wde_declar, MediaManager $mediaManager, FileProvider $provider, KernelInterface $kernel)
    {
        $session= $this->get('session')->all();
        if(!empty($session['_security_user'])){
            $user=unserialize($session['_security_user']);
            if(in_array('ROLE_USER', $user->getRoleNames())){
                 
                $WcoconRepository = $this->getDoctrine()->getRepository(Wcocon::class);
                $ExcellFile = $WcoconRepository->GenerateExcellComposantesStages($wde_declar);
    
                header('Content-Type: application/vnd.ms-excel');
                header("Content-Transfer-Encoding: Binary");
                header("Content-disposition: attachment; filename=\"" . $ExcellFile['name'] . "\"");
                //header("Content-Length: ".$media->getSize());
                readfile($ExcellFile['path']);
                exit();
            }
    
        }
    
        return $this->redirectToRoute('index');
    }    
    /**
     * @Route("/declaration/admin-composante-presse-numerique/{id}", name="declaration_admin_panorama_presse_numerique_download")
     */
    public function AdminComposantesPresseNumeriqueDownload(int $id, MediaManager $mediaManager, FileProvider $provider, KernelInterface $kernel)
    {
        $session= $this->get('session')->all();
        if(!empty($session['_security_user'])){
            $user=unserialize($session['_security_user']);
            if(in_array('ROLE_USER', $user->getRoleNames())){
             
                $WcoconRepository = $this->getDoctrine()->getRepository(Wcocon::class);
                $ExcellFile = $WcoconRepository->GenerateExcellPanoramaPresseNumerique($id);

                header('Content-Type: application/vnd.ms-excel');
                header("Content-Transfer-Encoding: Binary");
                header("Content-disposition: attachment; filename=\"" . $ExcellFile['name'] . "\"");
                //header("Content-Length: ".$media->getSize());
                readfile($ExcellFile['path']);
                exit();
            }
    
        }
    
        return $this->redirectToRoute('index');
    }
    
    /**
     * @Route("/declaration/admin-composante-presse-papier/{id}", name="declaration_admin_panorama_presse_papier_download")
     */
    public function AdminComposantesPressePapierDownload(int $id, MediaManager $mediaManager, FileProvider $provider, KernelInterface $kernel)
    {
        $session= $this->get('session')->all();
        if(!empty($session['_security_user'])){
            $user=unserialize($session['_security_user']);
            if(in_array('ROLE_USER', $user->getRoleNames())){
                 
                $WcoconRepository = $this->getDoctrine()->getRepository(Wcocon::class);
                $ExcellFile = $WcoconRepository->GenerateExcellPanoramaPressePapier($id);
    
                header('Content-Type: application/vnd.ms-excel');
                header("Content-Transfer-Encoding: Binary");
                header("Content-disposition: attachment; filename=\"" . $ExcellFile['name'] . "\"");
                //header("Content-Length: ".$media->getSize());
                readfile($ExcellFile['path']);
                exit();
            }
    
        }
    
        return $this->redirectToRoute('index');
    }
    
    public static function is_valid_domain($domain_name)
    {
        //FILTER_VALIDATE_URL checks length but..why not? so we dont move forward with more expensive operations
        $domain_len = strlen($domain_name);
        if ($domain_len < 3 OR $domain_len > 253)
            return FALSE;
    
            //getting rid of HTTP/S just in case was passed.
            if(stripos($domain_name, 'http://') === 0)
                $domain_name = substr($domain_name, 7);
                elseif(stripos($domain_name, 'https://') === 0)
                $domain_name = substr($domain_name, 8);
    
                //we dont need the www either
                if(stripos($domain_name, 'www.') === 0)
                    $domain_name = substr($domain_name, 4);
    
                    //Checking for a '.' at least, not in the beginning nor end, since http://.abcd. is reported valid
                    if(strpos($domain_name, '.') === FALSE OR $domain_name[strlen($domain_name)-1]=='.' OR $domain_name[0]=='.')
                        return FALSE;
                         
                        //now we use the FILTER_VALIDATE_URL, concatenating http so we can use it, and return BOOL
                        return (filter_var ('http://' . $domain_name, FILTER_VALIDATE_URL)===FALSE)? FALSE:TRUE;
    
    }
    
    public function cleanEmail($email){
        $email = str_replace('..','.',$email);
        $email = str_replace('.@','@',$email);
        return $email;
    }
    
}
