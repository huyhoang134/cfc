<?php
namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class AppExtension extends AbstractExtension
{
    public function getFilters()
    {
        return [
            new TwigFilter('price', [$this, 'formatPrice']),
            new TwigFilter('filtreCrochet', [$this, 'filtreCrochet']),
            new TwigFilter('basename', [$this, 'basename']),
        ];
    }

    public function formatPrice($number, $decimals = 0, $decPoint = '.', $thousandsSep = ',')
    {
        $price = number_format($number, $decimals, $decPoint, $thousandsSep);
        $price = '$'.$price;

        return $price;
    }
    
    public function filtreCrochet($string)
    {
        $expl=explode('[', $string);
        if(count($expl)){
            return $expl[0];
        }else{
            return $string;
        }
       
        
    }
    
    public function basename($string)
    {
        return basename($string);
    }
}

?>