<?php

namespace App\Repository;


use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Filesystem\Filesystem;
use Twig\Environment;
use Mpdf\Mpdf;
use Mpdf\MpdfException;
use Mpdf\Output\Destination;
use Psr\Container\ContainerInterface;
use App\Entity\Wcocon;
use App\Entity\Wdeclar;
use App\Entity\Wtype;
use App\Entity\Chorus;


use Symfony\Component\HttpFoundation\RequestStack;

use App\Application\Sonata\MediaBundle\Entity\Media;
use Symfony\Component\HttpKernel\KernelInterface;

use Sonata\MediaBundle\Provider\FileProvider;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Shared\Font as Share_Font;
use Imagine\Image\Palette\RGB;

use Gedmo\Sluggable\Util\Urlizer;



/**
 * @method Wcocon|null find($id, $lockMode = null, $lockVersion = null)
 * @method Wcocon|null findOneBy(array $criteria, array $orderBy = null)
 * @method Wcocon[]    findAll()
 * @method Wcocon[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WcoconRepository extends ServiceEntityRepository
{
    var $download_key='ec6bdd93b5da97cac519f4f534fd3de6';
    public function __construct(ManagerRegistry $registry,RequestStack $requestStack, Environment $twig, ParameterBagInterface $parameter, ContainerInterface $container, KernelInterface $kernel, FileProvider $provider)
    {
        $this->registry=$registry;
        $this->connection=$this->registry->getManager()->getConnection();
        $this->requestStack = $requestStack;
        $this->twig = $twig;
        $this->parameter = $parameter;
        $this->container = $container;
        $this->kernel = $kernel;
        $this->provider=$provider;
        parent::__construct($registry, Wcocon::class);
    }

    public function getChainedDeclarationConfiguration($Wcocon)
    {
        $conf=$this->getDeclarationConfiguration($Wcocon );
       
        foreach($conf as $i=>$c){
            if($c[0]->type_etape=='declaration' && $c[0]->type_interface>0){
                unset($conf[$i]);
            }
        }
        //reorder index
        $rconf=[];
        foreach($conf as $i=>$c){
            $rconf[]=$c;
        }

        $this->ChainedDeclarationConfiguration=$rconf;
        return $rconf;
    }
   
    public function getDeclarationConfigurationByWdeclar($wdeclar)
    {

        //$Wcocon=$wdeclar->getWdeDossier();
        $alldeclars=$this->getDeclarationConfiguration($wdeclar, $wtype_id=false, $forceAlldeclar=true);
        $declar=false;
        foreach($alldeclars as $d){
            if($d[0]->wde_declar==$wdeclar->getWdeDeclar()){
                $declar=$d;
            }
        }
        return [0=>$alldeclars[0],1=>$declar];
    }
    
    public function getDeclarationConfigurationByWcocon($wcocon, $wtype_id=false, $forceAlldeclar=false )
    {
        $wcocon=$wdeclar->getWdeDossier();
    }
    /** @var Wcocon */
    public function getDeclarationConfiguration($wcocon, $wtype_id=false, $forceAlldeclar=false )
    {
      
       $declarations=$wcocon->getWdeclars()->toArray();

        $configuration=[];
        $index_coordonnees=-2;
        foreach($declarations as $i=>$d){
           
            
            $wtype = $this->registry->getRepository('App\Entity\Wtype')->findOneBy(['label'=>$d->getWdeTypcont()]);
            // Skip non-exist configuration
            if (empty($wtype)) {
                continue;
            }
            $conf = $this->registry->getRepository('App\Entity\Wconf')->createQueryBuilder('c');
            $conf->where('c.wconf_typcont  = ' . $wtype->getId());
            $conf->andWhere('c.actif  = 1');
            $conf->orderBy('c.etape', 'asc');
            $conf->addOrderBy('c.position', 'asc');
    

            $conf_restults = $conf->getQuery()->getResult();
            $wde_dossier = $d->getWdeDossier()->getWcoDossier();
            $wde_declar  = $d->getWdeDeclar();
            $wde_contrat  = $d->getWdeContrat();
            $wde_etat_declar = $d->getWdeEtatDeclar();
            $wde_annee  = $d->getWdeAnnee();
            $wde_numperiod=$d->getWdeNumperiod();
            $label_annee = !empty($d->getWdeIntituleDecl())?$d->getWdeIntituleDecl().' '.$d->getWdeAnneeDecl():null;
            $wde_annee_declar = $d->getWdeAnneeDecl();

            
            //we remove the priority rule to chain all the declarations.
            //the order is allready set
            $label_interface=str_replace('%wde_annee%',$wde_annee, $wtype->getLabelInterface());
            $label_interface=str_replace('[wde_annee]',$wde_annee, $label_interface);
            
            $label_interface_home=str_replace('%wde_annee%',$wde_annee, $wtype->getLabelInterfaceHome());
            $label_interface_home=str_replace('[wde_annee]',$wde_annee, $label_interface_home);

            $label_interface     =str_replace('%wde_numperiod%',$wde_numperiod, $label_interface);
            $label_interface_home=str_replace('[wde_numperiod]',$wde_numperiod, $label_interface_home);
            
            
            $index_coordonnees=$index_coordonnees+2;
            $index_declaration=$index_coordonnees+1;

            foreach($conf_restults as $j => $items){
                if($items->getEtape()=="coordonnees"){
                    //en mode chaine, on affiche que la premiere étape de coordonnées.
                   
                    
                    if(empty($wtype_id) && $index_coordonnees==0){
                        
                        $el=clone($items);
                        $el->wde_dossier=$wde_dossier;
                        $el->wde_declar=$wde_declar;
                        $el->wde_contrat=$wde_contrat;
                        $el->wde_etat_declar=$wde_etat_declar;
                        $el->wde_annee=$wde_annee;
                        $el->wde_numperiod=$wde_numperiod;
                        $el->label_annee=$label_annee;
                        $el->annee_declar=$wde_annee_declar;
                        $el->type_etape="coordonnees";
                        $el->type_interface=$wtype->getTypeInterface();
                        $el->label_interface=$label_interface;
                        $el->label_interface_home=$label_interface_home;
                        $el->type_object=$el->getTypeObjet();
                        $el->wtype=$wtype;
                        $configuration[$index_coordonnees][]=$el;
                        //en mode non chainé, on ne récupere que les coordonées du type en cours.
                    }elseif($wtype_id==$wtype->getId())
                    {
                        
                        $el=clone($items);
                        $el->wde_dossier=$wde_dossier;
                        $el->wde_declar=$wde_declar;
                        $el->wde_contrat=$wde_contrat;
                        $el->wde_etat_declar=$wde_etat_declar;
                        $el->wde_annee=$wde_annee;
                        $el->wde_numperiod=$wde_numperiod;
                        $el->label_annee=$label_annee;
                        $el->annee_declar=$wde_annee_declar;
                        $el->type_etape="coordonnees";
                        $el->type_interface=$wtype->getTypeInterface();
                        $el->label_interface=$label_interface;
                        $el->label_interface_home=$label_interface_home;
                        $el->type_object=$el->getTypeObjet();
                        $el->wtype=$wtype;
                        $configuration[$index_coordonnees][]=$el;
                    }
                    
                }
            
                if($items->getEtape()=="declaration"){
                    
                    if(empty($wtype_id)){
                        $el=clone($items);
                        $el->wde_dossier=$wde_dossier;
                        $el->wde_declar = $wde_declar;
                        $el->wde_contrat=$wde_contrat;
                        $el->wde_etat_declar=$wde_etat_declar;
                        $el->wde_annee=$wde_annee;
                        $el->wde_numperiod=$wde_numperiod;
                        $el->label_annee=$label_annee;
                        $el->annee_declar=$wde_annee_declar;
                        $el->type_etape="declaration";
                        $el->type_interface=$wtype->getTypeInterface();
                        $el->label_interface=$label_interface;
                        $el->label_interface_home=$label_interface_home;
                        $el->type_object=$el->getTypeObjet();
                        $el->wtype=$wtype;
                        $configuration[$index_declaration][]=$el;
                    }elseif($wtype_id==$wtype->getId())
                    {
                        $el=clone($items);
                        $el->wde_dossier=$wde_dossier;
                        $el->wde_declar = $wde_declar;
                        $el->wde_contrat=$wde_contrat;
                        $el->wde_etat_declar=$wde_etat_declar;
                        $el->wde_annee=$wde_annee;
                        $el->wde_numperiod=$wde_numperiod;
                        $el->label_annee=$label_annee;
                        $el->annee_declar=$wde_annee_declar;
                        $el->type_etape="declaration";
                        $el->type_interface=$wtype->getTypeInterface();
                        $el->label_interface=$label_interface;
                        $el->label_interface_home=$label_interface_home;
                        $el->type_object=$el->getTypeObjet();
                        $el->wtype=$wtype;
                        $configuration[$index_declaration][]=$el;
                    }
                    
                }
            }
        }
        /*dirty fix to get number chaining when extra contract don't have coordinate steps.*/
        /* Todo: optimize*/
        $conf=[];
        foreach($configuration as $c){
            $conf[]=$c;
        }
        
        if(count($conf)<2){
            return false;
        }

        return $conf;
    }
    
    
    
    public function getValue($item, $wco_dossier, $wde_declar)
    {
        
        $values = $this->getShowObjectValues($wco_dossier, $wde_declar);
        $item   = trim($item);
        $prefix = substr($item, 0, 3);
    
        if($prefix == 'wco' && $item != 'wcompos')
        {
            switch ($item)
            {
                case 'wco_stamp':
                case 'wco_synchro':
                    return $values['wco'][$item]->date;
                default:
                    return $values['wco'][$item];
            }
        }elseif($prefix=='wde'){
            switch ($item)
            {
                case 'wde_stamp':
                case 'wde_synchro':
                case 'wde_date_validation':
                    return $values['wde'][$item]->date;
                default:
                    return $values['wde'][$item];
            }
        }elseif($prefix=='wan'){
            return !empty($values['wan'][$item])?$values['wan'][$item]:false;
        }elseif($item=='wcompos'||$item=='wcompos_stages'){
            return $values['wcompos'];
        }elseif($item=='panorama_press'){    
            return $values['wpp'];
        }elseif($item=='abo_press_livres'){
                return $values['wabo']; 
        }elseif($item=='abo_press'){
                    return $values['wabo']; 
        }elseif($item=='wprestataire'){     
            return $values['wprestataire'];
        }elseif($item=='relation_publique'){
                return $values['wrp'];
        }elseif($item=='cma_btp'){
                return $values['wrp']; 
        }elseif($item=='wrgpd'){
                return $values['wrgpd'];
        }elseif($item=='wcpextw'){
                return $values['wcpextw'];
        }elseif($item=='wcpextc'){
                return $values['wcpextc'];
        }else{
            /*dump($item);
            dump($values);*/
        }

        return 'value ' . $item;
    }    
    
    public function getDeclarById($wdeclar_id)
    {
        $wde = $this->registry->getRepository('App\Entity\Wdeclar')->createQueryBuilder('d');
        $wde->where('d.wde_declar  = '.$wdeclar_id);
        $wde_results = $wde->getQuery()->getResult();
        if(!empty($wde_results))
        {
            return $wde_results[0];
        }else{
            return false;
        }
        
    }

    public function getShowObjectValues($wco_dossier, $wde_declar)
    {
    
        if(empty($this->ObjVal[$wco_dossier])){
            $wco = $this->registry->getRepository('App\Entity\Wcocon')->createQueryBuilder('c');
            $wco->where('c.wco_dossier  = '.$wco_dossier);
            $wco_results = $wco->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
            $this->ObjVal[$wco_dossier]=$wco_results[0];
        }
    
        if(empty($this->ObjVal[$wde_declar])){
            $wde = $this->registry->getRepository('App\Entity\Wdeclar')->createQueryBuilder('d');
            $wde->where('d.wde_declar  = '.$wde_declar);
            $wde_results = $wde->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
            
            $this->ObjVal[$wde_declar]=$wde_results[0];
        }
    
        if(empty($this->ObjVal['wan'.$wde_declar])){
            $wan = $this->registry->getRepository('App\Entity\Wannexe')->createQueryBuilder('a');
            $wan->where('a.wan_declar  = '.$wde_declar);
            $wan_results = $wan->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
            $this->ObjVal['wan'.$wde_declar]=!empty($wan_results[0])?$wan_results[0]:false;
        }
        //see ENS/UNI to test (ex : dossier 166459 	or 166460)
        // /admin/app/viewwtype/18/wcocon/3024/show
        if(empty($this->ObjVal['wcompos'.$wde_declar])){
            $wcompos = $this->registry->getRepository('App\Entity\Wcompos')->createQueryBuilder('a');
            $wcompos->where('a.wcp_declar  = '.$wde_declar);
            $wcompos_results = $wcompos->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
            $this->ObjVal['wcompos'.$wde_declar]=!empty($wcompos_results)?$wcompos_results:false;
        }
        if(empty($this->ObjVal['wpp'.$wde_declar])){
            $wpp_array=[];
            $wpp = $this->registry->getRepository('App\Entity\Wpanoramapress')->createQueryBuilder('w');
            $wpp->where('w.wppDeclar  = '.$wde_declar);
            $wpp_results = $wpp->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
            //$wpp_results = $wpp->getQuery()->getResult();
            if(!empty($wpp_results)){
                $wppId=$wpp_results[0]["wppId"];
                $wpp_array=[
                    'wpp_id' => $wpp_results[0]["wppId"],
                    /*'wpp_prestataire' => $wpp_results[0]["wppPrestataire"],
                    'wpp_prestataire_label' => $wpp_results[0]["wppPrestataireLabel"],*/
                    ];
                $wppl = $this->registry->getRepository('App\Entity\WpanoramapressListe')->createQueryBuilder('wl');
                $wppl->where('wl.wpplWpp  = '.$wppId);
                $wppl_results = $wppl->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
                
                //$wpp_array['wpanoramapress_liste']=$wppl_results;
                if(!empty($wppl_results)){
                    foreach($wppl_results as $i=>$list){
                        if(!empty($list['wpplFichier'])){
                            $mediaManager = $this->container->get('sonata.media.manager.media');
                            $media=$mediaManager->find($list['wpplFichier']);
                            $file_name=$media->getName();
                            $file_reference=$this->provider->getReferenceFile($media,'reference')->getName();
                            $media_key=md5($list['wpplFichier'].$this->download_key).'%'.$list['wpplFichier'];
                        }else{
                            $file_name=false;
                            $media=false;
                            $media_key=false;
                            $file_reference=false;
                        }

                        
                        $wpp_array['wpanoramapress_liste'][$i]=[
                            'wppl_id'=>$list['wpplId'],
                            'wppl_label'=>$list['wpplLabel'],
                            'wppl_periode'=>$list['wpplPeriode'],
                            'wppl_periode_nombre'=>$list['wpplPeriodeNombre'],
                            'wppl_moyenne_repro'=>$list['wpplMoyenneRepro'],
                            'wppl_moyenne_exemplaire'=>$list['wpplMoyenneExemplaire'],
      
                            
                            'wppl_total'=>$list['wpplTotal'],
                            'wppl_fichier'=>$list['wpplFichier'],
                            

                            
                            'wppl_fichier_name'=>$file_name,
                            'wppl_fichier_media'=>$media,
                            'wppl_fichier_media_key'=>$media_key,
                            'wppl_fichier_media_reference'=>$file_reference,
                            
                            'wppl_is_chorus_pro'=>$list['wpplIsChorusPro'],
                            'wppl_siret'=>$list['wpplSiret'],
                            'wppl_service_code'=>$list['wpplServiceCode'],
                            'wppl_service_list'=>$this->registry->getRepository(Chorus::class)->findServicesBySiret( $list['wpplSiret'] ),
                            'wppl_has_order'=>$list['wpplHasOrder'],
                            'wppl_order_number'=>$list['wpplOrderNumber'],
                            
                        ];
                        $wpplp = $this->registry->getRepository('App\Entity\WpanoramapressListePublication')->createQueryBuilder('wlp');
                        $wpplp->where('wlp.wpplpWppl  = '.$list['wpplId']);
                        $wpplp_results = $wpplp->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
                        if(!empty($wpplp_results)){
                            foreach($wpplp_results as $j=>$publication){
                                $wpp_array['wpanoramapress_liste'][$i]['wpanoramapress_liste_publication'][$j]=['wpplp_label'=>$publication['wpplpLabel'], 'wpplp_total'=> $publication['wpplpTotal']];
                            }
                        }else{
                            $wpp_array['wpanoramapress_liste'][$i]['wpanoramapress_liste_publication']=[];
                        }
                       
                    }
                }else{
                    $wpp_array['wpanoramapress_liste']=[0=>[
                            'wppl_id'=>"",
                            'wppl_label'=>"",
                            'wppl_periode'=>"",
                            'wppl_periode_nombre'=>"",
                            'wppl_moyenne_repro'=>"",
                            'wppl_moyenne_exemplaire'=>"",
                            'wppl_total'=>"",
                            'wppl_fichier'=>"",
                            'wppl_fichier_media_key'=>"",
                            'wppl_fichier_media_reference'=>"",
                            'wppl_fichier_name'=>"",
                        
                            'wppl_is_chorus_pro'=>'',
                            'wppl_siret'=>'',
                            'wppl_service_code'=>'',
                            'wppl_order_number'=>'',
                            'wppl_service_list'=>[],
                            'wppl_has_order'=>'',
                            'wppl_order_number'=>'',
                        
                        
                            'wpanoramapress_liste_publication'=>[0=>['wpplp_label'=>'','wpplp_total'=>'']]
                        ]];
                }
               
            }else{
                $wpp_array=[
                    'wpp_id' => false,
                    /*'wpp_prestataire' => null,
                    'wpp_prestataire_label' => "",*/
                    'wpanoramapress_liste'=>[0=>[
                            'wppl_label'=>"",
                            'wppl_periode'=>"",
                            'wppl_periode_nombre'=>"",
                            'wppl_moyenne_repro'=>"",
                            'wppl_moyenne_exemplaire'=>"",
                            'wppl_total'=>"",
                            'wppl_fichier'=>"",
                            'wppl_fichier_media_key'=>"",
                            'wppl_fichier_media_reference'=>"",
                            'wppl_fichier_name'=>"",
                        
                            'wppl_is_chorus_pro'=>'',
                            'wppl_siret'=>'',
                            'wppl_service_code'=>'',
                            'wppl_order_number'=>'',
                            'wppl_service_list'=>[],
                            'wppl_has_order'=>'',
                            'wppl_order_number'=>'',
                        
                            'wpanoramapress_liste_publication'=>[0=>['wpplp_label'=>'','wpplp_total'=>'']]
                        ]]
                ];
            }

            $this->ObjVal['wpp'.$wde_declar]=!empty($wpp_array)?$wpp_array:false;
        }
        
        if(empty($this->ObjVal['wabo'.$wde_declar])){
            $wabo_array=[];
            $wabo = $this->registry->getRepository('App\Entity\Wabo')->createQueryBuilder('w');
            $wabo->where('w.waboDeclar  = '.$wde_declar);
            $wabo_results = $wabo->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
            if(!empty($wabo_results)){
                
                if(!empty($wabo_results[0]["waboFichier"])){
                    $mediaManager = $this->container->get('sonata.media.manager.media');
                    $media=$mediaManager->find($wabo_results[0]["waboFichier"]);
                    $file_name=$media->getName();
                    $file_reference=$this->provider->getReferenceFile($media,'reference')->getName();
                    $media_key=md5($wabo_results[0]["waboFichier"].$this->download_key).'%'.$wabo_results[0]["waboFichier"];
                }else{
                    $file_name=false;
                    $media=false;
                    $media_key=false;
                    $file_reference=false;
                }
                
                $wabo_array=[
                    "wabo_id"=>$wabo_results[0]["waboId"],
                    "wabo_declar"=>$wabo_results[0]["waboDeclar"],
                    "wabo_dossier"=>$wabo_results[0]["waboDossier"],
                    "wabo_contrat"=>$wabo_results[0]["waboContrat"],
                    "wabo_prestataire"=>$wabo_results[0]["waboPrestataire"],
                    "wabo_prestataire_label"=>$wabo_results[0]["waboPrestataireLabel"],
                    "wabo_fichier"=>$wabo_results[0]["waboFichier"],

                    'wabo_fichier_name'=>$file_name,
                    'wabo_fichier_media'=>$media,
                    'wabo_fichier_media_key'=>$media_key,
                    'wabo_fichier_media_reference'=>$file_reference,
                    
                    
                    "wabo_type"=>$wabo_results[0]["waboType"]
                    ];
                    
                  
                    $waboPublication = $this->registry->getRepository('App\Entity\WaboPublication')->createQueryBuilder('wp');
                    
                    $waboPublication->where('wp.wabopWabo  = '.$wabo_array["wabo_id"]);
                    $wabo_array['wabo_publications']=$waboPublication->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
            }else{
                $wabo_array=[
                    "wabo_id"=>"",
                    "wabo_declar"=>"",
                    "wabo_dossier"=>"",
                    "wabo_contrat"=>"",
                    "wabo_prestataire"=>"",
                    "wabo_prestataire_label"=>"",
                    "wabo_publications"=>[
                        ['wabopTitre'=>'','wabopNombre'=>'','wabopAuteur'=>'','wabopEditeur'=>'','wabopType'=>'titre'],
                        ['wabopTitre'=>'','wabopNombre'=>'','wabopAuteur'=>'','wabopEditeur'=>'','wabopType'=>'site'],
                        ['wabopTitre'=>'','wabopNombre'=>'','wabopAuteur'=>'','wabopEditeur'=>'','wabopType'=>'livre'],
                        
                    ],
                    "wabo_fichier"=>"",
                    'wabo_fichier_media_key'=>"",
                    'wabo_fichier_media_reference'=>"",
                    'wabo_fichier_name'=>"",
                    
                    "wabo_type"=>"" ,
                    
                ];
            }


            $this->ObjVal['wabo'.$wde_declar]=!empty($wabo_array)?$wabo_array:false;
            
        }
        if(empty($this->ObjVal['wpresta'.$wde_declar])){
            $wpresta = $this->registry->getRepository('App\Entity\Wprestataire')->createQueryBuilder('Wprestataire');
            $wpresta->where('Wprestataire.wprestaDeclar  = '.$wde_declar);
            $wpresta_results = $wpresta->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
            
            $RepertoirePrestataire = $this->registry->getRepository('App\Entity\RepertoirePrestataire')->createQueryBuilder('Wprestataire');
            //$RepertoirePrestataire->where('Wprestataire.wprestaDeclar  = '.$wde_declar);
            $RepertoirePrestataire_results = $RepertoirePrestataire->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);

            if(!empty($wpresta_results)){;

                $wpresta_array=[
                    "wpresta_id"=>$wpresta_results[0]['wprestaId'],
                    "wpresta_prestataire"=>$wpresta_results[0]['wprestaPrestataire'],
                    "wpresta_prestataire_label"=>$wpresta_results[0]['wprestaPrestataireLabel'],
                    "wpresta_prestataire_label2"=>$wpresta_results[0]['wprestaPrestataireLabel2'],
                    "repertoire_prestataire"=>$RepertoirePrestataire_results
                ];
            }else{
                $wpresta_array=[
                    "wpresta_id"=>"",
                    "wpresta_prestataire"=>"",
                    "wpresta_prestataire_label"=>"",
                    "wpresta_prestataire_label2"=>"",
                    "repertoire_prestataire"=>$RepertoirePrestataire_results
                
                ];
            }
            
            $this->ObjVal['wpresta'.$wde_declar]=!empty($wpresta_array)?$wpresta_array:false;
        }
        if(empty($this->ObjVal['wrp'.$wde_declar])){
            $wrp = $this->registry->getRepository('App\Entity\WrelationPublique')->createQueryBuilder('WrelationPublique');
            $wrp->where('WrelationPublique.wrpDeclar  = '.$wde_declar);
            $wrp_results = $wrp->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        
            if(!empty($wrp_results)){
                foreach($wrp_results as $w){
                    $wrp_array[]=[
                        "wrp_label"=>$w['wrpLabel'],
                        "wrp_adresse"=>$w['wrpAdresse'],
                        "wrp_cp"=>$w['wrpCp'],
                        "wrp_ville"=>$w['wrpVille'],
                        "wrp_volume"=>$w['wrpVolume'],
                        "wrp_telephone"=>$w['wrpTelephone'],
                        "wrp_email"=>$w['wrpEmail'],
                    ];
                }
                    
            }else{
                    $wrp_array[]=[
                       "wrp_label"=>"",
                        "wrp_adresse"=>"",
                        "wrp_cp"=>"",
                        "wrp_ville"=>"",
                        "wrp_volume"=>"",
                        "wrp_telephone"=>"",
                        "wrp_email"=>"",
                    ];
            }
        
            $this->ObjVal['wrp'.$wde_declar]=!empty($wrp_array)?$wrp_array:false;
        }        
        
        if(empty($this->ObjVal['wcpextw'.$wde_declar])){
            
            $wcpext = $this->registry->getRepository('App\Entity\Wcpext')->createQueryBuilder('wcpext');
            $wcpext->where('wcpext.wcpextDeclar  = '.$wde_declar);
            $wcpext_results = $wcpext->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
            if(!empty($wcpext_results)){
                foreach($wcpext_results as $i=>$r){
                   $wcpextmel = $this->registry->getRepository('App\Entity\WcpextMel')->createQueryBuilder('wcpextmel');
                   $wcpextmel->where('wcpextmel.wcpextId  = '.$r['wcpextId']);
                   $wcpextmel_results = $wcpextmel->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
                   
                   if(!empty($wcpextmel_results)){
                       $wcpext_results[$i]['wcpextMel']=$wcpextmel_results;
                   }else{
                       $wcpext_results[$i]['wcpextMel']=[
                        0=>['wcpextmelMel'=>""]
                    ];
                   }
                }
                
                $this->ObjVal['wcpextw'.$wde_declar]=$wcpext_results;
                
                //sm( $this->ObjVal['wcpextw'.$wde_declar]);die();
                

            }else{
                $this->ObjVal['wcpextw'.$wde_declar][]=[
                    'wcpextPublication'=>"",
                    'wcpextDateMel'=>"",
                    'wcpextTitre'=>"",
                    'wcpextAuteur'=>"",
                    'wcpextParution'=>"",
                    'wcpextDestinataires'=>"",
                    'wcpextMel'=>[
                        0=>['wcpextmelMel'=>""]
                    ]
                    
                ];
            }
            
        }
        
        if(empty($this->ObjVal['wcpextc'.$wde_declar])){

            $wcpext = $this->registry->getRepository('App\Entity\Wcpext')->createQueryBuilder('wcpext');
            $wcpext->where('wcpext.wcpextDeclar  = '.$wde_declar);
            $wcpext_results = $wcpext->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
            if(!empty($wcpext_results)){
                foreach($wcpext_results as $i=>$r){
                   $wcpextmel = $this->registry->getRepository('App\Entity\WcpextMel')->createQueryBuilder('wcpextmel');
                    $wcpextmel->where('wcpextmel.wcpextId  = '.$r['wcpextId']);
                    $wcpextmel_results = $wcpextmel->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
                    $wcpext_results[$i]['wcpextMel']=$wcpextmel_results;
                }
                
                $this->ObjVal['wcpextc'.$wde_declar]=$wcpext_results;
            }else{
                $this->ObjVal['wcpextc'.$wde_declar][]=[
                    'wcpextPublication'=>"",
                    'wcpextDateMel'=>"",
                    'wcpextTitre'=>"",
                    'wcpextAuteur'=>"",
                    'wcpextParution'=>"",
                    'wcpextDestinataires'=>"",
                    'wcpextMel'=>[]
                    
                ];
            }
        
        }
        
        if(empty($this->ObjVal['wrgpd'.$wde_declar])){
            $wrgpd = $this->registry->getRepository('App\Entity\Wrgpd')->createQueryBuilder('Wrgpd');
            $wrgpd->where('Wrgpd.wrgpdDeclar  = '.$wde_declar);
            $wrgpd_results = $wrgpd->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        
            if(!empty($wrgpd_results)){
                $this->ObjVal['wrgpd'.$wde_declar]=$wrgpd_results[0]['wrgpdValue'];
            }else{
                $this->ObjVal['wrgpd'.$wde_declar]=0;
            }
        }

        if(!empty($this->ObjVal[$wde_declar]['wde_siret']))
        {
            $this->ObjVal[$wde_declar]['wde_service_list']=$this->registry->getRepository(Chorus::class)->findServicesBySiret( $this->ObjVal[$wde_declar]['wde_siret'] );
        }else{
            $this->ObjVal[$wde_declar]['wde_service_list']=[];
        }

        return [
            "wco"=>$this->ObjVal[$wco_dossier], 
            "wde"=>$this->ObjVal[$wde_declar], 
            "wan"=>$this->ObjVal['wan'.$wde_declar], 
            "wcompos"=>$this->ObjVal['wcompos'.$wde_declar], 
            'wpp'=>$this->ObjVal['wpp'.$wde_declar], 
            'wabo'=>$this->ObjVal['wabo'.$wde_declar],
            'wprestataire'=>$this->ObjVal['wpresta'.$wde_declar],
            'wrp'=>$this->ObjVal['wrp'.$wde_declar],
            'wrgpd'=>$this->ObjVal['wrgpd'.$wde_declar],
            'wcpextw'=>$this->ObjVal['wcpextw'.$wde_declar],
            'wcpextc'=>$this->ObjVal['wcpextc'.$wde_declar]
        ];
    
    
    
    }    
    
    
    public function updateForm($data, $conf)
    {

        $sql="set session sql_mode='NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION,IGNORE_SPACE' ";
        $stmt=$this->connection->prepare($sql);
        $stmt->execute();

        foreach($conf as $c){
            $field = $c->getSynchroField();     
            $object_conf=$c->getTypeObjetConf();

            $hidden_field=false;
            
            if(!empty($object_conf['contact_field_id'])){
                $hidden_field=$object_conf['contact_field_id'];
                $hidden_field_question=$object_conf['contact_field_id']."_question";
            }
            if(!empty($object_conf['adress_field_id'])){
                $hidden_field=$object_conf['adress_field_id'];
                $hidden_field_question=$object_conf['adress_field_id']."_question";
            }
            if(!empty($hidden_field)){

                $hidden_field_prefix = substr($hidden_field, 0, 3);
                if($hidden_field_prefix == 'wco')
                {
                   $sql="UPDATE wcocon set `".$hidden_field."` = :".$hidden_field.", `".$hidden_field_question."` = :".$hidden_field_question.", wco_stamp=now() WHERE wco_dossier='".$c->wde_dossier."' LIMIT 1";
                    $stmt=$this->connection->prepare($sql);
                    $stmt->execute([
                        $hidden_field=>$data[$hidden_field],
                        $hidden_field_question=>$data[$hidden_field_question]
                        
                    ]);
                }elseif($hidden_field_prefix=='wde'){
                    
                    //fix pour mettre a jour les champs wde de l'étape de coordonnée dans toutes les lignes de la chaine wdeclar;
                    if($c->type_etape=='coordonnees' && !empty($this->ChainedDeclarationConfiguration)){
                        foreach($this->ChainedDeclarationConfiguration as $chain){
                            $sql="UPDATE wdeclar set `".$hidden_field."` = :".$hidden_field.", `".$hidden_field_question."` = :".$hidden_field_question.", wde_stamp=now() WHERE wde_declar='".$chain[0]->wde_declar."' LIMIT 1";
                            $stmt=$this->connection->prepare($sql);
                            $stmt->execute([
                                $hidden_field=>$data[$hidden_field],
                                $hidden_field_question=>$data[$hidden_field_question]
                            ]);
                        }
                    }else{
                        $sql="UPDATE wdeclar set `".$hidden_field."` = :".$hidden_field.", `".$hidden_field_question."` = :".$hidden_field_question.", wde_stamp=now() WHERE wde_declar='".$c->wde_declar."' LIMIT 1";
                        $stmt=$this->connection->prepare($sql);
                        $stmt->execute([
                            $hidden_field=>$data[$hidden_field],
                            $hidden_field_question=>$data[$hidden_field_question]
                        ]);
                    }

                }
            }
            
            
           
            if(in_array($c->getTypeObjet(), ['checkbox', 'civilite']))
            {
                $chk_conf=$c->getTypeObjetConf();
                if(isset($chk_conf['option_val_off'])){
                    $chk_conf_off=$chk_conf['option_val_off'];
                }else{
                    $chk_conf_off=0; //default value
                }
                $data[$field]=!empty($data[$field])?$data[$field]:"0";
                
            }
            

                
            
            
            if (!empty($c->getSynchroField()) || $c->getTypeObjet()=="chorus_pro" || $c->getTypeObjet()=="chorus_pro_extended" || $c->getTypeObjet()=="panorama_press_numerique" || $c->getTypeObjet()=="panorama_press_papier" || $c->getTypeObjet()=="accordion_options" || $c->getTypeObjet()=="bon_commande") {
                
                if($c->getTypeObjet()=="file_upload")
                {
                    if(!empty($_FILES['file_upload_file_input']['tmp_name'][$field])){
                        
                        $name=$_FILES['file_upload_file_input']['name'][$field];
                      
                         $temp_file= $this->kernel->getCacheDir()."/../../tmp_upload/$name";
                            if (!file_exists($this->kernel->getCacheDir()."/../../tmp_upload/")) {
                                mkdir($this->kernel->getCacheDir()."/../../tmp_upload/", 0777, true);
                            }
                            
                        move_uploaded_file($_FILES['file_upload_file_input']['tmp_name'][$field], $temp_file);
                        
     
                        $mediaManager = $this->container->get('sonata.media.manager.media');
                        $media = new Media();
                        $media->setBinaryContent($temp_file);
                        $media->setContext('file_upload');
                        $media->setAuthorName($c->wde_dossier);
                        $media->setCopyright($c->wde_contrat);
                        $media->setDescription("Déclaraiton : ".$c->wde_declar."\r\nDossier : ".$c->wde_dossier."\r\Contrat : ".$c->wde_contrat."\r\Date : ". date("Y-m-d H:i:s"));
                        $media->setProviderName('sonata.media.provider.file');
                        $ret=$mediaManager->save($media);
                        $media_id=$media->getId();
                        $data[$field]=$media_id;
                    }elseif(!empty($data['file_upload_fichier_media_key'][$field]))
                    {
                        $media_id=$this->checkMediaKey($data['file_upload_fichier_media_key'][$field]);
                        $data[$field]=$media_id;
                    }else{
                        $data[$field]=null;
                    }
          
                }
                
                
                
                $prefix = substr($field, 0, 3);
                if($prefix == 'wco' && $field != 'wcompos')
                {
                     $sql="UPDATE wcocon set `".$field."` = :".$field.", wco_stamp=now() WHERE wco_dossier='".$c->wde_dossier."' LIMIT 1";
                    $stmt=$this->connection->prepare($sql);
                    $stmt->execute([$field=>$data[$field]]);
                }elseif($prefix=='wde' && $c->getTypeObjet()!="chorus_pro" && $c->getTypeObjet()!="chorus_pro_extended" && $c->getTypeObjet()!="bon_commande" ){
                    //fix pour mettre a jour les champs wde de l'étape de coordonnée dans toutes les lignes de la chaine wdeclar;
                    if($c->type_etape=='coordonnees' && !empty($this->ChainedDeclarationConfiguration)){
                        foreach($this->ChainedDeclarationConfiguration as $chain){
                            $sql="UPDATE wdeclar set `".$field."` = :".$field." , wde_stamp=now() WHERE wde_declar='".$chain[0]->wde_declar."' LIMIT 1";
                            $stmt=$this->connection->prepare($sql);
                            $stmt->execute([$field=>$data[$field]]);
                        }
                    }else{
                        $sql="UPDATE wdeclar set `".$field."` = :".$field." , wde_stamp=now() WHERE wde_declar='".$c->wde_declar."' LIMIT 1";
                        $stmt=$this->connection->prepare($sql);
                        $stmt->execute([$field=>$data[$field]]);
                    }
                }elseif($prefix=='wan'){
                    $wde = $this->registry->getRepository('App\Entity\Wannexe')->createQueryBuilder('w');
                    $wde->where('w.wan_declar  = '.$c->wde_declar);
                    $wde_results = $wde->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
                    if(count($wde_results)){
                        $sql="UPDATE wannexe set `".$field."` = :".$field." WHERE wan_declar='".$c->wde_declar."' LIMIT 1";
                    }else{
                        //$sql="INSERT INTO `wannexe` (`wan_declar`, `wan_numsite`,`wan_nom`, `wan_tit`, `wan_fct`, `".$field."`) VALUES ('".$c->wde_declar."', '0','', '', '', :".$field.");";
                        $sql="INSERT INTO `wannexe` (`wan_declar`,  `".$field."`) VALUES ('".$c->wde_declar."', :".$field.");";
                    }
                    
                    $stmt=$this->connection->prepare($sql);
                    $stmt->execute([$field=>$data[$field]]);
                }elseif($field == 'wcompos'){
                    $sql="DELETE FROM wcompos where wcp_declar='".$c->wde_declar."';";
                    $stmt=$this->connection->prepare($sql);
                    $stmt->execute();
                    
                    if($c->getTypeObjet()=="wcompos_stages"){
                        if(!empty($data['compos_stagiaires'])){
                            foreach($data['compos_stagiaires'] as $i=>$v){
                                $sql="insert into wcompos(wcp_declar,wcp_libelle,wcp_effectif, wcp_heures) values(:wcp_declar,:wcp_libelle,:wcp_effectif,:wcp_heures)";
                                $stmt=$this->connection->prepare($sql);
                                $stmt->execute(['wcp_declar'=>$c->wde_declar,'wcp_libelle'=>$data['compos_title'][$i],'wcp_effectif'=>$v,'wcp_heures'=>$data['compos_heures'][$i]]);
                            }
                        }
                    }else{
                        if(!empty($data['compos_title'])){
                            foreach($data['compos_num'] as $i=>$v){
                                $sql="insert into wcompos(wcp_declar,wcp_libelle,wcp_effectif) values(:wcp_declar,:wcp_libelle,:wcp_effectif)";
                                $stmt=$this->connection->prepare($sql);
                                $stmt->execute(['wcp_declar'=>$c->wde_declar,'wcp_libelle'=>$data['compos_title'][$i],'wcp_effectif'=>$v]);
                            }
                        }
                    }
                    
                }elseif($c->getTypeObjet()=="chorus_pro"){
                        $sql="UPDATE wdeclar set   `wde_is_chorus_pro` = :wde_is_chorus_pro, `wde_siret` = :wde_siret, `wde_service_code` = :wde_service_code, `wde_is_order_number` = :wde_is_order_number, `wde_order_number` = :wde_order_number,  wde_stamp=now() WHERE wde_declar='".$c->wde_declar."' LIMIT 1";
                        $stmt=$this->connection->prepare($sql);
                        $stmt->execute(['wde_is_chorus_pro'=>$data['wde_is_chorus_pro'], 'wde_siret'=>$data['wde_siret'], 'wde_service_code'=>$data['wde_service_code'],'wde_is_order_number'=>$data['wde_is_order_number'], 'wde_order_number'=>$data['wde_order_number']]);
                }elseif($c->getTypeObjet()=="bon_commande"){

                    $sql="UPDATE wdeclar set   `wde_is_order_number` = :wde_is_order_number, `wde_order_number` = :wde_order_number,  wde_stamp=now() WHERE wde_declar='".$c->wde_declar."' LIMIT 1";
                    $stmt=$this->connection->prepare($sql);
                    $stmt->execute(['wde_is_order_number'=>$data['wde_is_order_number'],  'wde_order_number'=>$data['wde_order_number']]); 
                }elseif($c->getTypeObjet()=="chorus_pro_extended"){
  
                    $wppl_is_chorus_pro = $data['chorus_pro_multi'][0]['wppl_is_chorus_pro'];
                    $wppl_siret         = $data['chorus_pro_multi'][0]['wppl_siret'];
                    $wppl_service_code  = $data['chorus_pro_multi'][0]['wppl_service_code'];
                    $wppl_has_order     = $data['chorus_pro_multi'][0]['wppl_has_order'];
                    $wppl_order_number  = $data['chorus_pro_multi'][0]['wppl_order_number'];
                    
                    
                    $sql="UPDATE wdeclar set   `wde_is_chorus_pro` = :wde_is_chorus_pro, `wde_siret` = :wde_siret, `wde_service_code` = :wde_service_code, `wde_is_order_number` = :wde_is_order_number,  `wde_order_number` = :wde_order_number,  wde_stamp=now() WHERE wde_declar='".$c->wde_declar."' LIMIT 1";
                    $stmt=$this->connection->prepare($sql);
                    $stmt->execute(['wde_is_chorus_pro'=>$wppl_is_chorus_pro, 'wde_siret'=>$wppl_siret, 'wde_service_code'=>$wppl_service_code, 'wde_is_order_number'=>$wppl_has_order, 'wde_order_number'=>$wppl_order_number]);
                
                
                }elseif($c->getTypeObjet()=="panorama_press_numerique" || $c->getTypeObjet()=="panorama_press_papier"){
    
                    $sql="DELETE FROM wpanoramapress where wpp_declar='".$c->wde_declar."';";
                    $stmt=$this->connection->prepare($sql);
                    $stmt->execute();
                    
                    $sql="INSERT INTO `wpanoramapress` (`wpp_id`, `wpp_type`, `wpp_declar`, `wpp_dossier`, `wpp_contrat`) VALUES (NULL, '".str_replace('press','presse',$c->getTypeObjet())."', '".$c->wde_declar."', '".$c->wde_dossier."', '".$c->wde_contrat."');";
                    $stmt=$this->connection->prepare($sql);
                    
                    /*$stmt->bindParam(':wpp_prestataire', $data['wpp_prestataire'], \PDO::PARAM_STR);
                    $stmt->bindParam(':wpp_prestataire_label', $data['wpp_prestataire_label'], \PDO::PARAM_STR);*/
  
                    $stmt->execute();
                    $id=$this->connection->lastInsertId();
                    if(!empty($data['wpanoramapress_liste'])){
                        
                        
                    
                        foreach($data['wpanoramapress_liste'] as $i=>$p){
                            $wppl_fichier="NULL";
                            if(!empty($_FILES['wpanoramapress_liste']['tmp_name'][$i]['wppl_fichier'])){
                                $name=$_FILES['wpanoramapress_liste']['name'][$i]['wppl_fichier'];
                                $temp_file= $this->kernel->getCacheDir()."/../../tmp_upload/$name";
                                if (!file_exists($this->kernel->getCacheDir()."/../../tmp_upload/")) {
                                    mkdir($this->kernel->getCacheDir()."/../../tmp_upload/", 0777, true);
                                }
                            
                                move_uploaded_file($_FILES['wpanoramapress_liste']['tmp_name'][$i]['wppl_fichier'], $temp_file);
                                $mediaManager = $this->container->get('sonata.media.manager.media');
                                $media = new Media();
                                $media->setBinaryContent($temp_file);
                                $media->setContext('panorama_press');
                                $media->setAuthorName($c->wde_dossier);
                                $media->setCopyright($c->wde_contrat);
                                $media->setDescription("Déclaraiton : ".$c->wde_declar."\r\nDossier : ".$c->wde_dossier."\r\Contrat : ".$c->wde_contrat."\r\Date : ". date("Y-m-d H:i:s"));
                                $media->setProviderName('sonata.media.provider.file');
                                $ret=$mediaManager->save($media);
                                $wppl_fichier=$media->getId();
                            }elseif(!empty($p['wppl_fichier_media_key']))
                            {
                               $wppl_fichier=$this->checkMediaKey($p['wppl_fichier_media_key']);
                            }
                            
                            if(!empty($data['chorus_pro_multi'][$i]))
                            {
                                $wppl_is_chorus_pro = $data['chorus_pro_multi'][$i]['wppl_is_chorus_pro'];
                                $wppl_siret         = $data['chorus_pro_multi'][$i]['wppl_siret'];
                                $wppl_service_code  = $data['chorus_pro_multi'][$i]['wppl_service_code'];
                                $wppl_has_order     = $data['chorus_pro_multi'][$i]['wppl_has_order'];
                                $wppl_order_number  = $data['chorus_pro_multi'][$i]['wppl_order_number'];
                            }else{
                                $wppl_is_chorus_pro=null;
                                $wppl_siret=null;
                                $wppl_service_code=null;
                                $wppl_has_order=null;
                                $wppl_order_number=null;
                            }
                            $sql="INSERT INTO `wpanoramapress_liste` (`wppl_id`, `wppl_wpp_id`, `wppl_declar`, `wppl_dossier`, `wppl_contrat`, `wppl_label`, `wppl_periode`, `wppl_periode_nombre`,  `wppl_moyenne_repro`,`wppl_moyenne_exemplaire`, `wppl_total`, `wppl_fichier`, `wppl_is_chorus_pro`,`wppl_siret`,`wppl_service_code`, `wppl_has_order`,`wppl_order_number`)  VALUES 
                                (NULL, '".$id."', '".$c->wde_declar."', '".$c->wde_dossier."', '".$c->wde_contrat."', :wppl_label, :wppl_periode,:wppl_periode_nombre, :wppl_moyenne_repro, :wppl_moyenne_exemplaire, :wppl_total, $wppl_fichier,
                             :wppl_is_chorus_pro,
                                :wppl_siret,
                                :wppl_service_code,
                                :wppl_has_order,
                                :wppl_order_number
                            
                            );";
                            $stmt=$this->connection->prepare($sql);
                            $wppl_label=!empty($p['wppl_label'])?$p['wppl_label']:null;
                            $wppl_periode=!empty($p['wppl_periode'])?$p['wppl_periode']:null;
                            $wppl_total=!empty($p['wppl_total'])?$p['wppl_total']:null;
                            $wppl_periode_nombre=!empty($p['wppl_periode_nombre'])?$p['wppl_periode_nombre']:null;
                            $wppl_moyenne_repro=!empty($p['wppl_moyenne_repro'])?$p['wppl_moyenne_repro']:null;
                            $wppl_moyenne_exemplaire=!empty($p['wppl_moyenne_exemplaire'])?$p['wppl_moyenne_exemplaire']:null;
                            
                            
                            
                            $stmt->bindParam(':wppl_label', $wppl_label, \PDO::PARAM_STR);
                            $stmt->bindParam(':wppl_periode', $wppl_periode, \PDO::PARAM_STR);
                            $stmt->bindParam(':wppl_total', $wppl_total, \PDO::PARAM_INT);
                            
                            $stmt->bindParam(':wppl_periode_nombre', $wppl_periode_nombre, \PDO::PARAM_STR);
                            $stmt->bindParam(':wppl_moyenne_repro', $wppl_moyenne_repro, \PDO::PARAM_STR);
                            $stmt->bindParam(':wppl_moyenne_exemplaire', $wppl_moyenne_exemplaire, \PDO::PARAM_INT);
                            
                                $stmt->bindParam(':wppl_is_chorus_pro', $wppl_is_chorus_pro, \PDO::PARAM_INT);
                                $stmt->bindParam(':wppl_siret', $wppl_siret, \PDO::PARAM_STR);
                                $stmt->bindParam(':wppl_service_code', $wppl_service_code, \PDO::PARAM_STR);
                                $stmt->bindParam(':wppl_has_order', $wppl_has_order, \PDO::PARAM_INT);
                                $stmt->bindParam(':wppl_order_number', $wppl_order_number, \PDO::PARAM_STR);
                            
                            $stmt->execute();
                            $id_liste=$this->connection->lastInsertId();
                            if(!empty($p["publication"])){
                                foreach($p["publication"] as $publications){
                                    $sql="INSERT INTO `wpanoramapress_liste_publication` (`wpplp_id`, `wpplp_wppl_id`, `wpplp_label`, `wpplp_total`) VALUES (NULL, '".$id_liste."', :wpplp_label, :wpplp_total);";
                                    $stmt=$this->connection->prepare($sql);
                                    $stmt->bindParam(':wpplp_label', $publications['wpplp_label'], \PDO::PARAM_STR);
                                    $stmt->bindParam(':wpplp_total', $publications['wpplp_total'], \PDO::PARAM_INT);
                                    $stmt->execute();
                                }
                            }
                            
                        }
                    }
                 }elseif($c->getTypeObjet()=="abo_press_livres"){
                    
                        $sql="DELETE FROM wabo where wabo_declar='".$c->wde_declar."';";
                        $stmt=$this->connection->prepare($sql);
                        $stmt->execute();
                        $wabo_fichier="NULL";
                        if(!empty($_FILES['wabo_fichier']['tmp_name'])){
                            $name=$_FILES['wabo_fichier']['name'];
                            $temp_file= $this->kernel->getCacheDir()."/../../tmp_upload/$name";
                            if (!file_exists($this->kernel->getCacheDir()."/../../tmp_upload/")) {
                                mkdir($this->kernel->getCacheDir()."/../../tmp_upload/", 0777, true);
                            }
                            
                            move_uploaded_file($_FILES['wabo_fichier']['tmp_name'], $temp_file);
                            $mediaManager = $this->container->get('sonata.media.manager.media');
                            $media = new Media();
                            $media->setBinaryContent($temp_file);
                            $media->setContext('abo_press_livres');
                            $media->setAuthorName($c->wde_dossier);
                            $media->setCopyright($c->wde_contrat);
                            $media->setDescription("Déclaraiton : ".$c->wde_declar."\r\nDossier : ".$c->wde_dossier."\r\Contrat : ".$c->wde_contrat."\r\Date : ". date("Y-m-d H:i:s"));
                            $media->setProviderName('sonata.media.provider.file');
                            $ret=$mediaManager->save($media);
                            $wabo_fichier=$media->getId();
                        }elseif(!empty($data['wabo_fichier_media_key']))
                        {
                            $wabo_fichier=$this->checkMediaKey($data['wabo_fichier_media_key']);
                        }
                        $sql="INSERT INTO `wabo` (`wabo_id`, `wabo_declar`, `wabo_dossier`, `wabo_contrat`,  `wabo_fichier`, `wabo_type`) VALUES (NULL, '".$c->wde_declar."', '".$c->wde_dossier."', '".$c->wde_contrat."',  $wabo_fichier, '".$c->getTypeObjet()."');";
                        $stmt=$this->connection->prepare($sql);

                        
                        
                        $stmt->execute();
                        $id=$this->connection->lastInsertId();
                        

                        if(!empty($data['wabop_titre'])){
                            foreach($data['wabop_titre'] as $n=>$titre){
                                $sql="INSERT INTO `wabo_publication` ( `wabop_id`, `wabop_wabo_id`, `wabop_titre`, `wabop_nombre`, `wabop_auteur`, `wabop_editeur`, `wabop_type`) VALUES ( NULL,'$id', :titre, :nombre, NULL, NULL, 'titre');";
                                $stmt=$this->connection->prepare($sql);
                                $stmt->bindParam(':titre', $titre, \PDO::PARAM_STR);
                                $stmt->bindParam(':nombre', $data['wabop_titre_nombre'][$n], \PDO::PARAM_STR);
                                $stmt->execute();
                            }
                        }
                        if(!empty($data['wabop_site'])){
                            foreach($data['wabop_site'] as $n=>$site){
                                $sql="INSERT INTO `wabo_publication` ( `wabop_id`, `wabop_wabo_id`, `wabop_titre`, `wabop_nombre`, `wabop_auteur`, `wabop_editeur`, `wabop_type`) VALUES ( NULL,'$id', :site, :nombre, NULL, NULL, 'site');";
                                $stmt=$this->connection->prepare($sql);
                                $stmt->bindParam(':site', $site, \PDO::PARAM_STR);
                                $stmt->bindParam(':nombre', $data['wabop_site_nombre'][$n], \PDO::PARAM_STR);
                                $stmt->execute();
                            }
                        }

                        if(!empty($data['wabop_livre'])){
                            foreach($data['wabop_livre'] as $i=>$site){
                                $sql="INSERT INTO `wabo_publication` ( `wabop_id`, `wabop_wabo_id`, `wabop_titre`, `wabop_nombre`, `wabop_auteur`, `wabop_editeur`, `wabop_type`) VALUES ( NULL,'$id', :titre, :nombre, :auteur, :editeur, 'livre');";
                                $stmt=$this->connection->prepare($sql);
                                $stmt->bindParam(':titre', $site, \PDO::PARAM_STR);
                                $stmt->bindParam(':nombre', $data['wabop_livre_nombre'][$i], \PDO::PARAM_STR);
                                $stmt->bindParam(':auteur', $data['wabop_auteur'][$i], \PDO::PARAM_STR);
                                $stmt->bindParam(':editeur', $data['wabop_editeur'][$i], \PDO::PARAM_STR);
                                $stmt->execute();
                            }
                        }

                 }elseif($c->getTypeObjet()=="abo_press"){
                        
                            $sql="DELETE FROM wabo where wabo_declar='".$c->wde_declar."';";
                            $stmt=$this->connection->prepare($sql);
                            $stmt->execute();
                            $wabo_fichier="NULL";
                            if(!empty($_FILES['wabo_fichier']['tmp_name'])){
                                $name=$_FILES['wabo_fichier']['name'];
                                $temp_file= $this->kernel->getCacheDir()."/../../tmp_upload/$name";
                                if (!file_exists($this->kernel->getCacheDir()."/../../tmp_upload/")) {
                                    mkdir($this->kernel->getCacheDir()."/../../tmp_upload/", 0777, true);
                                }
                            
                                move_uploaded_file($_FILES['wabo_fichier']['tmp_name'], $temp_file);
                                $mediaManager = $this->container->get('sonata.media.manager.media');
                                $media = new Media();
                                $media->setBinaryContent($temp_file);
                                $media->setContext('abo_press_livres');
                                $media->setAuthorName($c->wde_dossier);
                                $media->setCopyright($c->wde_contrat);
                                $media->setDescription("Déclaraiton : ".$c->wde_declar."\r\nDossier : ".$c->wde_dossier."\r\Contrat : ".$c->wde_contrat."\r\Date : ". date("Y-m-d H:i:s"));
                                $media->setProviderName('sonata.media.provider.file');
                                $ret=$mediaManager->save($media);
                                $wabo_fichier=$media->getId();
                            }elseif(!empty($data['wabo_fichier_media_key']))
                            {
                                $wabo_fichier=$this->checkMediaKey($data['wabo_fichier_media_key']);
                            }
                            $sql="INSERT INTO `wabo` (`wabo_id`, `wabo_declar`, `wabo_dossier`, `wabo_contrat`, `wabo_prestataire`, `wabo_prestataire_label`, `wabo_fichier`, `wabo_type`) VALUES (NULL, '".$c->wde_declar."', '".$c->wde_dossier."', '".$c->wde_contrat."', :wabo_prestataire , :wabo_prestataire_label, $wabo_fichier, '".$c->getTypeObjet()."');";
                            $stmt=$this->connection->prepare($sql);
                            $stmt->bindParam(':wabo_prestataire', $data['wabo_prestataire'], \PDO::PARAM_STR);
                            $stmt->bindParam(':wabo_prestataire_label', $data['wabo_prestataire_label'], \PDO::PARAM_STR);
                     
                        
                            $stmt->execute();
                            $id=$this->connection->lastInsertId();
                        
                            if(!empty($data['wabop_titre'])){
                                foreach($data['wabop_titre'] as $n=>$titre){
                                    $sql="INSERT INTO `wabo_publication` ( `wabop_id`, `wabop_wabo_id`, `wabop_titre`, `wabop_nombre`, `wabop_auteur`, `wabop_editeur`, `wabop_type`) VALUES ( NULL,'$id', :titre, :nombre, NULL, NULL, 'titre');";
                                    $stmt=$this->connection->prepare($sql);
                                    $stmt->bindParam(':titre', $titre, \PDO::PARAM_STR);
                                    $stmt->bindParam(':nombre', $data['wabop_titre_nombre'][$n], \PDO::PARAM_STR);
                                    $stmt->execute();
                                }
                            }
                            if(!empty($data['wabop_site'])){
                                foreach($data['wabop_site'] as $n=>$site){
                                    $sql="INSERT INTO `wabo_publication` ( `wabop_id`, `wabop_wabo_id`, `wabop_titre`, `wabop_nombre`, `wabop_auteur`, `wabop_editeur`, `wabop_type`) VALUES ( NULL,'$id', :site, :nombre,  NULL, NULL, 'site');";
                                    $stmt=$this->connection->prepare($sql);
                                    $stmt->bindParam(':site', $site, \PDO::PARAM_STR);
                                    $stmt->bindParam(':nombre', $data['wabop_site_nombre'][$n], \PDO::PARAM_STR);
                                    $stmt->execute();
                                }
                            }

                }elseif($c->getTypeObjet()=="prestataire_veille_media"){    
                    $sql="DELETE FROM wprestataire where wpresta_declar='".$c->wde_declar."';";
                    $stmt=$this->connection->prepare($sql);
                    $stmt->execute();
                    
                    $sql="INSERT INTO `wprestataire` ( `wpresta_id`, `wpresta_declar`, `wpresta_dossier`, `wpresta_contrat`, `wpresta_prestataire`, `wpresta_prestataire_label`, `wpresta_prestataire_label2`) VALUES ( NULL,'".$c->wde_declar."', '".$c->wde_dossier."', '".$c->wde_contrat."', :wpresta_prestataire, :wpresta_prestataire_label, :wpresta_prestataire_label2 );";
                    $stmt=$this->connection->prepare($sql);
                    $stmt->bindParam(':wpresta_prestataire', $data['wpresta_prestataire'], \PDO::PARAM_INT);
                    $stmt->bindParam(':wpresta_prestataire_label', $data['wpresta_prestataire_label'], \PDO::PARAM_STR);
                    $stmt->bindParam(':wpresta_prestataire_label2', $data['wpresta_prestataire_label2'], \PDO::PARAM_STR);
                    $stmt->execute();
                    
                }elseif($c->getTypeObjet()=="accordion_options"){
                    $field_conf=$c->getTypeObjetConf();
                    $field_index[]=$field_conf['accordion_synchro_field_1'];
                    $field_index[]=$field_conf['accordion_synchro_field_2'];
                      
                        foreach($field_index as $f){
                            $prefix = substr($f, 0, 3);
                            if($prefix == 'wco' && $f != 'wcompos')
                            {
                                $sql="UPDATE wcocon set `".$f."` = :".$f.", wco_stamp=now() WHERE wco_dossier='".$c->wde_dossier."' LIMIT 1";
                                $stmt=$this->connection->prepare($sql);
                                 $stmt->execute([$f=>!empty($data[$f])?$data[$f]:null]);
                            }elseif($prefix=='wde' && $c->getTypeObjet()!="chorus_pro"){
                                $sql="UPDATE wdeclar set `".$f."` = :".$f." , wde_stamp=now() WHERE wde_declar='".$c->wde_declar."' LIMIT 1";
                                $stmt=$this->connection->prepare($sql);
                                 $stmt->execute([$f=>!empty($data[$f])?$data[$f]:null]);
                            }elseif($prefix=='wan'){
                                $wde = $this->registry->getRepository('App\Entity\Wannexe')->createQueryBuilder('w');
                                $wde->where('w.wan_declar  = '.$c->wde_declar);
                                $wde_results = $wde->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
                                if(count($wde_results)){
                                    $sql="UPDATE wannexe set `".$f."` = :".$f." WHERE wan_declar='".$c->wde_declar."' LIMIT 1";
                                }else{
                                    $sql="INSERT INTO `wannexe` (`wan_declar`, `wan_numsite`,`wan_nom`, `wan_tit`, `wan_fct`, `".$f."`) VALUES ('".$c->wde_declar."', '0','', '', '', :".$field.");";
                                }
                            
                                $stmt=$this->connection->prepare($sql);
                                $stmt->execute([$f=>!empty($data[$f])?$data[$f]:null]);
                            }
                        }
                    
                }elseif($c->getTypeObjet()=="relation_publique"){  
                    $sql="DELETE FROM wrelation_publique where wrp_declar='".$c->wde_declar."';";
                    $stmt=$this->connection->prepare($sql);
                    $stmt->execute();
                    if(!empty($data['wrp_label'])){
                        foreach($data['wrp_label'] as $i=>$rp){
                            $sql="INSERT INTO `wrelation_publique` (`wrp_id`, `wrp_stamp`, `wrp_declar`, `wrp_dossier`, `wrp_contrat`, `wrp_label`, `wrp_adresse`, `wrp_cp`, `wrp_ville`, `wrp_volume`) VALUES (NULL, now(), '".$c->wde_declar."', '".$c->wde_dossier."', '".$c->wde_contrat."', :wrp_label, :wrp_adresse, :wrp_cp, :wrp_ville, :wrp_volume);";
                            $stmt=$this->connection->prepare($sql);
                            $stmt->bindParam(':wrp_label',   $data['wrp_label'][$i],    \PDO::PARAM_STR);
                            $stmt->bindParam(':wrp_adresse', $data['wrp_adresse'][$i],  \PDO::PARAM_STR);
                            $stmt->bindParam(':wrp_cp',      $data['wrp_cp'][$i],       \PDO::PARAM_STR);
                            $stmt->bindParam(':wrp_ville',   $data['wrp_ville'][$i],    \PDO::PARAM_STR);
                            $stmt->bindParam(':wrp_volume',  $data['wrp_volume'][$i],   \PDO::PARAM_INT);
                            $stmt->execute();
                            
                        }
                    }
                    
                }elseif($c->getTypeObjet()=="cma_btp"){  
                    $sql="DELETE FROM wrelation_publique where wrp_declar='".$c->wde_declar."';";
                    $stmt=$this->connection->prepare($sql);
                    $stmt->execute();
                    if(!empty($data['wrp_label'])){
                        foreach($data['wrp_label'] as $i=>$rp){
                            $sql="INSERT INTO `wrelation_publique` (`wrp_id`, `wrp_stamp`, `wrp_declar`, `wrp_dossier`, `wrp_contrat`, `wrp_label`, `wrp_adresse`, `wrp_telephone`, `wrp_email`) VALUES (NULL, now(), '".$c->wde_declar."', '".$c->wde_dossier."', '".$c->wde_contrat."', :wrp_label, :wrp_adresse, :wrp_telephone, :wrp_email);";
                            $stmt=$this->connection->prepare($sql);
                            $stmt->bindParam(':wrp_label',   $data['wrp_label'][$i],    \PDO::PARAM_STR);
                            $stmt->bindParam(':wrp_adresse', $data['wrp_adresse'][$i],  \PDO::PARAM_STR);
                            $stmt->bindParam(':wrp_telephone',      $data['wrp_telephone'][$i],       \PDO::PARAM_STR);
                            $stmt->bindParam(':wrp_email',   $data['wrp_email'][$i],    \PDO::PARAM_STR);
                            $stmt->execute();
                            
                        }
                    }
                }elseif($c->getTypeObjet()=="copie_ext_web_reseau"){
                    
                    $sql="DELETE FROM wcpext  where wcpext_declar='".$c->wde_declar."';";
                    $stmt=$this->connection->prepare($sql);
                    $stmt->execute();

                    $sql="DELETE FROM wcpext_mel  where wcpextmel_declar ='".$c->wde_declar."';";
                    $stmt=$this->connection->prepare($sql);
                    $stmt->execute();
                    
                    if(!empty($data['wcpextw'])){
                        foreach($data['wcpextw'] as $i=>$d){
                            $sql="INSERT INTO `wcpext` (`wcpext_id`, `wcpext_stamp`, `wcpext_declar`, `wcpext_dossier`, `wcpext_contrat`, `wcpext_publication`, `wcpext_titre`, `wcpext_auteur`, `wcpext_parution`, `wcpext_date_mel`) VALUES (NULL, CURRENT_TIMESTAMP, '".$c->wde_declar."', '".$c->wde_dossier."', '".$c->wde_contrat."', :wcpext_publication, :wcpext_titre, :wcpext_auteur, :wcpext_parution, :wcpext_date_mel);";
                            $stmt=$this->connection->prepare($sql);
                            $stmt->bindParam(':wcpext_publication',   $d['publication'],    \PDO::PARAM_STR);
                            $stmt->bindParam(':wcpext_titre',   $d['titre'],    \PDO::PARAM_STR);
                            $stmt->bindParam(':wcpext_auteur',   $d['auteur'],    \PDO::PARAM_STR);
                            $stmt->bindParam(':wcpext_parution',   $d['parution'],    \PDO::PARAM_STR);
                            $stmt->bindParam(':wcpext_date_mel',   $d['date_mel'],    \PDO::PARAM_STR);
                            $stmt->execute();
                            $wcpextw_id=$this->connection->lastInsertId();

                            if(!empty($data['wcpextw'][0]['wcpextMel'])){
                                foreach($data['wcpextw'][0]['wcpextMel'] as $j=>$p){
                                    $sql="INSERT INTO `wcpext_mel` (`wcpextmel_id`, `wcpext_id`, `wcpext_stamp`, `wcpextmel_declar`, `wcpextmel_dossier`, `wcpextmel_contrat`, `wcpextmel_mel`) VALUES (NULL, '".$wcpextw_id."', CURRENT_TIMESTAMP, '".$c->wde_declar."', '".$c->wde_dossier."', '".$c->wde_contrat."', :wcpextmel_mel);";
                                    $stmt=$this->connection->prepare($sql);
                                    $stmt->bindParam(':wcpextmel_mel',   $p,    \PDO::PARAM_STR);
                                    $stmt->execute();
                                }   
                            }

                        }
                    }
                }elseif($c->getTypeObjet()=="copie_ext_cible"){
                    $sql="DELETE FROM wcpext  where wcpext_declar='".$c->wde_declar."';";
                    $stmt=$this->connection->prepare($sql);
                    $stmt->execute();

                    if(!empty($data['wcpextc'])){
                        foreach($data['wcpextc'] as $i=>$d){
                            $sql="INSERT INTO `wcpext` (`wcpext_id`, `wcpext_stamp`, `wcpext_declar`, `wcpext_dossier`, `wcpext_contrat`, `wcpext_publication`, `wcpext_titre`, `wcpext_auteur`, `wcpext_parution`, `wcpext_destinataires`) VALUES (NULL, CURRENT_TIMESTAMP, '".$c->wde_declar."', '".$c->wde_dossier."', '".$c->wde_contrat."', :wcpext_publication, :wcpext_titre, :wcpext_auteur, :wcpext_parution, :wcpext_destinataires);";
                            $stmt=$this->connection->prepare($sql);
                            $stmt->bindParam(':wcpext_publication',   $d['publication'],    \PDO::PARAM_STR);
                            $stmt->bindParam(':wcpext_titre',   $d['titre'],    \PDO::PARAM_STR);
                            $stmt->bindParam(':wcpext_auteur',   $d['auteur'],    \PDO::PARAM_STR);
                            $stmt->bindParam(':wcpext_parution',   $d['parution'],    \PDO::PARAM_STR);
                            $stmt->bindParam(':wcpext_destinataires',   $d['destinataires'],    \PDO::PARAM_STR);
                            $stmt->execute();
                        }
                    }
                    

                }elseif($field=="wrgpd"){ 
                
                    $sql="DELETE FROM wrgpd where wrgpd_declar='".$c->wde_declar."';";
                    $stmt=$this->connection->prepare($sql);
                    $stmt->execute();
                    
                    $sql="INSERT INTO `wrgpd` (`wrgpd_id`, `wrgpd_stamp`, `wrgpd_declar`, `wrgpd_dossier`, `wrgpd_contrat`, `wrgpd_value`) VALUES ('', CURRENT_TIMESTAMP, '".$c->wde_declar."', '".$c->wde_dossier."', '".$c->wde_contrat."', :rgpd);";
                    $stmt=$this->connection->prepare($sql);
                    $stmt->bindParam(':rgpd',   $data['wrgpd'],    \PDO::PARAM_INT);
                    $stmt->execute();
                }else{
                    dump($c->getTypeObjet());
                    if(!empty($item)){
                        dump($item);
                        dump($values);
                    }
                   
                   
                }
            }
           
        }

    }
    
    /*
     * Return a an array containing the wde_contrat ids from the declaration conf array
     * */
    public function getListContrats($declaration_conf)
    {
       
        $contrats=[];
        foreach($declaration_conf as $i=>$c)
        {
            if($i>0){
                $contrats[]=$c[0]->wde_contrat;
            }
        }
        if(empty($contrats)){
            $contrats[]=$declaration_conf[0][0]->wde_contrat;
        }

        return array_unique($contrats);
    }
    
    
    public function getListContratsChained($declaration_conf)
    {

        $contrats['chaine']=[];
        $contrats['independant']=[];
        foreach($declaration_conf as $i=>$c)
        {   
                if($c[0]->type_interface==0){
                    //$contrats['chaine'][]=$c[0]->wtype;
                    $contrats['chaine'][]=$c[0];
                }else{
                    //$contrats['independant'][]=$c[0];
                    $contrats['independant'][]=$c[0];
                }
                
        }

        //on verifie les contrats indépendants. Si il y en a 2 du meme type on les chaines.
            $dec_type=[];
            foreach( $contrats['independant'] as $i=>$c){
                if($c->type_etape=="declaration"){
                    $dec_type[$c->wtype->__toString()][]=$i;
                }
                
            }
            foreach($dec_type as $d){
                if($d>1){
                    foreach($d as $c){
                        $contrats['chaine'][]=$contrats['independant'][$c];
                        unset($contrats['independant'][$c]);
                    }

                    
                }
            }
            //peut etre on a migré des contrats en chainé. Dans ce cas, si on a qu'un contrat et que c'est des coordonnées on l'éfface
            if(count($contrats['independant'])==1 && $contrats['independant'][0]->type_etape=="coordonnees")
            {
                unset($contrats['independant'][0]);
            }
        
        $contrats['chaine']=$contrats['chaine'];
        $contrats['independant']=$contrats['independant'];
        return $contrats;
    }
    
    
    
    function getContract($wco_dossier){
        
            $qb=$this->registry->getRepository('App\Entity\Wdeclar')->createQueryBuilder('d');
            $qb->leftJoin(
                'App\Entity\Wtype',
                't',
                \Doctrine\ORM\Query\Expr\Join::WITH,
                'd.wde_typcont = t.label'
                )
                ->where('d.wde_dossier = :wde_dossier')
                ->orderBy('t.priorite', 'ASC')
                ->orderBy('t.id', 'ASC')
                //->AndWhere('t.priorite = :priorite')
                ->setParameter('wde_dossier', $wco_dossier)
                ->setMaxResults(1);
                //->setParameter('priorite', 0);
                list($wdeclar)=$qb->getQuery()->getResult();
        
                /** @var Wtype */
                $wtype = $this->registry
                ->getRepository('App\Entity\Wtype')
                ->findOneBy(['label' => $wdeclar->getWdeTypcont()])
                ;
        
            return array('wdeclar'=>$wdeclar, 'wtype'=>$wtype);

    }
    
    function checkUrl($wdeclar){
        $status=$wdeclar->getWdeEtatDeclar();
        
        $route=$this->requestStack->getCurrentRequest()->get('_route');

        if(in_array($route, ['app_pdf_declaration','declaration_admin_media_download','declaration_admin_composante_presse_download', 'declaration_admin_relation_publique_download', 'app_excell_declaration', 'declaration_admin_abo_presse_livre_download', 'declaration_admin_abo_presse_download', 'declaration_admin_composantes_download', 'declaration_admin_composantes_stages_download']))
        {
            return false;
        }

        $declaration_conf = $this->getDeclarationConfiguration($wdeclar);
        $display_open_interface=false;
        if(!empty($declaration_conf)){
            foreach($declaration_conf as $c){
                if( $c[0]->type_etape=='declaration' &&  in_array($c[0]->wde_etat_declar, ['N','V','A','P','W'])){
                    $display_open_interface=true;
                }
            }
            
            
        }

        if($display_open_interface){
            if(!in_array($route,['interface_open','declaration','interface_contact','security_logout',
                'api-chorus-pro-siret','api-panorama-press-publications', 'api-panorama-press-publications-site', 'api-panorama-press-publications-ipro' , 'api-repertoire-cne-web' , 'api-repertoire-cne-presse-ciblees',
                'validation-success','validations-success', 'app_pdf_declaration','declaration_media_download', 'interface_history', 'interface_history_download_all', 'interface_history_download', 'interface_history_download_last', 'api-contact-remove', 'declaration-independant', 
                'interface_bdc', 'interface_bdc_success'])){
                return 'interface_open';
            }
        }else{
            if(!in_array($route,['interface_closed','interface_closed_coordonnees', 'interface_closed_coordonnees_success', 'interface_contact','security_logout','api-chorus-pro-siret','app_pdf_declaration','declaration_media_download', 'interface_history', 'interface_history_download_all', 'interface_history_download', 'interface_history_download_last', 'api-contact-remove'])){
                return 'interface_closed';
            }
        }
        
        /*sm($declaration_conf);
        die();
        switch($status){
            case 'V':
            case 'N':
            case 'A':
                if(!in_array($route,['interface_open','declaration','interface_contact','security_logout','api-chorus-pro-siret','api-panorama-press-publications', 'api-panorama-press-publications-site', 'api-panorama-press-publications-ipro' ,'validation-success','validations-success', 'app_pdf_declaration','declaration_media_download', 'interface_history', 'interface_history_download_all', 'interface_history_download', 'interface_history_download_last', 'api-contact-remove', 'declaration-independant'])){
                    return 'interface_open';
                }
                break;
            case 'P':
                if(!in_array($route,["interface_bdc",'security_logout'])){
                    return 'interface_bdc';
                }
                break;
            case 'W':
                    if(!in_array($route,["interface_bdc_success",'security_logout'])){
                    return 'interface_bdc_success';
                    }
                break;
            case 'F':
            case 'X':
            case 'I':
                if(!in_array($route,['interface_closed','interface_closed_coordonnees', 'interface_closed_coordonnees_success', 'interface_contact','security_logout','api-chorus-pro-siret','app_pdf_declaration','declaration_media_download', 'interface_history', 'interface_history_download_all', 'interface_history_download', 'interface_history_download_last', 'api-contact-remove'])){
                    return 'interface_closed';
                }
                break;
                
                
                
            default: return 'index';
        };
        */
        return false;
 
    }
    
    public function GenerateExcell(int $wdeclarId){
        
        $WcoconRepository=$this->registry->getRepository(Wcocon::class);
        $WdeclarRepository=$this->registry->getRepository(Wdeclar::class);
        $WtypeRepository=$this->registry->getRepository(Wtype::class);
        
        /*$primary_declaration = $WdeclarRepository->findOneBy(["wde_declar" => $wdeclar]);
        $wcocon = $WcoconRepository->findOneBy(["wco_dossier" => $primary_declaration->getWdeDossier()]);
        $contrat=$primary_declaration->getWdeTypcont();
        $declar_id=$primary_declaration->getWdeContrat();
        $wtype_id=$WtypeRepository->findOneBy(["label" => $contrat])->getId();
        
        
        $declaration_conf = $WcoconRepository->getDeclarationConfiguration($wcocon, $wtype_id);
        $contrats = $this->getListContrats($declaration_conf);
        */
        
        
        
        $WcoconRepository=$this->registry->getRepository(Wcocon::class);
        $WdeclarRepository=$this->registry->getRepository(Wdeclar::class);
        $WtypeRepository=$this->registry->getRepository(Wtype::class);
        
        $wdeclar = $WdeclarRepository->findOneBy(["wde_declar" => $wdeclarId]);
        $wcocon = $WcoconRepository->findOneBy(["wco_dossier" => $wdeclar->getWdeDossier()]);;
        $contrat=$wdeclar->getWdeTypcont();
        $wtype_id=$WtypeRepository->findOneBy(["label" => $contrat])->getId();
        
        
        //$declaration_conf = $WcoconRepository->getDeclarationConfiguration($wcocon, $wtype_id);
        $declaration_conf = $WcoconRepository->getDeclarationConfigurationByWdeclar($wdeclar);
        $contrats = $this->getListContrats($declaration_conf);        
        
        

        $projectRoot = $this->kernel->getProjectDir();
        if (!file_exists($projectRoot."/var/tmp_upload/")) {
            mkdir($projectRoot."/var/tmp_upload/", 0777, true);
        }
        
        $temp_file= $projectRoot."/var/tmp_upload/tmp_excel_".time().".xls";
        
        
        $phpExcel = new Spreadsheet();
        $phpExcel->getProperties()->setCreator('CFC - extranet')
        ->setTitle('Declaration : '.$wdeclar->getWdeContrat())
        ->setSubject('Declaration : '.$wdeclar->getWdeContrat())
        ->setDescription('Declaration : '.$wdeclar->getWdeContrat());
        $invalidCharacters = array('*', ':', '/', '\\', '?', '[', ']');
        $sheet=$phpExcel->setActiveSheetIndex(0);
        $sheet->setTitle(Urlizer::urlize($contrat));
    
        $sheet_index=0;
        $column=1;
        foreach($declaration_conf as $i=>$conf){
            foreach($conf as $j=>$c){
                if($c->getTypeObjet()=="chorus_pro" || $c->getTypeObjet()=="chorus_pro_extended"){
                    $db_data_conf[$i]["wde_is_chorus_pro"]=$this->getValue("wde_is_chorus_pro", $c->wde_dossier, $c->wde_declar);
                    $db_data_conf[$i]["wde_siret"]=$this->getValue("wde_siret", $c->wde_dossier, $c->wde_declar);
                    $db_data_conf[$i]["wde_service_code"]=$this->getValue("wde_service_code", $c->wde_dossier, $c->wde_declar);
                    $db_data_conf[$i]["wde_is_order_number"]=$this->getValue("wde_is_order_number", $c->wde_dossier, $c->wde_declar);
                    $db_data_conf[$i]["wde_order_number"]=$this->getValue("wde_order_number", $c->wde_dossier, $c->wde_declar);
                
                    
                    $sheet->setCellValue(Coordinate::stringFromColumnIndex($column).'1','Souhaitez-vous recevoir votre facture via chorus pro ?');
                    $sheet->setCellValue(Coordinate::stringFromColumnIndex($column).'2', $db_data_conf[$i]["wde_is_chorus_pro"]==true?'oui':'non');
                    $sheet->getColumnDimension(Coordinate::stringFromColumnIndex($column))->setAutoSize(true);
                    $column++;
                    
                    $sheet->setCellValue(Coordinate::stringFromColumnIndex($column).'1','N° de siret/ridet');
                    $sheet->setCellValue(Coordinate::stringFromColumnIndex($column).'2', $db_data_conf[$i]["wde_siret"]);
                    $sheet->getColumnDimension(Coordinate::stringFromColumnIndex($column))->setAutoSize(true);
                    $column++;
                    
                    $sheet->setCellValue(Coordinate::stringFromColumnIndex($column).'1','Code service');
                    $sheet->setCellValue(Coordinate::stringFromColumnIndex($column).'2', $db_data_conf[$i]["wde_service_code"]);
                    $sheet->getColumnDimension(Coordinate::stringFromColumnIndex($column))->setAutoSize(true);
                    $column++;
                    
                    $sheet->setCellValue(Coordinate::stringFromColumnIndex($column).'1','Un bon de commande / engagement doit-il figurer sur la facture ?');
                    $sheet->setCellValue(Coordinate::stringFromColumnIndex($column).'2', !empty($db_data_conf[$i]["wde_is_order_number"])?'oui':'non');
                    $sheet->getColumnDimension(Coordinate::stringFromColumnIndex($column))->setAutoSize(true);
                    $column++;
                    
                    $sheet->setCellValue(Coordinate::stringFromColumnIndex($column).'1','N° de bon de commande / Engagement');
                    $sheet->setCellValue(Coordinate::stringFromColumnIndex($column).'2', $db_data_conf[$i]["wde_order_number"]);
                    $sheet->getColumnDimension(Coordinate::stringFromColumnIndex($column))->setAutoSize(true);
                    $column++;
                    
                   
                
                
                }elseif($c->getTypeObjet()=="panorama_press_numerique"){
                $db_data_conf[$i]["panorama_press"]=$WcoconRepository->getValue("panorama_press", $c->wde_dossier, $c->wde_declar);
                    foreach($db_data_conf[$i]["panorama_press"]["wpanoramapress_liste"] as $i=>$wpanoramapress_liste){
                        //on genere le panorama press
                        if(!empty($wpanoramapress_liste['wppl_id'])){
                            $generated_ppress=$this->GenerateExcellPanoramaPresseNumerique($wpanoramapress_liste['wppl_id'], ($i+1));
                            //on lit le fichier
                            $MergeReader = IOFactory::createReader('Xls');
                            $MergeExcel = $MergeReader->load($generated_ppress['path']);
                            $sheetToMerge=$MergeExcel->setActiveSheetIndex(0);
                            $sheetToMerge->setTitle(substr('Panorama '.($i+1).'- ' . str_replace($invalidCharacters, '', $wpanoramapress_liste['wppl_label']),0,30) );
                            $phpExcel->addExternalSheet($sheetToMerge);
                            $sheet_index++;
                        }
                        


                        
                    }

                }elseif($c->getTypeObjet()=="panorama_press_papier"){
                    $db_data_conf[$i]["panorama_press"]=$WcoconRepository->getValue("panorama_press", $c->wde_dossier, $c->wde_declar);
                    foreach($db_data_conf[$i]["panorama_press"]["wpanoramapress_liste"] as $wpanoramapress_liste){
                        if(!empty($wpanoramapress_liste['wppl_id'])){
                            //on genere le panorama press
                            $generated_ppress=$this->GenerateExcellPanoramaPressePapier($wpanoramapress_liste['wppl_id']);
                            
                            //on lit le fichier
                            $MergeReader = IOFactory::createReader('Xls');
                            $MergeExcel = $MergeReader->load($generated_ppress['path']);
                            $sheetToMerge=$MergeExcel->setActiveSheetIndex(0);
                            $sheetToMerge->setTitle(substr( 'Panorama - ' . str_replace($invalidCharacters, '', $wpanoramapress_liste['wppl_label']),0,30) );
                            $phpExcel->addExternalSheet($sheetToMerge);
                            $sheet_index++;
                        }

                        
                    }
                
                }elseif($c->getTypeObjet()=="file_upload")
                {
                    $db_data_conf[$i][$c->getSynchroField()]=$WcoconRepository->getValue($c->getSynchroField(), $c->wde_dossier, $c->wde_declar);
                    if(!empty($db_data_conf[$i][$c->getSynchroField()])){
        
        
                        $mediaManager = $this->container->get('sonata.media.manager.media');
                        $media=$mediaManager->find($db_data_conf[$i][$c->getSynchroField()]);
                        $file_name=$media->getName();
                        $file_reference=$this->provider->getReferenceFile($media,'reference')->getName();
                        $media_key=md5($db_data_conf[$i][$c->getSynchroField()].$this->download_key).'%'.$db_data_conf[$i][$c->getSynchroField()];
                        $db_data_conf[$i]['file_upload'][$c->getSynchroField()]=[
                            'file_upload_fichier_name'=>$file_name,
                            'file_upload_fichier_reference'=>$file_reference,
                            'file_upload_fichier_media_key'=>$media_key,
                        ];
                    }else{
                        $db_data_conf[$i]['file_upload'][$c->getSynchroField()]=[
                            'file_upload_fichier_name'=>"",
                            'file_upload_fichier_reference'=>"",
                            'file_upload_fichier_media_key'=>"",
                        ];
                    }

                    if($declaration_conf[$i][$j-1]->getTypeObjet()=="title"){
                        $title=strip_tags($declaration_conf[$i][$j-1]->getLabel());
                    }else{
                        $title=$declaration_conf[$i][$j]->getLabel();
                    }
                    $sheet->setCellValue(Coordinate::stringFromColumnIndex($column).'1', $title);
                    $sheet->setCellValue(Coordinate::stringFromColumnIndex($column).'2', $db_data_conf[$i]['file_upload'][$c->getSynchroField()]['file_upload_fichier_name']);
                    $sheet->getCell(Coordinate::stringFromColumnIndex($column).'2')->getHyperlink()->setUrl('http://'.$_SERVER['HTTP_HOST'].'/upload/media/'.$db_data_conf[$i]['file_upload'][$c->getSynchroField()]['file_upload_fichier_reference']);
                    
                    $sheet->getColumnDimension(Coordinate::stringFromColumnIndex($column))->setAutoSize(true);
                    $column++;
                    
                    
                    
                    
                }elseif($c->getTypeObjet()=="accordion_options")
                {
                    $TypeObjetConf=$c->getTypeObjetConf();
                    
                    if($declaration_conf[$i][$j-1]->getTypeObjet()=="title"){
                        $title=strip_tags($declaration_conf[$i][$j-1]->getLabel());
                    }else{
                        $title=$declaration_conf[$i][$j]->getLabel();
                    }
                    
                    $db_data_conf[$i][$TypeObjetConf['accordion_synchro_field_1']]=$WcoconRepository->getValue($TypeObjetConf['accordion_synchro_field_1'], $c->wde_dossier, $c->wde_declar);
                    $db_data_conf[$i][$TypeObjetConf['accordion_synchro_field_2']]=$WcoconRepository->getValue($TypeObjetConf['accordion_synchro_field_2'], $c->wde_dossier, $c->wde_declar);
                
                    
                    $sheet->setCellValue(Coordinate::stringFromColumnIndex($column).'1', $title.' : '.strip_tags(str_replace('*','',$TypeObjetConf['accordion_value_label'])));
                    $sheet->setCellValue(Coordinate::stringFromColumnIndex($column).'2', $db_data_conf[$i][$TypeObjetConf['accordion_synchro_field_1']]);
                    $sheet->getColumnDimension(Coordinate::stringFromColumnIndex($column))->setAutoSize(true);
                    $column++;
                    
                    if(!empty($db_data_conf[$i][$TypeObjetConf['accordion_synchro_field_1']]))
                    {
                        if($db_data_conf[$i][$TypeObjetConf['accordion_synchro_field_2']]==1){
                            $label=$TypeObjetConf['accordion_option1_label'];
                        }else{
                            $label=$TypeObjetConf['accordion_option2_label'];
                        }
                    }else{
                        $label='';
                    }
                   
                    $sheet->setCellValue(Coordinate::stringFromColumnIndex($column).'1', $title.' : '.strip_tags(str_replace('*','',$TypeObjetConf['accordion_option_title'])));
                    $sheet->setCellValue(Coordinate::stringFromColumnIndex($column).'2', strip_tags($label));
                    $sheet->getColumnDimension(Coordinate::stringFromColumnIndex($column))->setAutoSize(true);
                    $column++;
                    
                

                }elseif($c->getTypeObjet()=="relation_publique")
                {
                    //on genere le fichier relation public
                    $generated_rp=$this->GenerateExcellRelationPublique($c->wde_declar);
                    
                    //on lit le fichier
                    $MergeReader = IOFactory::createReader('Xls');
                    $MergeExcel = $MergeReader->load($generated_rp['path']);
                    $sheetToMerge=$MergeExcel->setActiveSheetIndex(0);
                    $sheetToMerge->setTitle('Relation publique');
                    $phpExcel->addExternalSheet($sheetToMerge);
                    $sheet_index++;
                    
                 }elseif($c->getTypeObjet()=="cma_btp")
                 {
                        //on genere le fichier relation public
                        $generated_rp=$this->GenerateExcellCmaBtp($c->wde_declar);
                    
                        //on lit le fichier
                        $MergeReader = IOFactory::createReader('Xls');
                        $MergeExcel = $MergeReader->load($generated_rp['path']);
                        $sheetToMerge=$MergeExcel->setActiveSheetIndex(0);
                        $sheetToMerge->setTitle('Sites');
                        $phpExcel->addExternalSheet($sheetToMerge);
                        $sheet_index++;
                        
                }elseif($c->getTypeObjet()=="prestataire_veille_media"){
                    $sheet=$phpExcel->setActiveSheetIndex(0);
                    $value=$this->getValue($c->getSynchroField(), $c->wde_dossier, $c->wde_declar);
  
                    
                    $sheet->setCellValue(Coordinate::stringFromColumnIndex($column).'1','Avez-vous un prestataire de veille media ?');
                    $sheet->setCellValue(Coordinate::stringFromColumnIndex($column).'2', $value['wpresta_prestataire']==true?'oui':'non');
                    $sheet->getColumnDimension(Coordinate::stringFromColumnIndex($column))->setAutoSize(true);
                    $column++;
                    
                    
                    $sheet->setCellValue(Coordinate::stringFromColumnIndex($column).'1','lequel');
                    $sheet->setCellValue(Coordinate::stringFromColumnIndex($column).'2', $value['wpresta_prestataire_label'].(!empty($value['wpresta_prestataire_label2'])?", ". $value['wpresta_prestataire_label2']:null));
                    $sheet->getColumnDimension(Coordinate::stringFromColumnIndex($column))->setAutoSize(true);
                    $column++;
                    
               }elseif($c->getTypeObjet()=="wcompos"){
                   $generated_composantes=$this->GenerateExcellComposantes($c->wde_declar);
                   //on lit le fichier
                   $MergeReader = IOFactory::createReader('Xls');
                   $MergeExcel = $MergeReader->load($generated_composantes['path']);
                   $sheetToMerge=$MergeExcel->setActiveSheetIndex(0);
                   $sheetToMerge->setTitle('Composantes');
                   $phpExcel->addExternalSheet($sheetToMerge);
                   $sheet_index++;
                   
               }elseif($c->getTypeObjet()=="wcompos_stages"){
                   $generated_composantes_stages=$this->GenerateExcellComposantesStages($c->wde_declar);
                   //on lit le fichier
                   $MergeReader = IOFactory::createReader('Xls');
                   $MergeExcel = $MergeReader->load($generated_composantes_stages['path']);
                   $sheetToMerge=$MergeExcel->setActiveSheetIndex(0);
                   $sheetToMerge->setTitle('Composantes stages');
                   $phpExcel->addExternalSheet($sheetToMerge);
                   $sheet_index++;                   
               }elseif($c->getTypeObjet()=="abo_press"){    
                    
                    //on genere le fichier relation public
                    $generated_presse=$this->GenerateExcellAboPresse($c->wde_declar);

                    //on lit le fichier
                    $MergeReader = IOFactory::createReader('Xls');
                    $MergeExcel = $MergeReader->load($generated_presse['path']);
                    $sheetToMerge=$MergeExcel->setActiveSheetIndex(0);
                    $sheetToMerge->setTitle('Titres et sites de presse');
                    $phpExcel->addExternalSheet($sheetToMerge);
                    //$MergeReader = IOFactory::createReader('Xls');
                    //$MergeExcel = $MergeReader->load($generated_presse['path']);
                    //$sheetToMerge=$MergeExcel->setActiveSheetIndex(1);
                    //$sheetToMerge->setTitle('Sites de presse');
                    //$phpExcel->addExternalSheet($sheetToMerge);
                    $sheet_index++;
                }elseif($c->getTypeObjet()=="abo_press_livres"){
                
                    //on genere le fichier relation public
                    $generated_presse=$this->GenerateExcellAboPresseLivre($c->wde_declar);
                
                    //on lit le fichier
                    $MergeReader = IOFactory::createReader('Xls');
                    $MergeExcel = $MergeReader->load($generated_presse['path']);
                    $sheetToMerge=$MergeExcel->setActiveSheetIndex(0);
                    $sheetToMerge->setTitle('Cipro-presse&livres');
                    $phpExcel->addExternalSheet($sheetToMerge);
                    $sheet_index++;
                    
                   /* $MergeReader = IOFactory::createReader('Xls');
                    $MergeExcel = $MergeReader->load($generated_presse['path']);
                    $sheetToMerge=$MergeExcel->setActiveSheetIndex(1);
                    $sheetToMerge->setTitle('Sites de presse');
                    $phpExcel->addExternalSheet($sheetToMerge);
                    $sheet_index++;
                    
                    $MergeReader = IOFactory::createReader('Xls');
                    $MergeExcel = $MergeReader->load($generated_presse['path']);
                    $sheetToMerge=$MergeExcel->setActiveSheetIndex(2);
                    $sheetToMerge->setTitle('Abonnements livres');
                    $phpExcel->addExternalSheet($sheetToMerge);
                    $sheet_index++;*/
                }elseif($c->getTypeObjet()=="copie_ext_cible"){
                    //on genere le fichier copie_ext_cible
                    $generated_copie_ext_cible=$this->GenerateExcellCopieExtCible($c->wde_declar);
                    //on lit le fichier
                    $MergeReader = IOFactory::createReader('Xls');
                    $MergeExcel = $MergeReader->load($generated_copie_ext_cible['path']);
                    $sheetToMerge=$MergeExcel->setActiveSheetIndex(0);
                    $sheetToMerge->setTitle('Copie externe Cible');
                    $phpExcel->addExternalSheet($sheetToMerge);
                    $sheet_index++;
                }elseif($c->getTypeObjet()=="copie_ext_web_reseau"){
                        //on genere le fichier copie_ext_cible
                        $generated_copie_ext_reseau=$this->GenerateExcellCopieExtReseau($c->wde_declar);
                        //on lit le fichier
                        $MergeReader = IOFactory::createReader('Xls');
                        $MergeExcel = $MergeReader->load($generated_copie_ext_reseau['path']);
                        $sheetToMerge=$MergeExcel->setActiveSheetIndex(0);
                        $sheetToMerge->setTitle('Copie externe Réseau');
                        $phpExcel->addExternalSheet($sheetToMerge);
                        $sheet_index++;
                }elseif($c->getTypeObjet()=="radio"){
                    if (!empty($c->getSynchroField()) && !in_array($c->getTypeObjet(), ["title"])) {
                        $sheet=$phpExcel->setActiveSheetIndex(0);
                        
                        $db_data_conf[$i][$c->getSynchroField()]=$this->getValue($c->getSynchroField(), $c->wde_dossier, $c->wde_declar);
                        $obj_conf=$c->getTypeObjetConf();
                        $value=$this->getValue($c->getSynchroField(), $c->wde_dossier, $c->wde_declar);
                        $expl_value=explode("|",$obj_conf['list_value_'.$value]);
                        $sheet->setCellValue(Coordinate::stringFromColumnIndex($column).'1',strip_tags($c->getLabel()));
                        $sheet->setCellValue(Coordinate::stringFromColumnIndex($column).'2', $expl_value[1]);
                        $sheet->getColumnDimension(Coordinate::stringFromColumnIndex($column))->setAutoSize(true);
                        $column++;
                    }
                }else{
                   
                    if (!empty($c->getSynchroField()) && !in_array($c->getTypeObjet(), ["title"])) {
                        $sheet=$phpExcel->setActiveSheetIndex(0);
                       

                        $db_data_conf[$i][$c->getSynchroField()]=$this->getValue($c->getSynchroField(), $c->wde_dossier, $c->wde_declar);
                        $value=$this->getValue($c->getSynchroField(), $c->wde_dossier, $c->wde_declar);
                        $sheet->setCellValue(Coordinate::stringFromColumnIndex($column).'1',strip_tags($c->getLabel()));
                        $sheet->setCellValue(Coordinate::stringFromColumnIndex($column).'2', $value);
                        $sheet->getColumnDimension(Coordinate::stringFromColumnIndex($column))->setAutoSize(true);
                        $column++;
                    }
                }
            }
        }


        $writer = new Xlsx($phpExcel);
        $writer->save($temp_file);
        
        $urlized=Urlizer::urlize($contrat);
        
        return ['name'=>''.$urlized.'_'.$wdeclar->getWdeContrat().'.xlsx', 'path'=>$temp_file];
        return ['name'=>''.$urlized.'_'.$wdeclarId.'.xls', 'path'=>$temp_file];

    }
    
    public function GeneratePdf(int $wdeclarId, $validation=false)
    {
        $WcoconRepository=$this->registry->getRepository(Wcocon::class);
        $WdeclarRepository=$this->registry->getRepository(Wdeclar::class);
        $WtypeRepository=$this->registry->getRepository(Wtype::class);
        
        $wdeclar = $WdeclarRepository->findOneBy(["wde_declar" => $wdeclarId]);
        $wcocon = $WcoconRepository->findOneBy(["wco_dossier" => $wdeclar->getWdeDossier()]);
        //$declaration_conf = $WcoconRepository->getDeclarationConfiguration($wcocon);
        //$contrats = $this->getListContrats($declaration_conf);
        $contrat=$wdeclar->getWdeTypcont();
        $wtype_id=$WtypeRepository->findOneBy(["label" => $contrat])->getId();

        
        //$declaration_conf = $WcoconRepository->getDeclarationConfiguration($wcocon, $wtype_id);
        $declaration_conf = $WcoconRepository->getDeclarationConfigurationByWdeclar($wdeclar);
        $contrats = $this->getListContrats($declaration_conf);

        
        foreach($declaration_conf as $i=>$conf){
            foreach($conf as $j=>$c){
                if($c->getTypeObjet()=="chorus_pro" || $c->getTypeObjet()=="chorus_pro_extended"){
                    $db_data_conf[$i]["wde_is_chorus_pro"]=$this->getValue("wde_is_chorus_pro", $c->wde_dossier, $c->wde_declar);
                    $db_data_conf[$i]["wde_siret"]=$this->getValue("wde_siret", $c->wde_dossier, $c->wde_declar);
                    $db_data_conf[$i]["wde_service_code"]=$this->getValue("wde_service_code", $c->wde_dossier, $c->wde_declar);
                    $db_data_conf[$i]["wde_is_order_number"]=$this->getValue("wde_is_order_number", $c->wde_dossier, $c->wde_declar);
                    $db_data_conf[$i]["wde_order_number"]=$this->getValue("wde_order_number", $c->wde_dossier, $c->wde_declar);
                }elseif($c->getTypeObjet()=="panorama_press_numerique"){
                    $db_data_conf[$i]["panorama_press"]=$WcoconRepository->getValue("panorama_press", $c->wde_dossier, $c->wde_declar);
                    
                    
                    
                }elseif($c->getTypeObjet()=="panorama_press_papier"){
                        $db_data_conf[$i]["panorama_press"]=$WcoconRepository->getValue("panorama_press", $c->wde_dossier, $c->wde_declar);
                }elseif($c->getTypeObjet()=="file_upload")
                {
                    $db_data_conf[$i][$c->getSynchroField()]=$WcoconRepository->getValue($c->getSynchroField(), $c->wde_dossier, $c->wde_declar);
                    if(!empty($db_data_conf[$i][$c->getSynchroField()])){
                        
                        
                        $mediaManager = $this->container->get('sonata.media.manager.media');
                        $media=$mediaManager->find($db_data_conf[$i][$c->getSynchroField()]);
                        $file_name=$media->getName();
                        $file_reference=$this->provider->getReferenceFile($media,'reference')->getName();
                        $media_key=md5($db_data_conf[$i][$c->getSynchroField()].$this->download_key).'%'.$db_data_conf[$i][$c->getSynchroField()];
                        $db_data_conf[$i]['file_upload'][$c->getSynchroField()]=[
                            'file_upload_fichier_name'=>$file_name,
                            'file_upload_fichier_reference'=>$file_reference,
                            'file_upload_fichier_media_key'=>$media_key,
                        ];
                    }else{
                        $db_data_conf[$i]['file_upload'][$c->getSynchroField()]=[
                            'file_upload_fichier_name'=>"",
                            'file_upload_fichier_reference'=>"",
                            'file_upload_fichier_media_key'=>"",
                        ];
                    }
                }elseif($c->getTypeObjet()=="accordion_options")
                {
                        $TypeObjetConf=$c->getTypeObjetConf();
                        $db_data_conf[$i][$TypeObjetConf['accordion_synchro_field_1']]=$WcoconRepository->getValue($TypeObjetConf['accordion_synchro_field_1'], $c->wde_dossier, $c->wde_declar);
                        $db_data_conf[$i][$TypeObjetConf['accordion_synchro_field_2']]=$WcoconRepository->getValue($TypeObjetConf['accordion_synchro_field_2'], $c->wde_dossier, $c->wde_declar);
                    
                }else{
                    if (!empty($c->getSynchroField())) {
                        $db_data_conf[$i][$c->getSynchroField()]=$this->getValue($c->getSynchroField(), $c->wde_dossier, $c->wde_declar);
                        $db_data_conf[$i]["wde_order_number"]=$this->getValue("wde_order_number", $c->wde_dossier, $c->wde_declar);
                    }
                }
            }
        }
        
        $typcont = $declaration_conf[1][0]->getWconfTypcont()->getLabel();
        
        $declaration_conf=$this->sanitizePdfFields($declaration_conf,$db_data_conf);
        
       
        $pdf_css=file_get_contents($this->parameter->get('kernel.project_dir') . '/public/css/pdf.css');
        //$bootstrap_css=file_get_contents($this->parameter->get('kernel.project_dir') . '/public/css/bootstrap-4.4.1/css/bootstrap.min.css');
        $content = $this->twig->render('interface/declaration/pdf.html.twig', [
            'bootstrap_css'=>'',
            'primary_declaration'=>$wdeclar,
            'declaration_conf'=>$declaration_conf,
            'db_data_conf'=>$db_data_conf,
            'wcocon'=>$wcocon,
            'contrats'=>$contrats,
            'ROOT_DIR'=>$this->parameter->get('kernel.project_dir')
        ]);

        //type contrat is allways the first declaration of the list of conf
        //$typcont = $declaration_conf[1][0]->getWconfTypcont()->getLabel();

        /*
    echo "<style>".$pdf_css.'</style>'.$content;
    die();
  */
    
        if($validation){
            $folder_name = str_replace(' / ', '', $typcont);
            $folder_name = str_replace('/', '', $folder_name);
            $folder_name = str_replace(' ', '', $folder_name);
            $folder_year = str_replace('/', '-', $wdeclar->getWdeAnnee());
            
            $pdf_path = $this->parameter->get('kernel.project_dir') . '/public/pdf/' . $folder_name . '/'.$folder_year.'/';
            $pdf_name = $folder_name.'_'. $wdeclar->getWdeContrat() . '.pdf';
            $pdf_file=$pdf_path.$pdf_name;
        }else{
            $projectRoot = $this->parameter->get('kernel.project_dir');
            $pdf_path= $projectRoot."/var/tmp_upload/";
            if (!file_exists($pdf_path)) {
                mkdir($pdf_path, 0777, true);
            }
            
            $pdf_file= $pdf_path."/tmp_pdf_".time().".pdf";
        }
        

        $fileSystem = new Filesystem();
        try {
            $fileSystem->mkdir($pdf_path);
        }
        catch (IOExceptionInterface $e) {
            $this->logger->info($e->getMessage());
            return FALSE;
        }
    
        
    
        $tmp_path = sys_get_temp_dir();
        
        
        
        
        $defaultConfig = (new \Mpdf\Config\ConfigVariables())->getDefaults();
        $fontDirs = $defaultConfig['fontDir'];
        $defaultFontConfig = (new \Mpdf\Config\FontVariables())->getDefaults();
        $fontData = $defaultFontConfig['fontdata'];

      

        try {
            $mpdf = new Mpdf([
                'mode' => 'utf-8', 
                'format' => 'A4',
                'tempDir' => $tmp_path,
                'fontDir' => array_merge($fontDirs, [$this->parameter->get('kernel.project_dir').'/assets/fonts/montserrat/']),
                'fontdata' => $fontData + [
                    'montserratsemibold' => [
                        'R' => 'Montserrat-SemiBold.ttf',
                        'I' => 'Montserrat-SemiBold.ttf',
                    ],
                    'montserrat' => [
                        'R' => 'Montserrat-Regular.ttf',
                        'I' => 'Montserrat-Regular.ttf',
                    ]
                    ,
                    'montserratbold' => [
                        'R' => 'Montserrat-Bold.ttf',
                        'I' => 'Montserrat-Bold.ttf',
                    ]
                ],
                'default_font' => 'dejavusans'
                /**/
            ]);
            
            $mpdf->WriteHTML($pdf_css, \Mpdf\HTMLParserMode::HEADER_CSS);
            $mpdf->WriteHTML($content, \Mpdf\HTMLParserMode::HTML_BODY);

            $pdf_content = $mpdf->Output($pdf_file, Destination::FILE);
        } catch (MpdfException $e) {
            if(!empty($this->logger)){
                $this->logger->info($e->getMessage());
            }else{
                dump($e->getMessage());
            }
            die();
            return FALSE;
        }
    
        return ['path' => $pdf_file];
    }
    
    public function sanitizePdfFields($declaration_conf,$db_data_conf)
    {
       // sm($declaration_conf);
        $sanitize=[];
        //first we remove empty items
       /* foreach($declaration_conf as $i=>$conf){
                foreach($conf as $j=>$c){
                    if($c->getTypeObjet()=="chorus_pro" || $c->getTypeObjet()=="chorus_pro_extended"){
                        $sanitize[$i][]=$declaration_conf[$i][$j];
                    }elseif($c->getTypeObjet()=="panorama_press_numerique" || $c->getTypeObjet()=="panorama_press_papier"){
                        $sanitize[$i][]=$declaration_conf[$i][$j];
                        $data=$db_data_conf[$i][$c->getSynchroField()];
                        $db_data_conf[$i][$c->getSynchroField()]=$data;
                        //sm($db_data_conf[$i][$c->getSynchroField()]);
                    }elseif($c->getTypeObjet()=="accordion_options"){
                        $sanitize[$i][]=$declaration_conf[$i][$j];
                    }elseif($c->getTypeObjet()=="file_upload"){
                        $sanitize[$i][]=$declaration_conf[$i][$j];
                    }else{
                        if (!empty($c->getSynchroField()) && empty($db_data_conf[$i][$c->getSynchroField()]) ) {
                            //sm($db_data_conf[$i][$c->getSynchroField()]);
                            unset($declaration_conf[$i][$j]);
                        }else{
                            $sanitize[$i][]=$declaration_conf[$i][$j];
                            
                            
                        }
                        
                    }
                }
            }*/


            //we reorganise items by hierachy
            $cat=[];
            $cat_index=0;
            foreach($declaration_conf as $i=>$conf){
                foreach($conf as $j=>$c){
                  // sm($declaration_conf[$i][$j]->getTypeObjet());
                    if($declaration_conf[$i][$j]->getTypeObjet()=="title" || $declaration_conf[$i][$j]->getTypeObjet()=="line_separator" || $declaration_conf[$i][$j]->getTypeObjet()=="rgpd"){
                        $cat_index++;
                        $cat[$i][$cat_index][]=$declaration_conf[$i][$j];
                    }else
                    {
                        $cat[$i][$cat_index][]=$declaration_conf[$i][$j];
                    }
                    
                }
                
            }
            
            //sm($cat);
            
            foreach($cat as $i=>$declar){
                foreach($declar as $section_index=>$sections)
                {
                  
                  
                    //$sections[0] are allways title
                    if(!empty($sections[1]) && $sections[1]->getTypeObjet()=="civilite")
                    {
                        if(!empty($sections[2])){
                            $val1=@$db_data_conf[$i][$sections[2]->getSynchroField()];
                        }else{
                            $val1=false;
                        }
                        
                        if(!empty($sections[3])){
                            $val2=@$db_data_conf[$i][$sections[3]->getSynchroField()];
                        }else{
                            $val2=false;
                        }
                        
                        if(!empty($sections[4])){
                            $val3=@$db_data_conf[$i][$sections[4]->getSynchroField()];
                        }else{
                            $val3=false;
                        }
                        
                        //bon si les trois valeurs suivantes sont vide, c'est que c'est pas remplis. On supprime le bloc
                        if(empty($val1) && empty($val2) && empty($val3))
                        {
                            unset($cat[$i][$section_index]);
                        }
                    }

                   
                    
                    //traitement des champs vides dans les adresses
                    //si on est dans l'indice 0 donc dans la déclaration et qu'il y a plus de 5 éléments, alors on est dans une adresse d'établissement.
                    if($i==0 && count($sections)>6){
                       // sm($sections);
                        foreach($sections as $js=>$s){
                            //si t'as un champs synchro
                            if(!empty($s->getSynchroField())){
                                //mais que tu es vide, alors tu jarte.. tutut j'ai dit tu jarte.
                                if(empty($db_data_conf[$i][$s->getSynchroField()])){
                                    unset($cat[$i][$section_index][$js]);
                                }
                                
                            }
                            
                        }
                   
                        //sm('#############################');
                        //sm('#############################');
                        //sm('#############################');
                        $is_not_only_civil=false;
                        foreach($sections as $js=>$s){
                           //sm($s->type_object);
                           if(!empty($s->getSynchroField()) && !empty($db_data_conf[$i][$s->getSynchroField()]) ){
                               //echo $db_data_conf[$i][$s->getSynchroField()];
                               if($s->type_object!='civilite'){
                                   $is_not_only_civil=true;
                                   //sm("objets  ".$s->type_object);
                               }
                           }
                        }
                       
                        if($is_not_only_civil==false){
                            unset($cat[$i][$section_index]);
                        }
                     }
                    //traitement du bloc des franchises dans CS
                    //si on est dans l'indice 0 donc dans la déclaration et qu'il y a 3 éléments
                    if($i==0 && count($sections)==3){
                        
                        if(!empty($sections[2])){
                            $val1=@$db_data_conf[$i][$sections[2]->getSynchroField()];
                        }else{
                            $val1=false;
                        }
                        
                        if(!empty($sections[3])){
                            $val2=@$db_data_conf[$i][$sections[2]->getSynchroField()];
                        }else{
                            $val2=false;
                        }
                        //si les 2 valeurs suivantes sont vide, c'est que c'est pas remplis. On supprime le bloc
                        if(empty($val1) && empty($val2))
                        {
                            unset($cat[$i][$section_index]);
                        }
                    }
                    
                   
                    //traitement d'un bloc d'info classic de déclaration.
                    //si on est dans un indice supérieur a 0 alors on est dans une déclaration et non des coordonnées
                    if($i==1 && count($sections)>2){
                        
                        //on boucle sur toutes les valeurs du bloc, et si elle sont vide, on les supprime.
                        //on ne supprime que les champs text et int
                        foreach($sections as $item_index=>$item)
                        {
                            if( !empty($item->getSynchroField()) ){
                                //si l'item a un préfix, on le récupere.
                                $item_conf=$cat[$i][$section_index][$item_index]->getTypeObjetConf();
                                if(!empty($cat[$i][$section_index][$item_index+1]) ){
                                    $item_conf_next=$cat[$i][$section_index][$item_index+1]->getTypeObjetConf();
                                }
                                if(!empty($item_conf['display_column_prefix'])){
                                    $item_conf['display_column_prefix']=str_replace('&nbsp;','', trim($item_conf['display_column_prefix']));
                                }
                                if(!empty($item_conf_next['display_column_prefix'])){
                                    $item_conf_next['display_column_prefix']=str_replace('&nbsp;','', trim($item_conf_next['display_column_prefix']));
                                }
                                if(!empty($item_conf['display_column_prefix'])){
                                    $prefix_label=$item_conf['display_column_prefix'];
                                }
                                
                                
                                if(
                                    in_array($item->getTypeObjet(),['text_max_25', "text_max_50", 'text_max_40', 'text_max_80', 'text_max_100', 'num_2', "num_3", 'num_4', 'num_5','num_6','num_7', 'zipcode', 'title_num_5','title_num_6','title_num_7','subtitle_num_5','subtitle_num_7']) 
                                    && 
                                    empty($db_data_conf[$i][$item->getSynchroField()]) 
                                    ){
                                        //si on a un préfix, et que le suivant a une zone de préfix vide, on lui affecte le label avant de supprimer le premier.
                                        if(!empty($item_conf['display_column_prefix']) && !empty($item_conf_next['display_column_prefix_size']) && empty($item_conf_next['display_column_prefix'])){
                                            $item_conf_next['display_column_prefix']=$prefix_label;
                                            $cat[$i][$section_index][$item_index+1]->setTypeObjetConf($item_conf_next);
                                        }
                                        unset($cat[$i][$section_index][$item_index]);
                                        
                                      
                                }
                            }
                        }
                        
                        
                        //Comme on a viré des index, c'est un vrai gruyere. Donc on réindex proprement.
                        $cat[$i][$section_index]=array_values($cat[$i][$section_index]);

                        
                        // Si on a 2 éléments que le premier est un titre, et que le second est une ligne séparatrice, on supprime le bloc aussi
                        if(!empty($cat[$i][$section_index]) && count($cat[$i][$section_index])==2 && $cat[$i][$section_index][0]->getTypeObjet()=='title' && !empty($cat[$i][$section_index][1]) && $cat[$i][$section_index][1]->getTypeObjet()=='line_separator'){
                            unset($cat[$i][$section_index]);
                        }
                        
                        
                        
                        //si on a 2 éléments, que le premier est un titre et que le second est un sous-titre
                        if(!empty($cat[$i][$section_index]) && count($cat[$i][$section_index])==2 && $cat[$i][$section_index][0]->getTypeObjet()=='title' && $cat[$i][$section_index][1]->getTypeObjet()=='subtitle'){
                            unset($cat[$i][$section_index]);
                        }
                        
                        // Si on a juste un titre, on supprime toute la section.
                        if(!empty($cat[$i][$section_index]) && count($cat[$i][$section_index])==1 && ( $cat[$i][$section_index][0]->getTypeObjet()=='title' || $cat[$i][$section_index][0]->getTypeObjet()=='line_separator'  )){
                            unset($cat[$i][$section_index]);
                        }
                    }
                    
                }
                
               
                
                
            }
           
            //on remet la configuration dans l'ordre
            $sanitize=[];
            foreach($cat as $i=>$declar){
                foreach($declar as $sections)
                {
                    foreach($sections as $s){
                        $sanitize[$i][]=$s;
                    }
                    
                }
            }
            
            return $sanitize;

    }
    
    
    
    public function GenerateExcellAboPresseLivre($wdeclar)
    {
        $projectRoot = $this->kernel->getProjectDir();
        if (!file_exists($projectRoot."/var/tmp_upload/")) {
            mkdir($projectRoot."/var/tmp_upload/", 0777, true);
        }
        $temp_file= $projectRoot."/var/tmp_upload/tmp_excel_".time().".xls";
    
        $WcoconRepository=$this->registry->getRepository(Wcocon::class);
        $WdeclarRepository=$this->registry->getRepository(Wdeclar::class);
        $WtypeRepository=$this->registry->getRepository(Wtype::class);
    
        $primary_declaration = $WdeclarRepository->findOneBy(["wde_declar" => $wdeclar]);
        $wcocon = $WcoconRepository->findOneBy(["wco_dossier" => $primary_declaration->getWdeDossier()]);
        $contrat=$primary_declaration->getWdeTypcont();
        $declar_id=$primary_declaration->getWdeContrat();
        $wtype_id=$WtypeRepository->findOneBy(["label" => $contrat])->getId();
        $declaration_conf = $WcoconRepository->getDeclarationConfiguration($wcocon, $wtype_id);
        $contrats = $this->getListContrats($declaration_conf);
    
        $value=$this->getValue('abo_press_livres', $declaration_conf[1][0]->wde_dossier, $declaration_conf[1][0]->wde_declar);
        
        if(empty($value['wabo_fichier'])){
            
            $liste_abonnements=[];
            foreach($value['wabo_publications'] as $i=>$abo){
                $liste_abonnements[$abo['wabopType']][$i]["titre"]=$abo['wabopTitre'];
                $liste_abonnements[$abo['wabopType']][$i]["nombre"]=$abo['wabopNombre'];
                $liste_abonnements[$abo['wabopType']][$i]["auteur"]=$abo['wabopAuteur'];
                $liste_abonnements[$abo['wabopType']][$i]["editeur"]=$abo['wabopEditeur'];
                if($abo['wabopType']=='livre'){
                    $liste_abonnements_livre[]=$abo;
                }
            }
            
            
            $phpExcel = new Spreadsheet();
            $phpExcel->getProperties()->setCreator('CFC - extranet')
            ->setTitle('Declaration : '.$primary_declaration->getWdeContrat())
            ->setSubject('Declaration : '.$primary_declaration->getWdeContrat())
            ->setDescription('Declaration : '.$primary_declaration->getWdeContrat());
            $invalidCharacters = array('*', ':', '/', '\\', '?', '[', ']');
            $sheet=$phpExcel->setActiveSheetIndex(0);
            $sheet->setTitle('Titres de presse');
            
            // sm($liste_abonnements);
            $l=1;
            if(!empty($liste_abonnements['titre'])){
                $sheet->setCellValue(Coordinate::stringFromColumnIndex(1).$l,'Titres de presse, livres,  site de presse');
                $sheet->setCellValue(Coordinate::stringFromColumnIndex(2).$l,'Nombre d\'abonnements');
                $sheet->setCellValue(Coordinate::stringFromColumnIndex(3).$l,'Auteur');
                $sheet->setCellValue(Coordinate::stringFromColumnIndex(4).$l,'Editeur');
                // sm( $l);
                foreach($liste_abonnements['titre'] as $k=>$v){
                    $l++;
                    // sm('$liste_abonnements["titre"] :'. $l);
                    $sheet->setCellValue(Coordinate::stringFromColumnIndex(1).$l, $v['titre']);
                    $sheet->getColumnDimension(Coordinate::stringFromColumnIndex(1))->setAutoSize(true);
                    $sheet->setCellValue(Coordinate::stringFromColumnIndex(2).$l, $v['nombre']);
                    $sheet->getColumnDimension(Coordinate::stringFromColumnIndex(2))->setAutoSize(true);
                }
            }
            
             
            if(!empty($liste_abonnements['site'])){
                //$sheet = $phpExcel->createSheet();
                //$sheet->setTitle('Sites de presse');
                //$l++;
                //$l++;
                // sm( $l);
               // $sheet->setCellValue(Coordinate::stringFromColumnIndex(1).$l,'Sites de presse');
               // $sheet->setCellValue(Coordinate::stringFromColumnIndex(2).$l,'Nombre d\'abonnements');
            
                foreach($liste_abonnements['site'] as $k=>$v){
                    $l++;
                    // sm('$liste_abonnements["site"]'.  $l);
                     
                    $sheet->setCellValue(Coordinate::stringFromColumnIndex(1).$l, $v['titre']);
                    $sheet->getColumnDimension(Coordinate::stringFromColumnIndex(1))->setAutoSize(true);
                    $sheet->setCellValue(Coordinate::stringFromColumnIndex(2).$l, $v['nombre']);
                    $sheet->getColumnDimension(Coordinate::stringFromColumnIndex(2))->setAutoSize(true);
                }
            }
            if(!empty($liste_abonnements_livre)){
            
                //$l++;
               // $l++;
                //sm(  $l);
                //$sheet = $phpExcel->createSheet();
                //$sheet->setTitle('Abonnements Livres');
               /* $sheet->setCellValue(Coordinate::stringFromColumnIndex(1).$l,'Titre livre');
                $sheet->setCellValue(Coordinate::stringFromColumnIndex(2).$l,'Nombre d\'achats');
                $sheet->setCellValue(Coordinate::stringFromColumnIndex(3).$l,'Auteur');
                $sheet->setCellValue(Coordinate::stringFromColumnIndex(4).$l,'Editeur');*/
            
                foreach($liste_abonnements_livre as $k=>$v){
                    $l++;
            
                    //sm('$liste_abonnements_livre'.  $l);
                    $sheet->setCellValue(Coordinate::stringFromColumnIndex(1).$l, $v['wabopTitre']);
                    $sheet->setCellValue(Coordinate::stringFromColumnIndex(2).$l, $v['wabopNombre']);
                    $sheet->setCellValue(Coordinate::stringFromColumnIndex(3).$l, $v['wabopAuteur']);
                    $sheet->setCellValue(Coordinate::stringFromColumnIndex(4).$l, $v['wabopEditeur']);
                    $sheet->getColumnDimension(Coordinate::stringFromColumnIndex(1))->setAutoSize(true);
                    $sheet->getColumnDimension(Coordinate::stringFromColumnIndex(2))->setAutoSize(true);
                    $sheet->getColumnDimension(Coordinate::stringFromColumnIndex(3))->setAutoSize(true);
                    $sheet->getColumnDimension(Coordinate::stringFromColumnIndex(4))->setAutoSize(true);
                }
            }
        }else{
            //inclusion du fichier uploadé
            /*$mediaManager = $this->container->get('sonata.media.manager.media');
            $media=$mediaManager->find($value['wabo_fichier']);
            $file_name=$media->getName();
            $file_reference='./upload/media/'.$this->provider->getReferenceFile($media,'reference')->getName();
            
            
            $MergeReader = IOFactory::createReader('Xls');
            $MergeExcel = $MergeReader->load($file_reference);
            $sheetToMerge=$MergeExcel->setActiveSheetIndex(1);
            $sheetToMerge->setTitle('Cipro-presse&livres');
            $phpExcel->addExternalSheet($sheetToMerge);
            $sheet_index++;*/
            
            $phpExcel = new Spreadsheet();
            $phpExcel->getProperties()->setCreator('CFC - extranet')
            ->setTitle('Declaration : '.$primary_declaration->getWdeContrat())
            ->setSubject('Declaration : '.$primary_declaration->getWdeContrat())
            ->setDescription('Declaration : '.$primary_declaration->getWdeContrat());
            $invalidCharacters = array('*', ':', '/', '\\', '?', '[', ']');
            $sheet=$phpExcel->setActiveSheetIndex(0);
            $sheet->setTitle('Cipro-presse&livres');
            
            $mediaManager = $this->container->get('sonata.media.manager.media');
            $media=$mediaManager->find($value['wabo_fichier']);
            $file_name=$media->getName();
            $file_reference=$this->provider->getReferenceFile($media,'reference')->getName();
            $sheet->setCellValue(Coordinate::stringFromColumnIndex(1).'1','Fichier Cipro-presse&livres : ');
            $sheet->setCellValue(Coordinate::stringFromColumnIndex(2).'1', $file_name);
            $sheet->getCell(Coordinate::stringFromColumnIndex(2).'1')->getHyperlink()->setUrl('http://'.$_SERVER['HTTP_HOST'].'/upload/media/'.$file_reference);
            $sheet->getColumnDimension(Coordinate::stringFromColumnIndex(1))->setAutoSize(true);
            $sheet->getColumnDimension(Coordinate::stringFromColumnIndex(2))->setAutoSize(true);
        }
         

        
      
        
    
        
        
        
        $objWriter = IOFactory::createWriter($phpExcel, 'Xls');
        $objWriter->save($temp_file);
    
        return ['name'=>'Titres_presse_'.$contrat.'_'.$declaration_conf[1][0]->wde_declar.'.xls', 'path'=>$temp_file];
    
    
    
    }
    
    
    public function GenerateExcellCopieExtReseau($wdeclar)
    {
    
        $projectRoot = $this->kernel->getProjectDir();
        if (!file_exists($projectRoot."/var/tmp_upload/")) {
            mkdir($projectRoot."/var/tmp_upload/", 0777, true);
        }
    
        $temp_file= $projectRoot."/var/tmp_upload/tmp_excel_".time().".xls";
    
        $WcoconRepository=$this->registry->getRepository(Wcocon::class);
        $WdeclarRepository=$this->registry->getRepository(Wdeclar::class);
        $WtypeRepository=$this->registry->getRepository(Wtype::class);
    
        $primary_declaration = $WdeclarRepository->findOneBy(["wde_declar" => $wdeclar]);
        $wcocon = $WcoconRepository->findOneBy(["wco_dossier" => $primary_declaration->getWdeDossier()]);
        $contrat=$primary_declaration->getWdeTypcont();
        $declar_id=$primary_declaration->getWdeContrat();
        $wtype_id=$WtypeRepository->findOneBy(["label" => $contrat])->getId();
        $declaration_conf = $WcoconRepository->getDeclarationConfiguration($wcocon, $wtype_id);
        $contrats = $this->getListContrats($declaration_conf);
    
    
        //wcpextw
        $value=$this->getValue('wcpextw', $declaration_conf[1][0]->wde_dossier, $declaration_conf[1][0]->wde_declar);
         

    
        $phpExcel = new Spreadsheet();
        $phpExcel->getProperties()->setCreator('CFC - extranet')
        ->setTitle('Declaration : '.$primary_declaration->getWdeContrat())
        ->setSubject('Declaration : '.$primary_declaration->getWdeContrat())
        ->setDescription('Declaration : '.$primary_declaration->getWdeContrat());
        $invalidCharacters = array('*', ':', '/', '\\', '?', '[', ']');
        $sheet=$phpExcel->setActiveSheetIndex(0);
        $sheet->setTitle(Urlizer::urlize($contrat));
    
    
        foreach($value as $k=>$v){
            $line=$k+2;
            $sheet->setCellValue(Coordinate::stringFromColumnIndex(1).'1','TITRE DE LA PUBLICATION DONT EST ISSU L\'ARTICLE');
            $sheet->setCellValue(Coordinate::stringFromColumnIndex(1).$line, $v['wcpextPublication']);
            $sheet->getColumnDimension(Coordinate::stringFromColumnIndex(1))->setAutoSize(true);
             
    
            $sheet->setCellValue(Coordinate::stringFromColumnIndex(2).'1','TITRE DE L\'ARTICLE');
            $sheet->setCellValue(Coordinate::stringFromColumnIndex(2).$line, $v['wcpextTitre']);
            $sheet->getColumnDimension(Coordinate::stringFromColumnIndex(2))->setAutoSize(true);
    
            $sheet->setCellValue(Coordinate::stringFromColumnIndex(3).'1','AUTEUR(S) DE L\'ARTICLE');
            $sheet->setCellValue(Coordinate::stringFromColumnIndex(3).$line, $v['wcpextAuteur']);
            $sheet->getColumnDimension(Coordinate::stringFromColumnIndex(3))->setAutoSize(true);
    
            $sheet->setCellValue(Coordinate::stringFromColumnIndex(4).'1','DATE DE PARUTION DE L\'ARTICLE');
            $sheet->setCellValue(Coordinate::stringFromColumnIndex(4).$line, $v['wcpextParution']);
            $sheet->getColumnDimension(Coordinate::stringFromColumnIndex(4))->setAutoSize(true);
            
            $sheet->setCellValue(Coordinate::stringFromColumnIndex(4).'1','DATE DE MISE EN LIGNE DE LA COPIE DE L\'ARTICLE');
            $sheet->setCellValue(Coordinate::stringFromColumnIndex(4).$line, $v['wcpextDateMel']);
            $sheet->getColumnDimension(Coordinate::stringFromColumnIndex(4))->setAutoSize(true);
            
            
            $value_parution="";
            foreach($v['wcpextMel'] as $parutions)
            {
                $value_parution.='• '.$parutions['wcpextmelMel']."\r\n";
            }
            
            $sheet->setCellValue(Coordinate::stringFromColumnIndex(5).'1','SITES, PLATEFORMES, ET/OU RÉSEAUX SOCIAUX SUR LESQUELS L\'ARTICLE EST MIS EN LIGNE');
            $sheet->setCellValue(Coordinate::stringFromColumnIndex(5).$line, $value_parution);
            $sheet->getColumnDimension(Coordinate::stringFromColumnIndex(5))->setAutoSize(true);

    
        }
    
    
        $objWriter = IOFactory::createWriter($phpExcel, 'Xls');
        $objWriter->save($temp_file);
    
        return ['name'=>'copie_ext_reseau_'.$contrat.'_'.$declaration_conf[1][0]->wde_declar.'.xls', 'path'=>$temp_file];
    
    
    
    }
    
    public function GenerateExcellCopieExtCible($wdeclar)
    {

        $projectRoot = $this->kernel->getProjectDir();
        if (!file_exists($projectRoot."/var/tmp_upload/")) {
            mkdir($projectRoot."/var/tmp_upload/", 0777, true);
        }
    
        $temp_file= $projectRoot."/var/tmp_upload/tmp_excel_".time().".xls";
    
        $WcoconRepository=$this->registry->getRepository(Wcocon::class);
        $WdeclarRepository=$this->registry->getRepository(Wdeclar::class);
        $WtypeRepository=$this->registry->getRepository(Wtype::class);
    
        $primary_declaration = $WdeclarRepository->findOneBy(["wde_declar" => $wdeclar]);
        $wcocon = $WcoconRepository->findOneBy(["wco_dossier" => $primary_declaration->getWdeDossier()]);
        $contrat=$primary_declaration->getWdeTypcont();
        $declar_id=$primary_declaration->getWdeContrat();
        $wtype_id=$WtypeRepository->findOneBy(["label" => $contrat])->getId();
        $declaration_conf = $WcoconRepository->getDeclarationConfiguration($wcocon, $wtype_id);
        $contrats = $this->getListContrats($declaration_conf);
    

        //wcpextw
        $value=$this->getValue('wcpextc', $declaration_conf[1][0]->wde_dossier, $declaration_conf[1][0]->wde_declar);
         

    
        $phpExcel = new Spreadsheet();
        $phpExcel->getProperties()->setCreator('CFC - extranet')
        ->setTitle('Declaration : '.$primary_declaration->getWdeContrat())
        ->setSubject('Declaration : '.$primary_declaration->getWdeContrat())
        ->setDescription('Declaration : '.$primary_declaration->getWdeContrat());
        $invalidCharacters = array('*', ':', '/', '\\', '?', '[', ']');
        $sheet=$phpExcel->setActiveSheetIndex(0);
        $sheet->setTitle(Urlizer::urlize($contrat));
    
    
        foreach($value as $k=>$v){
            $line=$k+2;
            $sheet->setCellValue(Coordinate::stringFromColumnIndex(1).'1','TITRE DE LA PUBLICATION DONT EST ISSU L\'ARTICLE');
            $sheet->setCellValue(Coordinate::stringFromColumnIndex(1).$line, $v['wcpextPublication']);
            $sheet->getColumnDimension(Coordinate::stringFromColumnIndex(1))->setAutoSize(true);
   
    
            $sheet->setCellValue(Coordinate::stringFromColumnIndex(2).'1','TITRE DE L\'ARTICLE');
            $sheet->setCellValue(Coordinate::stringFromColumnIndex(2).$line, $v['wcpextTitre']);
            $sheet->getColumnDimension(Coordinate::stringFromColumnIndex(2))->setAutoSize(true);
    
            $sheet->setCellValue(Coordinate::stringFromColumnIndex(3).'1','AUTEUR(S) DE L\'ARTICLE');
            $sheet->setCellValue(Coordinate::stringFromColumnIndex(3).$line, $v['wcpextAuteur']);
            $sheet->getColumnDimension(Coordinate::stringFromColumnIndex(3))->setAutoSize(true);
            
            $sheet->setCellValue(Coordinate::stringFromColumnIndex(4).'1','DATE DE PARUTION DE L\'ARTICLE');
            $sheet->setCellValue(Coordinate::stringFromColumnIndex(4).$line, $v['wcpextParution']);
            $sheet->getColumnDimension(Coordinate::stringFromColumnIndex(4))->setAutoSize(true);
            
            $sheet->setCellValue(Coordinate::stringFromColumnIndex(5).'1','NOMBRE DE DESTINATAIRE DE VEILLE MEDIA');
            $sheet->setCellValue(Coordinate::stringFromColumnIndex(5).$line, $v['wcpextDestinataires']);
            $sheet->getColumnDimension(Coordinate::stringFromColumnIndex(5))->setAutoSize(true);

        }
    
    
        $objWriter = IOFactory::createWriter($phpExcel, 'Xls');
        $objWriter->save($temp_file);
    
        return ['name'=>'copie_ext_cible_'.$contrat.'_'.$declaration_conf[1][0]->wde_declar.'.xls', 'path'=>$temp_file];
    
    
    
    }
    
    
    
    public function GenerateExcellAboPresse($wdeclar)
    {
        $projectRoot = $this->kernel->getProjectDir();
        if (!file_exists($projectRoot."/var/tmp_upload/")) {
            mkdir($projectRoot."/var/tmp_upload/", 0777, true);
        }
        

        $temp_file= $projectRoot."/var/tmp_upload/tmp_excel_".time().".xls";
    
        $WcoconRepository=$this->registry->getRepository(Wcocon::class);
        $WdeclarRepository=$this->registry->getRepository(Wdeclar::class);
        $WtypeRepository=$this->registry->getRepository(Wtype::class);
    
        $primary_declaration = $WdeclarRepository->findOneBy(["wde_declar" => $wdeclar]);
        $wcocon = $WcoconRepository->findOneBy(["wco_dossier" => $primary_declaration->getWdeDossier()]);
        $contrat=$primary_declaration->getWdeTypcont();
        $declar_id=$primary_declaration->getWdeContrat();
        $wtype_id=$WtypeRepository->findOneBy(["label" => $contrat])->getId();
        $declaration_conf = $WcoconRepository->getDeclarationConfiguration($wcocon, $wtype_id);
        $contrats = $this->getListContrats($declaration_conf);
    
        $value=$this->getValue('abo_press_livres', $declaration_conf[1][0]->wde_dossier, $declaration_conf[1][0]->wde_declar);
         
        $liste_abonnements=[];
        foreach($value['wabo_publications'] as $abo){
            $liste_abonnements[$abo['wabopType']][]=['titre'=>$abo['wabopTitre'],'nombre'=>$abo['wabopNombre']];
        }
         
    
        $phpExcel = new Spreadsheet();
        $phpExcel->getProperties()->setCreator('CFC - extranet')
        ->setTitle('Declaration : '.$primary_declaration->getWdeContrat())
        ->setSubject('Declaration : '.$primary_declaration->getWdeContrat())
        ->setDescription('Declaration : '.$primary_declaration->getWdeContrat());
        $invalidCharacters = array('*', ':', '/', '\\', '?', '[', ']');
        $sheet=$phpExcel->setActiveSheetIndex(0);
        $sheet->setTitle('Titres et sites de presse');
    
        $sheet->setCellValue(Coordinate::stringFromColumnIndex(1).'1','Titres & Sites de presse');
        $sheet->setCellValue(Coordinate::stringFromColumnIndex(2).'1','Nombre d\'abonnements');
        
            
        $k=0;
        if(!empty($liste_abonnements['titre'])){
            foreach($liste_abonnements['titre'] as $k=>$v){
                $line=$k+2;
                $sheet->setCellValue(Coordinate::stringFromColumnIndex(1).$line, $v['titre']);
                $sheet->getColumnDimension(Coordinate::stringFromColumnIndex(1))->setAutoSize(true);
                $sheet->setCellValue(Coordinate::stringFromColumnIndex(2).$line, $v['nombre']);
                $sheet->getColumnDimension(Coordinate::stringFromColumnIndex(2))->setAutoSize(true);
            }
        }

    
        //$sheet = $phpExcel->createSheet();
        //$sheet->setTitle('Sites de presse');
        //$line = $line + 2;
        //$sheet->setCellValue(Coordinate::stringFromColumnIndex(1). $line,'Sites de presse');
        $j=0;
        if(!empty($liste_abonnements['site'])){
            foreach($liste_abonnements['site'] as $j=>$v){
                $line=$line+$j+1;
                $sheet->setCellValue(Coordinate::stringFromColumnIndex(1).$line, $v['titre']);
                $sheet->getColumnDimension(Coordinate::stringFromColumnIndex(1))->setAutoSize(true);
                $sheet->setCellValue(Coordinate::stringFromColumnIndex(2).$line, $v['nombre']);
                $sheet->getColumnDimension(Coordinate::stringFromColumnIndex(2))->setAutoSize(true);
            }
        }

    
        $objWriter = IOFactory::createWriter($phpExcel, 'Xls');
        $objWriter->save($temp_file);
    
        return ['name'=>'Titres_presse_'.$contrat.'_'.$declaration_conf[1][0]->wde_declar.'.xls', 'path'=>$temp_file];
    
    
    
    }
    
    
    public function GenerateExcellComposantes($wdeclar)
    {
        $projectRoot = $this->kernel->getProjectDir();
        if (!file_exists($projectRoot."/var/tmp_upload/")) {
            mkdir($projectRoot."/var/tmp_upload/", 0777, true);
        }
        
        $temp_file= $projectRoot."/var/tmp_upload/tmp_excel_".time().".xls";
        
        $WcoconRepository=$this->registry->getRepository(Wcocon::class);
        $WdeclarRepository=$this->registry->getRepository(Wdeclar::class);
        $WtypeRepository=$this->registry->getRepository(Wtype::class);
        
        $primary_declaration = $WdeclarRepository->findOneBy(["wde_declar" => $wdeclar]);
        $wcocon = $WcoconRepository->findOneBy(["wco_dossier" => $primary_declaration->getWdeDossier()]);
        $contrat=$primary_declaration->getWdeTypcont();
        $declar_id=$primary_declaration->getWdeContrat();
        $wtype_id=$WtypeRepository->findOneBy(["label" => $contrat])->getId();
        $declaration_conf = $WcoconRepository->getDeclarationConfiguration($wcocon, $wtype_id);
        $contrats = $this->getListContrats($declaration_conf);
        
        $value=$this->getValue('wcompos', $declaration_conf[1][0]->wde_dossier, $declaration_conf[1][0]->wde_declar);
        $phpExcel = new Spreadsheet();
        $phpExcel->getProperties()->setCreator('CFC - extranet')
        ->setTitle('Declaration : '.$primary_declaration->getWdeContrat())
        ->setSubject('Declaration : '.$primary_declaration->getWdeContrat())
        ->setDescription('Declaration : '.$primary_declaration->getWdeContrat());
        $invalidCharacters = array('*', ':', '/', '\\', '?', '[', ']');
        $sheet=$phpExcel->setActiveSheetIndex(0);
        
        $sheet->setTitle("Composantes");
        
        if(!empty($value)){
            foreach($value as $k=>$v){
                $line=$k+2;
                $sheet->setCellValue(Coordinate::stringFromColumnIndex(1).'1','Libelle');
                $sheet->setCellValue(Coordinate::stringFromColumnIndex(1).$line, $v['wcp_libelle']);
                $sheet->getColumnDimension(Coordinate::stringFromColumnIndex(1))->setAutoSize(true);
            
                $sheet->setCellValue(Coordinate::stringFromColumnIndex(2).'1','Effectif');
                $sheet->setCellValue(Coordinate::stringFromColumnIndex(2).$line, $v['wcp_effectif']);
                $sheet->getColumnDimension(Coordinate::stringFromColumnIndex(2))->setAutoSize(true);
            
            
            } 
        }


        
        $objWriter = IOFactory::createWriter($phpExcel, 'Xls');
        $objWriter->save($temp_file);
 
        return ['name'=>'Composantes_'.$contrat.'_'.$declaration_conf[1][0]->wde_declar.'.xls', 'path'=>$temp_file];
    }
    
    public function GenerateExcellComposantesStages($wdeclar)
    {
        $projectRoot = $this->kernel->getProjectDir();
        if (!file_exists($projectRoot."/var/tmp_upload/")) {
            mkdir($projectRoot."/var/tmp_upload/", 0777, true);
        }
        
        $temp_file= $projectRoot."/var/tmp_upload/tmp_excel_".time().".xls";
    
        $WcoconRepository=$this->registry->getRepository(Wcocon::class);
        $WdeclarRepository=$this->registry->getRepository(Wdeclar::class);
        $WtypeRepository=$this->registry->getRepository(Wtype::class);
    
        $primary_declaration = $WdeclarRepository->findOneBy(["wde_declar" => $wdeclar]);
        $wcocon = $WcoconRepository->findOneBy(["wco_dossier" => $primary_declaration->getWdeDossier()]);
        $contrat=$primary_declaration->getWdeTypcont();
        $declar_id=$primary_declaration->getWdeContrat();
        $wtype_id=$WtypeRepository->findOneBy(["label" => $contrat])->getId();
        $declaration_conf = $WcoconRepository->getDeclarationConfiguration($wcocon, $wtype_id);
        $contrats = $this->getListContrats($declaration_conf);
    
        $value=$this->getValue('wcompos', $declaration_conf[1][0]->wde_dossier, $declaration_conf[1][0]->wde_declar);
        $phpExcel = new Spreadsheet();
        $phpExcel->getProperties()->setCreator('CFC - extranet')
        ->setTitle('Declaration : '.$primary_declaration->getWdeContrat())
        ->setSubject('Declaration : '.$primary_declaration->getWdeContrat())
        ->setDescription('Declaration : '.$primary_declaration->getWdeContrat());
        $invalidCharacters = array('*', ':', '/', '\\', '?', '[', ']');
        $sheet=$phpExcel->setActiveSheetIndex(0);
    
        $sheet->setTitle("Composantes stages");
    
    
        foreach($value as $k=>$v){
            $line=$k+2;
            $sheet->setCellValue(Coordinate::stringFromColumnIndex(1).'1','Intitulé du stage');
            $sheet->setCellValue(Coordinate::stringFromColumnIndex(1).$line, $v['wcp_libelle']);
            $sheet->getColumnDimension(Coordinate::stringFromColumnIndex(1))->setAutoSize(true);
    
            $sheet->setCellValue(Coordinate::stringFromColumnIndex(2).'1','Nombre de stagiaires');
            $sheet->setCellValue(Coordinate::stringFromColumnIndex(2).$line, $v['wcp_effectif']);
            $sheet->getColumnDimension(Coordinate::stringFromColumnIndex(2))->setAutoSize(true);
    
            $sheet->setCellValue(Coordinate::stringFromColumnIndex(3).'1','Nombre d\'heures stagiaires');
            $sheet->setCellValue(Coordinate::stringFromColumnIndex(3).$line, $v['wcp_heures']);
            $sheet->getColumnDimension(Coordinate::stringFromColumnIndex(3))->setAutoSize(true);
    
        }
    
    
        $objWriter = IOFactory::createWriter($phpExcel, 'Xls');
        $objWriter->save($temp_file);
    
        return ['name'=>'Composantes_'.$contrat.'_'.$declaration_conf[1][0]->wde_declar.'.xls', 'path'=>$temp_file];
    }    
    
    

    public function GenerateExcellRelationPublique($wdeclar)
    {
        $projectRoot = $this->kernel->getProjectDir();
        if (!file_exists($projectRoot."/var/tmp_upload/")) {
            mkdir($projectRoot."/var/tmp_upload/", 0777, true);
        }
        
        $temp_file= $projectRoot."/var/tmp_upload/tmp_excel_".time().".xls";
        
        $WcoconRepository=$this->registry->getRepository(Wcocon::class);
        $WdeclarRepository=$this->registry->getRepository(Wdeclar::class);
        $WtypeRepository=$this->registry->getRepository(Wtype::class);
        
        $primary_declaration = $WdeclarRepository->findOneBy(["wde_declar" => $wdeclar]);
        $wcocon = $WcoconRepository->findOneBy(["wco_dossier" => $primary_declaration->getWdeDossier()]);
        $contrat=$primary_declaration->getWdeTypcont();
        $declar_id=$primary_declaration->getWdeContrat();
        $wtype_id=$WtypeRepository->findOneBy(["label" => $contrat])->getId();
        $declaration_conf = $WcoconRepository->getDeclarationConfiguration($wcocon, $wtype_id);
        $contrats = $this->getListContrats($declaration_conf);
        
        $value=$this->getValue('relation_publique', $declaration_conf[1][0]->wde_dossier, $declaration_conf[1][0]->wde_declar);
       
        

        $phpExcel = new Spreadsheet();
        $phpExcel->getProperties()->setCreator('CFC - extranet')
        ->setTitle('Declaration : '.$primary_declaration->getWdeContrat())
        ->setSubject('Declaration : '.$primary_declaration->getWdeContrat())
        ->setDescription('Declaration : '.$primary_declaration->getWdeContrat());
        $invalidCharacters = array('*', ':', '/', '\\', '?', '[', ']');
        $sheet=$phpExcel->setActiveSheetIndex(0);
        $sheet->setTitle(Urlizer::urlize($contrat));
        
        
        foreach($value as $k=>$v){
            $line=$k+2;
            $sheet->setCellValue(Coordinate::stringFromColumnIndex(1).'1','Client');
            $sheet->setCellValue(Coordinate::stringFromColumnIndex(1).$line, $v['wrp_label']);
            $sheet->getColumnDimension(Coordinate::stringFromColumnIndex(1))->setAutoSize(true);
        
            $sheet->setCellValue(Coordinate::stringFromColumnIndex(2).'1','Adresse');
            $sheet->setCellValue(Coordinate::stringFromColumnIndex(2).$line, $v['wrp_adresse']);
            $sheet->getColumnDimension(Coordinate::stringFromColumnIndex(2))->setAutoSize(true);
        
            $sheet->setCellValue(Coordinate::stringFromColumnIndex(3).'1','Code postal');
            $sheet->setCellValue(Coordinate::stringFromColumnIndex(3).$line, $v['wrp_cp']);
            $sheet->getColumnDimension(Coordinate::stringFromColumnIndex(3))->setAutoSize(true);
        
            $sheet->setCellValue(Coordinate::stringFromColumnIndex(4).'1','Ville');
            $sheet->setCellValue(Coordinate::stringFromColumnIndex(4).$line, $v['wrp_ville']);
            $sheet->getColumnDimension(Coordinate::stringFromColumnIndex(4))->setAutoSize(true);
        
            $sheet->setCellValue(Coordinate::stringFromColumnIndex(5).'1','Volume d\'article fournis');
            $sheet->setCellValue(Coordinate::stringFromColumnIndex(5).$line, $v['wrp_volume']);
            $sheet->getColumnDimension(Coordinate::stringFromColumnIndex(5))->setAutoSize(true);
        
        }

        
        $objWriter = IOFactory::createWriter($phpExcel, 'Xls');
        $objWriter->save($temp_file);
 
        return ['name'=>'Relation_presse_'.$contrat.'_'.$declaration_conf[1][0]->wde_declar.'.xls', 'path'=>$temp_file];
        
        
        
    }
    
    public function GenerateExcellCmaBtp($wdeclar)
    {
        $projectRoot = $this->kernel->getProjectDir();
        if (!file_exists($projectRoot."/var/tmp_upload/")) {
            mkdir($projectRoot."/var/tmp_upload/", 0777, true);
        }
        
        $temp_file= $projectRoot."/var/tmp_upload/tmp_excel_".time().".xls";
    
        $WcoconRepository=$this->registry->getRepository(Wcocon::class);
        $WdeclarRepository=$this->registry->getRepository(Wdeclar::class);
        $WtypeRepository=$this->registry->getRepository(Wtype::class);
    
        $primary_declaration = $WdeclarRepository->findOneBy(["wde_declar" => $wdeclar]);
        $wcocon = $WcoconRepository->findOneBy(["wco_dossier" => $primary_declaration->getWdeDossier()]);
        $contrat=$primary_declaration->getWdeTypcont();
        $declar_id=$primary_declaration->getWdeContrat();
        $wtype_id=$WtypeRepository->findOneBy(["label" => $contrat])->getId();
        $declaration_conf = $WcoconRepository->getDeclarationConfiguration($wcocon, $wtype_id);
        $contrats = $this->getListContrats($declaration_conf);
    
        $value=$this->getValue('relation_publique', $declaration_conf[1][0]->wde_dossier, $declaration_conf[1][0]->wde_declar);
         
    
    
        $phpExcel = new Spreadsheet();
        $phpExcel->getProperties()->setCreator('CFC - extranet')
        ->setTitle('Declaration : '.$primary_declaration->getWdeContrat())
        ->setSubject('Declaration : '.$primary_declaration->getWdeContrat())
        ->setDescription('Declaration : '.$primary_declaration->getWdeContrat());
        $invalidCharacters = array('*', ':', '/', '\\', '?', '[', ']');
        $sheet=$phpExcel->setActiveSheetIndex(0);
        $sheet->setTitle(Urlizer::urlize($contrat));
    
    
        foreach($value as $k=>$v){
            $line=$k+2;
            $sheet->setCellValue(Coordinate::stringFromColumnIndex(1).'1','Nom du site');
            $sheet->setCellValue(Coordinate::stringFromColumnIndex(1).$line, $v['wrp_label']);
            $sheet->getColumnDimension(Coordinate::stringFromColumnIndex(1))->setAutoSize(true);
    
            $sheet->setCellValue(Coordinate::stringFromColumnIndex(2).'1','Adresse');
            $sheet->setCellValue(Coordinate::stringFromColumnIndex(2).$line, $v['wrp_adresse']);
            $sheet->getColumnDimension(Coordinate::stringFromColumnIndex(2))->setAutoSize(true);
    
            $sheet->setCellValue(Coordinate::stringFromColumnIndex(3).'1','Téléphone');
            $sheet->setCellValue(Coordinate::stringFromColumnIndex(3).$line, $v['wrp_telephone']);
            $sheet->getColumnDimension(Coordinate::stringFromColumnIndex(3))->setAutoSize(true);
    
            $sheet->setCellValue(Coordinate::stringFromColumnIndex(4).'1','Email');
            $sheet->setCellValue(Coordinate::stringFromColumnIndex(4).$line, $v['wrp_email']);
            $sheet->getColumnDimension(Coordinate::stringFromColumnIndex(4))->setAutoSize(true);
    
        }
    
    
        $objWriter = IOFactory::createWriter($phpExcel, 'Xls');
        $objWriter->save($temp_file);
    
        return ['name'=>'Relation_presse_'.$contrat.'_'.$declaration_conf[1][0]->wde_declar.'.xls', 'path'=>$temp_file];
    
    
    
    }
    

    public function GenerateExcellPanoramaPresseNumerique($id, $index)
    {
        $projectRoot = $this->kernel->getProjectDir();
        if (!file_exists($projectRoot."/var/tmp_upload/")) {
            mkdir($projectRoot."/var/tmp_upload/", 0777, true);
        }
        $temp_file= $projectRoot."/var/tmp_upload/tmp_excel_".time().".xls";
        
        $objReader = IOFactory::createReader('Xls');
        
        $objPHPExcel = $objReader->load($projectRoot."/public/build/images/template_panorama_numerique.xls");
        
        $wppl = $this->registry->getRepository('App\Entity\WpanoramapressListe')->createQueryBuilder('wl');
        $wppl->where('wl.wpplId  = '.$id);
        $wppl_results = $wppl->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        if(!empty($wppl_results)){


            
            
            //Get the panorama info from the composante
            $wpp = $this->registry->getRepository('App\Entity\Wpanoramapress')->createQueryBuilder('w');
            $wpp->where('w.wppId  = '.$wppl_results[0]['wpplWpp']);
            $wpp_results = $wpp->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
            
            //get the declaration conf
            $wco = $this->registry->getRepository('App\Entity\Wcocon')->createQueryBuilder('wco');
            $wco->where('wco.wco_dossier  = '.$wpp_results[0]['wppDossier']);
            $wcocon = $wco->getQuery()->getResult();
            $declaration_conf = $this->getDeclarationConfiguration($wcocon[0]);
            $chorus=[];
            foreach($declaration_conf as $i=>$conf){
                foreach($conf as $j=>$c){
                    if($c->getTypeObjet()=="chorus_pro"){
                        $db_data_conf[$i]["wde_is_chorus_pro"]=$this->getValue("wde_is_chorus_pro", $c->wde_dossier, $c->wde_declar);
                        $db_data_conf[$i]["wde_siret"]=$this->getValue("wde_siret", $c->wde_dossier, $c->wde_declar);
                        $db_data_conf[$i]["wde_service_code"]=$this->getValue("wde_service_code", $c->wde_dossier, $c->wde_declar);
                        $db_data_conf[$i]["wde_is_order_number"]=$this->getValue("wde_is_order_number", $c->wde_dossier, $c->wde_declar);
                        $db_data_conf[$i]["wde_order_number"]=$this->getValue("wde_order_number", $c->wde_dossier, $c->wde_declar);
                        $chorus["wde_is_chorus_pro"]=$db_data_conf[$i]["wde_is_chorus_pro"];
                        $chorus["wde_siret"]=$db_data_conf[$i]["wde_siret"];
                        $chorus["wde_service_code"]=$db_data_conf[$i]["wde_service_code"];
                        $chorus["wde_is_order_number"]=$db_data_conf[$i]["wde_is_order_number"];
                        $chorus["wde_order_number"]=$db_data_conf[$i]["wde_order_number"];

                    }elseif($c->getTypeObjet()=="panorama_press_numerique"){
                        $db_data_conf[$i]["panorama_press_numerique"]=$this->getValue("panorama_press", $c->wde_dossier, $c->wde_declar);
                    }elseif($c->getTypeObjet()=="panorama_press_papier"){
                        $db_data_conf[$i]["panorama_press_numerique"]=$this->getValue("panorama_press", $c->wde_dossier, $c->wde_declar);
                    }else{
                        if (!empty($c->getSynchroField())) {
                            $db_data_conf[$i][$c->getSynchroField()]=$this->getValue($c->getSynchroField(), $c->wde_dossier, $c->wde_declar);
                        }
                    }
                }
            }

            $sheet = $objPHPExcel->getActiveSheet();
            $sheet->setCellValue('B1', $db_data_conf[0]['wco_raisoc1'].' - '. $db_data_conf[0]['wco_raisoc2'].' - '.$db_data_conf[0]['wco_raisoc3']);
            /*$sheet->setCellValue('C8', $db_data_conf[0]['wco_raisoc2']);
            $sheet->setCellValue('D8', $db_data_conf[0]['wco_raisoc3']);
            
            $sheet->setCellValue('B9', $db_data_conf[0]['wco_adresse1'].' '.$db_data_conf[0]['wco_adresse2'].' '.$db_data_conf[0]['wco_adresse3']);
            $sheet->setCellValue('B10', $db_data_conf[0]['wco_copos'].' '.$db_data_conf[0]['wco_ville']);

            $sheet->setCellValue('B14', $db_data_conf[0]['wde_nom_resp']);
            $sheet->setCellValue('B15', $db_data_conf[0]['wde_fct_resp']);
            $sheet->setCellValue('B16', $db_data_conf[0]['wde_mel_resp']);
            $objPHPExcel->getActiveSheet()->setCellValue('B17', $db_data_conf[0]['wde_tel_resp']);*/

            /*$head_style_blue=array(
                    'fill' => array(
                        'type' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                        'color' => array('rgb' => '66FFCC')
                    )
                );
            $head_style_yellow=array(
                'fill' => array(
                    'type' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                     'color' => array('rgb' => 'FFFFFF00')
                )
            );
            $sheet->getStyle('B8')->applyFromArray($head_style_blue);
            $sheet->getStyle('C8')->applyFromArray($head_style_blue);
            $sheet->getStyle('D8')->applyFromArray($head_style_blue);
            
            $sheet->getStyle('B14')->applyFromArray($head_style_blue);
            $sheet->getStyle('C14')->applyFromArray($head_style_blue);
            $sheet->getStyle('D14')->applyFromArray($head_style_blue);
            
            $sheet->getStyle('B31')->applyFromArray($head_style_yellow);
            $sheet->getStyle('C31')->applyFromArray($head_style_yellow);
            $sheet->getStyle('D31')->applyFromArray($head_style_yellow);
            
            $sheet->getStyle('B32')->applyFromArray($head_style_yellow);
            $sheet->getStyle('C32')->applyFromArray($head_style_yellow);
            $sheet->getStyle('D32')->applyFromArray($head_style_yellow);*/
            

            //$row->setBackground('FFFF00');
            //$sheet->getStyle('D40')->setBackground('#FFFF00');
            
            
            
            $contrat=$wpp_results[0]['wppContrat'];
            $declaration=$wpp_results[0]['wppDeclar'];
            
            $wpresta=$this->getValue('wprestataire', $c->wde_dossier, $c->wde_declar); 
            
            $prestataire=$wpresta['wpresta_prestataire'];
            $prestataire_label=$wpresta['wpresta_prestataire_label'];
            $prestataire_label2=$wpresta['wpresta_prestataire_label2'];
            
            $objPHPExcel->getActiveSheet()->setCellValue('B2', $contrat);
            /*$objPHPExcel->getActiveSheet()->setCellValue('B21', ($prestataire?"oui":"non"));
            $objPHPExcel->getActiveSheet()->setCellValue('C21', $prestataire_label.(!empty($prestataire_label2)?", ".$prestataire_label2:null));*/
            

            $label=$wppl_results[0]["wpplLabel"];
            if(empty($label)){
                $label="Panorama de presse ".($index);
            }
            
            $periode=$wppl_results[0]["wpplPeriode"];
            $total=$wppl_results[0]["wpplTotal"];
            
            $is_chorus_pro=$wppl_results[0]["wpplIsChorusPro"];
            $siret=$wppl_results[0]["wpplSiret"];
            $service_code=$wppl_results[0]["wpplServiceCode"];
            $has_order_number=$wppl_results[0]["wpplHasOrder"];
            $order_number=$wppl_results[0]["wpplOrderNumber"];
            
            
          /*  
            if($is_chorus_pro){
                //$objPHPExcel->getActiveSheet()->setCellValue('F36', 'oui');
                $objPHPExcel->getActiveSheet()->setCellValue('C34', " ".$siret);
                $objPHPExcel->getActiveSheet()->setCellValue('C35', " ".$service_code);
            }else{
                //$objPHPExcel->getActiveSheet()->setCellValue('F36', 'non');
            }
            */
            
            
            $objPHPExcel->getActiveSheet()->setCellValue('B5', $periode);
            $objPHPExcel->getActiveSheet()->setCellValue('B6', $total);
            $objPHPExcel->getActiveSheet()->setCellValue('B4', $label);
            
            /*if(!empty($siret)){
                $objPHPExcel->getActiveSheet()->setCellValue('C37', "Siret : ");
                $objPHPExcel->getActiveSheet()->setCellValue('D37', " ".$siret);
            }
            if(!empty($service_code)){
                $objPHPExcel->getActiveSheet()->setCellValue('C38', "Code service : ");
                $objPHPExcel->getActiveSheet()->setCellValue('D38', " ".$service_code);
            }*/
            
            if(!empty($has_order_number)){
               // $objPHPExcel->getActiveSheet()->setCellValue('C39', "N° bon de commande : ");
                $objPHPExcel->getActiveSheet()->setCellValue('C42', " ".$order_number);
            }

            
            /*$sheet->getStyle('B39')->applyFromArray($head_style_yellow);
            $sheet->getStyle('C39')->applyFromArray($head_style_yellow);
            $sheet->getStyle('D39')->applyFromArray($head_style_yellow);
            
            $sheet->getStyle('B40')->applyFromArray($head_style_yellow);
            $sheet->getStyle('C40')->applyFromArray($head_style_yellow);
            $sheet->getStyle('D40')->applyFromArray($head_style_yellow);

            
            $sheet->getStyle('A3')->applyFromArray($head_style_yellow);*/
            /*$sheet->getStyle('B3')->applyFromArray($head_style_yellow);
            $sheet->getStyle('C3')->applyFromArray($head_style_yellow);
            $sheet->getStyle('D3')->applyFromArray($head_style_yellow);*/
            
            
            $wpplp = $this->registry->getRepository('App\Entity\WpanoramapressListePublication')->createQueryBuilder('wlp');
            $wpplp->where('wlp.wpplpWppl  = '.$id);
            $wpplp_results = $wpplp->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
            if(!empty($wpplp_results))
            {
                $row=9;
                foreach($wpplp_results as $i=>$v){
                    $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $v['wpplpLabel']);
                    $objPHPExcel->getActiveSheet()->setCellValue('B'.$row, $v['wpplpTotal']);
                    $row++;
                }
            }
            
            $objWriter = IOFactory::createWriter($objPHPExcel, 'Xls');
            $objWriter->save($temp_file);
            $urlized=Urlizer::urlize($label);
            return ['name'=>'Panorama _'.$contrat.'_'.$urlized.'.xls', 'path'=>$temp_file];
            
        }

    }
    
    public function GenerateExcellPanoramaPressePapier($id)
    {
        $projectRoot = $this->kernel->getProjectDir();
        if (!file_exists($projectRoot."/var/tmp_upload/")) {
            mkdir($projectRoot."/var/tmp_upload/", 0777, true);
        }
        $temp_file= $projectRoot."/var/tmp_upload/tmp_excel_".time().".xls";
    
        $objReader = IOFactory::createReader('Xls');
    
        $objPHPExcel = $objReader->load($projectRoot."/public/build/images/template_panorama_papier.xls");
    
        $wppl = $this->registry->getRepository('App\Entity\WpanoramapressListe')->createQueryBuilder('wl');
        $wppl->where('wl.wpplId  = '.$id);
        $wppl_results = $wppl->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        if(!empty($wppl_results)){
    
    
    
    
            //Get the panorama info from the composante
            $wpp = $this->registry->getRepository('App\Entity\Wpanoramapress')->createQueryBuilder('w');
            $wpp->where('w.wppId  = '.$wppl_results[0]['wpplWpp']);
            $wpp_results = $wpp->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    
            //get the declaration conf
            $wco = $this->registry->getRepository('App\Entity\Wcocon')->createQueryBuilder('wco');
            $wco->where('wco.wco_dossier  = '.$wpp_results[0]['wppDossier']);
            $wcocon = $wco->getQuery()->getResult();
            $declaration_conf = $this->getDeclarationConfiguration($wcocon[0]);
            $chorus=[];
            foreach($declaration_conf as $i=>$conf){
                foreach($conf as $j=>$c){
                    if($c->getTypeObjet()=="chorus_pro"){
                        $db_data_conf[$i]["wde_is_chorus_pro"]=$this->getValue("wde_is_chorus_pro", $c->wde_dossier, $c->wde_declar);
                        $db_data_conf[$i]["wde_siret"]=$this->getValue("wde_siret", $c->wde_dossier, $c->wde_declar);
                        $db_data_conf[$i]["wde_service_code"]=$this->getValue("wde_service_code", $c->wde_dossier, $c->wde_declar);
                        $db_data_conf[$i]["wde_is_order_number"]=$this->getValue("wde_is_order_number", $c->wde_dossier, $c->wde_declar);
                        $db_data_conf[$i]["wde_order_number"]=$this->getValue("wde_order_number", $c->wde_dossier, $c->wde_declar);
                        $chorus["wde_is_chorus_pro"]=$db_data_conf[$i]["wde_is_chorus_pro"];
                        $chorus["wde_siret"]=$db_data_conf[$i]["wde_siret"];
                        $chorus["wde_service_code"]=$db_data_conf[$i]["wde_service_code"];
                        $chorus["wde_is_order_number"]=$db_data_conf[$i]["wde_is_order_number"];
                        $chorus["wde_order_number"]=$db_data_conf[$i]["wde_order_number"];
    
                    }elseif($c->getTypeObjet()=="panorama_press_numerique"){
                        $db_data_conf[$i]["panorama_press_numerique"]=$this->getValue("panorama_press", $c->wde_dossier, $c->wde_declar);
                    }elseif($c->getTypeObjet()=="panorama_press_papier"){
                        $db_data_conf[$i]["panorama_press_numerique"]=$this->getValue("panorama_press", $c->wde_dossier, $c->wde_declar);
                    }else{
                        if (!empty($c->getSynchroField())) {
                            $db_data_conf[$i][$c->getSynchroField()]=$this->getValue($c->getSynchroField(), $c->wde_dossier, $c->wde_declar);
                        }
                    }
                }
            }
    
            $sheet = $objPHPExcel->getActiveSheet();
            $sheet->setCellValue('B8', $db_data_conf[0]['wco_raisoc1']);
            $sheet->setCellValue('C8', $db_data_conf[0]['wco_raisoc2']);
            $sheet->setCellValue('D8', $db_data_conf[0]['wco_raisoc3']);
    
            $sheet->setCellValue('B9', $db_data_conf[0]['wco_adresse1'].' '.$db_data_conf[0]['wco_adresse2'].' '.$db_data_conf[0]['wco_adresse3']);
            $sheet->setCellValue('B10', $db_data_conf[0]['wco_copos'].' '.$db_data_conf[0]['wco_ville']);
    
            $sheet->setCellValue('B14', $db_data_conf[0]['wde_nom_resp']);
            $sheet->setCellValue('B15', $db_data_conf[0]['wde_fct_resp']);
            $sheet->setCellValue('B16', $db_data_conf[0]['wde_mel_resp']);
            $objPHPExcel->getActiveSheet()->setCellValue('B17', $db_data_conf[0]['wde_tel_resp']);

            $head_style_blue=array(
                'fill' => array(
                    'type' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                    'color' => array('rgb' => '66FFCC')
                )
            );
            $head_style_yellow=array(
                'fill' => array(
                    'type' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                    'color' => array('rgb' => 'FFFFFF00')
                )
            );
            $sheet->getStyle('B8')->applyFromArray($head_style_blue);
            $sheet->getStyle('C8')->applyFromArray($head_style_blue);
            $sheet->getStyle('D8')->applyFromArray($head_style_blue);
    
            $sheet->getStyle('B14')->applyFromArray($head_style_blue);
            $sheet->getStyle('C14')->applyFromArray($head_style_blue);
            $sheet->getStyle('D14')->applyFromArray($head_style_blue);

    
    
            //$row->setBackground('FFFF00');
            //$sheet->getStyle('D40')->setBackground('#FFFF00');
    
    
    
            $contrat=$wpp_results[0]['wppContrat'];
            $declaration=$wpp_results[0]['wppDeclar'];
    
            $wpresta=$this->getValue('wprestataire', $c->wde_dossier, $c->wde_declar);
    
            $prestataire=$wpresta['wpresta_prestataire'];
            $prestataire_label=$wpresta['wpresta_prestataire_label'];
            $prestataire_label2=$wpresta['wpresta_prestataire_label2'];
    
            $objPHPExcel->getActiveSheet()->setCellValue('B7', $contrat);
            $objPHPExcel->getActiveSheet()->setCellValue('B21', ($prestataire?"oui":"non"));
            $objPHPExcel->getActiveSheet()->setCellValue('C21', $prestataire_label.(!empty($prestataire_label2)?$prestataire_label2:", ".$prestataire_label2));
    
    
            $label=$wppl_results[0]["wpplLabel"];
            $periode=$wppl_results[0]["wpplPeriode"];
            $total=$wppl_results[0]["wpplTotal"];
            
            $periode_nombre=$wppl_results[0]["wpplPeriodeNombre"];
            $moyenne_repro=$wppl_results[0]["wpplMoyenneRepro"];
            $moyenne_exemplaire=$wppl_results[0]["wpplMoyenneExemplaire"];
    
            $is_chorus_pro=$wppl_results[0]["wpplIsChorusPro"];
            $siret=$wppl_results[0]["wpplSiret"];
            $service_code=$wppl_results[0]["wpplServiceCode"];
            $has_order_number=$wppl_results[0]["wpplHasOrder"];
            $order_number=$wppl_results[0]["wpplOrderNumber"];

    
 
            if($is_chorus_pro){

                //$objPHPExcel->getActiveSheet()->setCellValue('F36', 'oui');
                $objPHPExcel->getActiveSheet()->setCellValue('D38', " ".$siret);
                $objPHPExcel->getActiveSheet()->setCellValue('D39', " ".$service_code);
            }else{
                //$objPHPExcel->getActiveSheet()->setCellValue('F36', 'non');
            }
    
    
            $objPHPExcel->getActiveSheet()->setCellValue('J2', $label);
            $objPHPExcel->getActiveSheet()->setCellValue('B31', $periode);
            $objPHPExcel->getActiveSheet()->setCellValue('C31', $periode_nombre);
            $objPHPExcel->getActiveSheet()->setCellValue('D31', $moyenne_repro);
            $objPHPExcel->getActiveSheet()->setCellValue('E31', $moyenne_exemplaire);
            //$objPHPExcel->getActiveSheet()->setCellValue('F31', $total);
    
    
            if(!empty($has_order_number)){
                // $objPHPExcel->getActiveSheet()->setCellValue('C39', "N° bon de commande : ");
                $objPHPExcel->getActiveSheet()->setCellValue('D46', " ".$order_number);
            }

            $sheet->getStyle('A3')->applyFromArray($head_style_yellow);

            $wpplp = $this->registry->getRepository('App\Entity\WpanoramapressListePublication')->createQueryBuilder('wlp');
            $wpplp->where('wlp.wpplpWppl  = '.$id);
            $wpplp_results = $wpplp->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
            if(!empty($wpplp_results))
            {
                $row=9;
                foreach($wpplp_results as $i=>$v){
                    $objPHPExcel->getActiveSheet()->setCellValue('H'.$row, $v['wpplpLabel']);
                    $objPHPExcel->getActiveSheet()->setCellValue('J'.$row, $v['wpplpTotal']);
                    $row++;
                }
            }
    
            $objWriter = IOFactory::createWriter($objPHPExcel, 'Xls');
            $objWriter->save($temp_file);
            $urlized=Urlizer::urlize($label);
            return ['name'=>'Panorama _'.$contrat.'_'.$urlized.'.xls', 'path'=>$temp_file];
    
        }
    
    }
    
    public function update_BDC_press_liste($wppl_id, $wppl_order_number, $wppl_dossier, $wppl_declar){
        $sql="update wpanoramapress_liste set wppl_order_number=:wppl_order_number where wppl_id=:wppl_id and wppl_dossier=:wppl_dossier and wppl_declar=:wppl_declar  LIMIT 1 ";
        $stmt=$this->connection->prepare($sql);
        $stmt->bindParam(':wppl_order_number',   $wppl_order_number,    \PDO::PARAM_STR);
        $stmt->bindParam(':wppl_dossier', $wppl_dossier,  \PDO::PARAM_STR);
        $stmt->bindParam(':wppl_declar',      $wppl_declar,       \PDO::PARAM_STR);
        $stmt->bindParam(':wppl_id',      $wppl_id,       \PDO::PARAM_STR);

        $stmt->execute();
        
        
    }

    public function update_BDC_declar( $wde_order_number, $wde_dossier, $wde_declar){
        $sql="update wdeclar set wde_order_number=:wde_order_number where  wde_dossier=:wde_dossier and wde_declar=:wde_declar  LIMIT 1 ";
        $stmt=$this->connection->prepare($sql);
        $stmt->bindParam(':wde_order_number',   $wde_order_number,    \PDO::PARAM_STR);
        $stmt->bindParam(':wde_dossier', $wde_dossier,  \PDO::PARAM_STR);
        $stmt->bindParam(':wde_declar',      $wde_declar,       \PDO::PARAM_STR);
        $stmt->execute();
        
    }
    
   public function checkMediaKey($key){
       list($k,$id)=explode('%',$key);
       if(md5($id.$this->download_key)==$k){
           return $id;
       }else{
           return false;
       }
   }
   
}
