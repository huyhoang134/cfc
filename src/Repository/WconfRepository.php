<?php

namespace App\Repository;

use App\Entity\Wconf;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Wconf|null find($id, $lockMode = null, $lockVersion = null)
 * @method Wconf|null findOneBy(array $criteria, array $orderBy = null)
 * @method Wconf[]    findAll()
 * @method Wconf[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WconfRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Wconf::class);
    }

    // /**
    //  * @return Wconf[] Returns an array of Wconf objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('w.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Wconf
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
