<?php

namespace App\Services;

use App\Entity\Wdeclar;
use App\Entity\Wcocon;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class CustomerService 
{
    /**
     * @var SSOApi
     */
    private $ssoApi;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var ParameterBagInterface
     */
    private $params;

    public function __construct(EntityManagerInterface $entityManager, SSOApi $ssoApi, ParameterBagInterface $params)
    {
        $this->entityManager = $entityManager;
        $this->ssoApi = $ssoApi;
        $this->params = $params;
    }

    public function loginCheckAction(array $request): JsonResponse
    {
        $url  = '/api/v1/security/login-grant';

        $postFields = [
            'username' => $request['_username'],
            'password' => $request['_password'],
        ];

        [$res, $code] = $this->ssoApi->request($url, true, $postFields);
        

        if ($res['status'] !== 'ok') {
            return new JsonResponse([$res['message']]);
        }


        $user=$this->logUser($res['data']['user'], $res['data']['tokens']);

        if (empty($user)) {
            return new JsonResponse(['Utilisateur inconu']);
        }
      
        /*sm($res);
        die();
        $sso_data_table=$this->params->get('sso_data_table'); //target wdeclar id.
        sm($sso_data_table);
        //sm($this->ssoApi->request($url, true, $postFields));
        die("postfields");*/

        
        return new JsonResponse([
            'token'         => $res['data']['tokens']['access_token']['hash'],
//            'refresh_token' => $res['data']['tokens']['refresh_token']['hash'],
            'name'          => $res['data']['user'],
        ]);
    }
    
    public function getUserDetail()
    {
        $token      = $_GET['backToken'] ;//request->get('backUsername', null);
        $url  = '/api/v1/security/get-user-by-token?SaveToken='.urlencode($token);

        [$res, $code] = $this->ssoApi->request($url,false);

        
        if ($res['status'] !== 'ok') {
            return new JsonResponse($res['message']);
        }

        $this->logUser($res['data']['user'], $res['data']['tokens']);

        return $this->syncUser($res['data']['user']);
        /*return $this->entityManager->getRepository(Wdeclar::class)->findOneBy([
            'wde_dossier' => $res['data']['username'],
        ]);*/
    }

    /**
     * Log user.
     *
     * @param array $userData       The user data
     * @param array $token          The token
     *
     * @return object
     */
    public function logUser(array $userData, array $token)
    {
        $_SESSION['tokens'] = [
            'token'         => $token['access_token']['hash'],
//            'refresh_token' => $token['refresh_token']['hash'],
        ];
        $_SESSION['user_data'] = [
            'token'         => $userData,
            //            'refresh_token' => $token['refresh_token']['hash'],
        ];
        return $this->syncUser($userData);
    }

    public function syncUser($userData)
    {

        //
        if(is_object($userData))
        {
            //debug_print_backtrace();
            //debug_print_backtrace(1);
            //throw new \Exception();
            sm($userData);
            die();
            //sm($_SESSION);
            $user_data = json_decode($this->getUserDetail()->getContent());
            //dd($userData);
        }

        $wdeclar_list=[];
        $sso_data_table=$this->params->get('sso_data_table');

            foreach($userData as $u){
               
                if(is_array($u)){
                    if($u['sso_table']==$sso_data_table){
                        $wdeclar_id=$u['sso_id_reccord'];
                        $wdeclar_list[]=$this->entityManager->getRepository(Wdeclar::class)->findOneBy([
                            'wde_declar' => $wdeclar_id,
                        ]);
                    }
                }elseif(is_object($u)){
                    if($u->sso_table==$sso_data_table){
                        $wdeclar_id=$u->sso_id_reccord;
                        $wdeclar_list[]=$this->entityManager->getRepository(Wdeclar::class)->findOneBy([
                            'wde_declar' => $wdeclar_id,
                        ]);
                    }
                }
            
              
            }

        //sm($userData);
        //sm($wdeclar_list);
        $user=$this->entityManager->getRepository(Wcocon::class)->findOneBy([
            'wco_dossier' => $wdeclar_list[0]->getWdeDossier(),
        ]);

        $user->setSsoSyncData($userData);
            /*$user=$wdeclar_list[0]->getWdeDossier();
            foreach($wdeclar_list as $d){
                $user->addWdeclar($d);
            }*/


        //if user is null we dont need to create it.
        /*if ($user === null) {
            $user = $this->createFullUser($userData, $user);
        }*/

        return $user;
    }

    /**
     * Create the full user.
     *
     * @param array $userData
     * 
     */
    public function createFullUser(array $userData): Wdeclar
    {
       /* $user = new Wdeclar();
        $user->setWdeDeclar($userData['username'].'123');
        $user->setWdePassword($userData['wdePassword']);
        $user->setWdeTypcont($userData['wdeTypcont']);
        $user->setWdeStamp(new \DateTime($userData['wdeStamp']['date']));
        $user->setWdeSynchro(new \DateTime($userData['wdeSynchro']['date']));
        $user->setWdeContrat($userData['wdeContrat']);
        $user->setWdeAnnee($userData['wdeAnnee']);
        $user->setWdeNumperiod($userData['wdeNumperiod']);
        $user->setWdeEleves($userData['wdeEleves']);
        $user->setWdeTranche($userData['wdeTranche']);
        $user->setWdeCopieurs($userData['wdeCopieurs']);
        $user->setWdeProfs($userData['wdeProfs']);
        $user->setWdeEtatDeclar($userData['wdeEtatDeclar']);
        $user->setWdeDateValidation(new \DateTime($userData['wdeDateValidation']['date']));
        $user->setWdeEmailSent($userData['wdeEmailSent']);
        $user->setWdeNomResp($userData['wdeNomResp']);
        $user->setWdeTitResp($userData['wdeTitResp']);
        $user->setWdeFctResp($userData['wdeFctResp']);
        $user->setWdeMelResp($userData['wdeMelResp']);
        $user->setWdeEffectif($userData['wdeEffectif']);
        $user->setWdeTranche01($userData['wdeTranche01']);
        $user->setWdeTranche02($userData['wdeTranche02']);
        $user->setWdeTranche03($userData['wdeTranche03']);
        $user->setWdeTranche04($userData['wdeTranche04']);
        $user->setWdeTranche05($userData['wdeTranche05']);
        $user->setWdeTranche06($userData['wdeTranche06']);
        $user->setWdeTranche07($userData['wdeTranche07']);
        $user->setWdeIdCtNomSecGen($userData['wdeIdCtNomSecGen']);
        $user->setWdeNomSecGen($userData['wdeNomSecGen']);
        $user->setWdeTitSecGen($userData['wdeTitSecGen']);
        $user->setWdeFctSecGen($userData['wdeFctSecGen']);
        $user->setWdeMelSecGen($userData['wdeMelSecGen']);
        $user->setWdeIntituleDecl($userData['wdeIntituleDecl']);
        $user->setWdeAnneeDecl($userData['wdeAnneeDecl']);
        $user->setWdeTranche08($userData['wdeTranche08']);
        $user->setWdeTranche09($userData['wdeTranche09']);
        $user->setWdeTranche10($userData['wdeTranche10']);
        $user->setWdeTranche11($userData['wdeTranche11']);
        $user->setWdeTranche12($userData['wdeTranche12']);
        $user->setWdeTranche13($userData['wdeTranche13']);
        $user->setWdeTranche14($userData['wdeTranche14']);
        $user->setWdeTranche15($userData['wdeTranche15']);
        $user->setWdeTranche16($userData['wdeTranche16']);
        $user->setWdeTranche17($userData['wdeTranche17']);
        $user->setWdeTranche18($userData['wdeTranche18']);
        $user->setWdeTranche19($userData['wdeTranche19']);
        $user->setWdeTranche20($userData['wdeTranche20']);
        $user->setWdeTranche21($userData['wdeTranche21']);
        $user->setWdeTranche22($userData['wdeTranche22']);
        $user->setWdeIdCtResp($userData['wdeIdCtResp']);
        $user->setWdeTelResp($userData['wdeTelResp']);
        $user->setWdeFaxResp($userData['wdeFaxResp']);
        $user->setWdeEffectif2($userData['wdeEffectif2']);
        $user->setWdeEffectif3($userData['wdeEffectif3']);
        $user->setWdeEffectif4($userData['wdeEffectif4']);
        $user->setWdeIsChorusPro($userData['wdeIsChorusPro']);
        $user->setWdeSiret($userData['wdeSiret']);
        $user->setWdeServiceCode($userData['wdeServiceCode']);
        $user->setWdeOrderNumber($userData['wdeOrderNumber']);
        $user->setWdeIdCtFact($userData['wdeIdCtFact']);
        $user->setWdeNomFact($userData['wdeNomFact']);
        $user->setWdeTitFact($userData['wdeTitFact']);
        $user->setWdeFctFact($userData['wdeFctFact']);
        $user->setWdeRaisoc1Fact($userData['wdeRaisoc1Fact']);
        $user->setWdeAdresse1Fact($userData['wdeAdresse1Fact']);
        $user->setWdeAdresse2Fact($userData['wdeAdresse2Fact']);
        $user->setWdeAdresse3Fact($userData['wdeAdresse3Fact']);
        $user->setWdeCoposFact($userData['wdeCoposFact']);
        $user->setWdeVilleFact($userData['wdeVilleFact']);
        $user->setWdeTelFact($userData['wdeTelFact']);
        $user->setWdeMailFact($userData['wdeMailFact']);
        $user->setWdeIdAdrFact($userData['wdeIdAdrFact']);

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $user;*/
    }

    /**
     * @param string $parameter
     *
     * @return false|string
     */
    public function encryptParameter(string $parameter)
    {
        // Store the cipher method
        $ciphering = "AES-256-CBC";

        // Use OpenSSl Encryption method
        $options = 0;

        // Non-NULL Initialization Vector for encryption
        $encryption_iv = $this->params->get('encryption_iv');

        // Store the encryption key
        $encryption_key = $this->params->get('encryption_key');
        
        // Use openssl_encrypt() function to encrypt the data
        $encryption = openssl_encrypt($parameter, $ciphering,
            $encryption_key, $options, $encryption_iv);

        return urlencode($encryption);
    }
    
    /**
     * @param string $parameter
     *
     * @return false|string
     */
    public function decryptParameter(string $parameter)
    {
        // Store the cipher method
        $ciphering = "AES-256-CBC";
    
        // Use OpenSSl Encryption method
        $options = 0;
    
        $decryption_iv = $this->params->get('encryption_iv');
    
        // Store the decryption key
        $decryption_key = $this->params->get('encryption_key');
    
        // Use openssl_decrypt() function to decrypt the data
        return openssl_decrypt ($parameter, $ciphering,
            $decryption_key, $options, $decryption_iv);
    }
}
