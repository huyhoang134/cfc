<?php

namespace App\Admin;


use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Sonata\AdminBundle\Form\Type\ModelAutocompleteType;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\AdminBundle\Form\Type\ModelListType;
use Sonata\Form\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Sonata\CoreBundle\Form\Type\DatePickerType;


use Symfony\Component\Routing\Generator\UrlGeneratorInterface as RoutingUrlGeneratorInterface;
use Sonata\FormatterBundle\Form\Type\SimpleFormatterType;

use Knp\Menu\ItemInterface as MenuItemInterface;
use Sonata\AdminBundle\Admin\AdminInterface;

use Sonata\AdminBundle\Route\RouteCollection;
#use Pix\SortableBehaviorBundle\Services\PositionORMHandler as PositionHandler;
use Sonata\AdminBundle\Controller\CRUDController;
use Burgov\Bundle\KeyValueFormBundle\Form\Type\KeyValueType;


class ExportModuleAdmin extends AbstractAdmin
{
    
    public function __construct( $code, $class, $baseControllerName ) {
        parent::__construct( $code, $class, $baseControllerName );
        

    }
    
    public function configure()
    {
        parent::configure();
    }
    
    
    protected function configureRoutes(RouteCollection $collection)
    {
        //$collection->clearExcept(['list']);
        $collection->clearExcept(['export-delef']);
        $collection->remove('create');
        $collection->add('export-delef');
    }
    

    public function setContainer(ContainerInterface $container)
    {
        $this->container = $container;
    }
    
   
    public function getDashboardActions()
    {
        $actions = parent::getDashboardActions();
    
        $actions['import'] = [
            'label'              => 'Export',
            'url'                => $this->generateUrl('export-delef'),
            'icon'               => ' fas fa-file-export',
            'translation_domain' => 'SonataAdminBundle', // optional
            'template'           => '@SonataAdmin/CRUD/dashboard__action.html.twig', // optional
        ];
    
        return $actions;
    }
    
}