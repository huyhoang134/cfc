<?php

namespace App\Admin;


use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Sonata\AdminBundle\Form\Type\ModelAutocompleteType;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\AdminBundle\Form\Type\ModelListType;
use Sonata\Form\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

use Sonata\AdminBundle\Route\RouteCollection;

use App\Entity\Wcocon;
use App\Entity\Wdeclar;
//use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Sonata\MediaBundle\Provider\FileProvider;

class WcoconAdmin extends AbstractAdmin
{
    private $provider;
    public function __construct( $code, $class, $baseControllerName , FileProvider $provider) {
        $this->provider=$provider;
        parent::__construct( $code, $class, $baseControllerName );
    

    }
    

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('wco_dossier');
        $datagridMapper->add('wco_raisoc1');
        $datagridMapper->add('wco_raisoc2');
        $datagridMapper->add('wco_copos');
        $datagridMapper->add('wco_ville');
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        //add route for our custuom button
        //$collection->add('declar', $this->getRouterIdParameter().'/show');
        $collection->add('wlistdeclar', $this->getRouterIdParameter().'/wdeclarlist/list');
        $collection->remove('create');
        $collection->remove('edit');
        $collection->remove('delete');
        $collection->remove('export');/**/
    }
    
    
    protected function configureListFields(ListMapper $listMapper)
    {
        
        $listMapper->add('_action', 'actions', [
            'header_style' => 'width: 120px',
            'actions' => [
                /*'edit' => [],*/
               /* 'declar' => [
                    'template' => 'Admin/list__action_declar.html.twig'
                ],*/
                'wlistdeclar' => [
                    'template' => 'Admin/list__action_wlistdeclar.html.twig'
                ]
            ]
        ]);
        
        
        $listMapper->add('wco_dossier', null, ['label' => 'Dossier', 'header_style' => 'width: 50px']);
        $listMapper->add('wco_raisoc1', null, ['label' => 'Raison sociale', 'header_style' => 'width: 150px']);
        $listMapper->add('wco_raisoc2', null, ['label' => 'Raison sociale', 'header_style' => 'width: 150px']);
        $listMapper->add('wco_copos', null, ['label' => 'Code postale', 'header_style' => 'width: 150px']);
        $listMapper->add('wco_ville', null, ['label' => 'Ville', 'header_style' => '']);
        
        //unset mosaic mode in list view
        unset($this->listModes['mosaic']);

    }
    

    
    public function createQuery($context = 'list')
    {
        if($context=='list'){
            
            //Get current type id 
            $id=$this->getRequest()->get('id');
            //Get current typecont
            $typecont=parent::createQuery()->getEntityManager()->getRepository('App\Entity\Wtype')->findOneBy(['id'=>$id])->getLabel();
            $proxyQuery = parent::createQuery('list');
            $proxyQuery->leftJoin(
                'App\Entity\Wdeclar',
                'd',
                \Doctrine\ORM\Query\Expr\Join::WITH,
                'o.wco_dossier = d.wde_dossier'
                );
            $proxyQuery->where('d.wde_typcont = :typecont');
            $proxyQuery->setParameter('typecont', $typecont);
            
           
        }

    
        return $proxyQuery;
    }
    
    
    

    
    

   /* 
    public function getShowConfigurationContracts($conf){
        $contracts=[];
        foreach($conf as $c){
           if(!in_array( $c[0]->wde_declar, $contracts))
            {
                $contracts[]=$c[0]->wde_declar;
            }
        }
        return $contracts;
    }

*/
    
    
   



    public function getExportFormats()
    {
        //remove the export button
        return [/*"json", "xml", "csv", "xls"*/];
    }
    
    
    //Remove batch actions
    public function getBatchActions()
    {
        $actions = parent::getBatchActions();
        unset($actions['delete']);
    
        return $actions;
    }
 
   
}