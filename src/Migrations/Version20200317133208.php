<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200317133208 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE chorus (id INT AUTO_INCREMENT NOT NULL, type_identifiant INT DEFAULT NULL, identifiant VARCHAR(255) DEFAULT NULL, raison_sociale VARCHAR(255) DEFAULT NULL, adresse VARCHAR(255) DEFAULT NULL, code_postal VARCHAR(255) DEFAULT NULL, ville VARCHAR(255) DEFAULT NULL, pays_code VARCHAR(255) DEFAULT NULL, pays_libelle VARCHAR(255) DEFAULT NULL, service_code VARCHAR(255) DEFAULT NULL, service_nom VARCHAR(255) DEFAULT NULL, service_gestion_egmt TINYINT(1) NOT NULL, service_service_actif TINYINT(1) NOT NULL, emetteur_edi TINYINT(1) NOT NULL, recepteur_edi TINYINT(1) NOT NULL, gestion_statut_mise_en_paiement TINYINT(1) NOT NULL, gestion_engagement TINYINT(1) NOT NULL, gestion_service TINYINT(1) NOT NULL, gestion_service_engagement TINYINT(1) NOT NULL, est_moa TINYINT(1) NOT NULL, est_moa_uniquement TINYINT(1) NOT NULL, structure_active TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE chorus');
    }
}
