<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221115061101 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE wcontrat (wct_contrat INT AUTO_INCREMENT NOT NULL, wct_stamp DATETIME NOT NULL, wct_synchro DATETIME DEFAULT NULL, wct_dossier INT NOT NULL, wct_typcont VARCHAR(50) NOT NULL, wct_nom_resp VARCHAR(50) DEFAULT NULL, wct_tit_resp VARCHAR(25) DEFAULT NULL, wct_fct_resp VARCHAR(50) DEFAULT NULL, wct_mel_resp VARCHAR(80) DEFAULT NULL, wct_nom_sec_gen VARCHAR(50) DEFAULT NULL, wct_tit_sec_gen VARCHAR(25) DEFAULT NULL, wct_fct_sec_gen VARCHAR(50) DEFAULT NULL, wct_mel_sec_gen VARCHAR(80) DEFAULT NULL, wct_tel_resp VARCHAR(25) DEFAULT NULL, wct_fax_resp VARCHAR(25) DEFAULT NULL, wct_is_chorus_pro SMALLINT NOT NULL, wct_siret VARCHAR(14) DEFAULT NULL, wct_service_code VARCHAR(100) NOT NULL, wct_order_number VARCHAR(50) DEFAULT NULL, wct_nom_fact VARCHAR(50) DEFAULT NULL, wct_tit_fact VARCHAR(35) DEFAULT NULL, wct_fct_fact VARCHAR(50) DEFAULT NULL, wct_raisoc1_fact VARCHAR(50) DEFAULT NULL, wct_adresse1_fact VARCHAR(50) DEFAULT NULL, wct_adresse2_fact VARCHAR(50) DEFAULT NULL, wct_adresse3_fact VARCHAR(50) DEFAULT NULL, wct_copos_fact VARCHAR(16) DEFAULT NULL, wct_ville_fact VARCHAR(42) DEFAULT NULL, wct_tel_fact VARCHAR(26) DEFAULT NULL, wct_id_ct_fact INT DEFAULT NULL, wct_id_ct_nom_sec_gen INT DEFAULT NULL, wct_id_ct_resp INT DEFAULT NULL, wct_id_adr_fact INT DEFAULT NULL, wct_password VARCHAR(8) DEFAULT NULL, wct_is_order_number SMALLINT DEFAULT NULL, wct_mail_fact VARCHAR(80) DEFAULT NULL, wct_id_ct_nom_sec_gen_question INT DEFAULT NULL, wct_id_ct_resp_question INT DEFAULT NULL, wct_id_ct_fact_question INT DEFAULT NULL, wct_id_adr_fact_question INT DEFAULT NULL, FkSecGenInit INT DEFAULT NULL, FkIdRespInit INT DEFAULT NULL, FkIdCtFactInit INT DEFAULT NULL, FkAdrFactInit INT DEFAULT NULL, FkUserDea VARCHAR(3) DEFAULT NULL, FkFacture INT DEFAULT NULL, CodeTypeContrat VARCHAR(2) DEFAULT NULL, FkFactType INT DEFAULT NULL, CoordonneesAJour TINYINT(1) NOT NULL, RaisSocFact1 VARCHAR(50) DEFAULT NULL, RaisSocFact2 VARCHAR(50) DEFAULT NULL, PRIMARY KEY(wct_contrat)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE wdeclef (wdf_declar INT AUTO_INCREMENT NOT NULL, wdf_stamp DATETIME NOT NULL, wdf_synchro DATETIME DEFAULT NULL, wdf_dossier INT NOT NULL, wdf_contrat INT NOT NULL, wdf_annee INT NOT NULL, wdf_annee_decl VARCHAR(9) NOT NULL, wdf_eleves INT DEFAULT NULL, wdf_tranche VARCHAR(3) DEFAULT NULL, wdf_copieurs INT DEFAULT NULL, wdf_profs INT DEFAULT NULL, wdf_etat_declar VARCHAR(1) NOT NULL, wdf_date_validation DATETIME DEFAULT NULL, wdf_email_sent SMALLINT DEFAULT NULL, wdf_effectif INT DEFAULT NULL, wdf_tranche01 INT DEFAULT NULL, wdf_tranche02 INT DEFAULT NULL, wdf_tranche03 INT DEFAULT NULL, wdf_tranche04 INT DEFAULT NULL, wdf_tranche05 INT DEFAULT NULL, wdf_tranche06 INT DEFAULT NULL, wdf_tranche07 INT DEFAULT NULL, wdf_intitule_decl VARCHAR(255) DEFAULT NULL, wdf_tranche08 INT DEFAULT NULL, wdf_tranche09 INT DEFAULT NULL, wdf_tranche10 INT DEFAULT NULL, wdf_tranche11 INT DEFAULT NULL, wdf_tranche12 INT DEFAULT NULL, wdf_tranche13 INT DEFAULT NULL, wdf_tranche14 INT DEFAULT NULL, wdf_tranche15 INT DEFAULT NULL, wdf_tranche16 INT DEFAULT NULL, wdf_effectif2 INT DEFAULT NULL, wdf_effectif3 INT DEFAULT NULL, LibelleTarif VARCHAR(60) DEFAULT NULL, FkProforma INT DEFAULT NULL, wdf_effectif4 INT DEFAULT NULL, wdf_tranche17 INT DEFAULT NULL, wdf_tranche18 INT DEFAULT NULL, wdf_tranche19 INT DEFAULT NULL, wdf_tranche20 INT DEFAULT NULL, wdf_tranche21 INT DEFAULT NULL, wdf_tranche22 INT DEFAULT NULL, MotifAttente VARCHAR(3) DEFAULT NULL, wdf_numperiod VARCHAR(255) DEFAULT NULL, PRIMARY KEY(wdf_declar)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE wdocument (PkIndexDocument INT AUTO_INCREMENT NOT NULL, FkDocument INT NOT NULL, NomFichier VARCHAR(200) DEFAULT NULL, Titre VARCHAR(200) DEFAULT NULL, MotsClefs VARCHAR(200) DEFAULT NULL, doc_source VARCHAR(1) DEFAULT NULL, doc_dossier_ftp VARCHAR(200) DEFAULT NULL, doc_stamp DATETIME NOT NULL, doc_synchro DATETIME DEFAULT NULL, PRIMARY KEY(PkIndexDocument)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE wliendocument (PkLienDocument INT AUTO_INCREMENT NOT NULL, FkIndexDocument INT NOT NULL, NomTable VARCHAR(50) NOT NULL, ClefTable INT NOT NULL, Stamp DATETIME NOT NULL, Synchro DATETIME DEFAULT NULL, PRIMARY KEY(PkLienDocument)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE wcontrat');
        $this->addSql('DROP TABLE wdeclef');
        $this->addSql('DROP TABLE wdocument');
        $this->addSql('DROP TABLE wliendocument');
    }
}
