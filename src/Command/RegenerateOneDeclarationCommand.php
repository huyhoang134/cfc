<?php 
namespace App\Command;

use App\Entity\Wconf;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\Persistence\ManagerRegistry;

use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\Filesystem\Filesystem;
use Twig\Environment;

use App\Entity\Wcocon;
use App\Entity\Wdeclar;
use App\Entity\Wtype;




class RegenerateOneDeclarationCommand extends Command
{
    private $nb_thread;
    private $my_argument_name;

    /**
     *  @var ManagerRegistry
     */
    private $doctrine;
    /**
     * @var Environment
     */
    protected $twig;
    /** 
     * @var \Swift_Mailer
     */
    private $mailer;

    public function __construct(ManagerRegistry $doctrine, Environment $twig, \Swift_Mailer $mailer, ParameterBagInterface $params)
    {
        parent::__construct();

        $this->doctrine = $doctrine;
        $this->twig = $twig;
        $this->mailer = $mailer;
        $this->em = $this->doctrine->getManager();
        $this->params = $params;
    }

    protected function configure()
    {
        $this->setName('delef:regenerate-one-declar')
            ->setDescription('Re-generate declaration')
            ->setHelp('')
            ->addArgument('wdeclar_id', InputArgument::REQUIRED, 'wdeclar id')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->property_accessor = PropertyAccess::createPropertyAccessor();
        $from_email = $this->params->get('delef_from_email');
        $from_name = $this->params->get('delef_from_name');
        
        $wdeclar_id=$input->getArgument('wdeclar_id');
        

        $io = new SymfonyStyle($input, $output);
        
        //$io->title('Delef Email Validation');
        
        $this->em->getConnection()->getConfiguration()->setSQLLogger(null);
        
        /** @var Wdeclar[] */
        $qb=$this->em->createQueryBuilder();
        $qb->select('d')->from('App\Entity\Wdeclar', 'd')
        ->leftJoin(
            'App\Entity\Wtype',
            't',
            \Doctrine\ORM\Query\Expr\Join::WITH,
            'd.wde_typcont = t.label'
            )
        ->where("d.wde_declar = ".$wdeclar_id." ")
        ;
        $declars=$qb->getQuery()->getResult();
        
        
   
        foreach ($declars as $i=>$declar) {
           
            $wcocon = $declar->getWdeDossier();
            /** @var Wtype */
            $wtype = $this->doctrine
                ->getRepository(Wtype::class)
                ->findOneBy(['label' => $declar->getWdeTypcont()])
            ;

            $WcoconRepository=$this->doctrine->getRepository(Wcocon::class);
            $wconfs=$WcoconRepository->getDeclarationConfigurationByWdeclar($declar);
                
            $WcoconRepository = $this->doctrine->getRepository(Wcocon::class);
            $pdf_file_path = $WcoconRepository->GeneratePdf($declar->getWdeDeclar(), true);

             //$io->note(memory_get_usage() .' - fichier pdf : '.$pdf_file_path['path']);
             echo memory_get_usage() .' - fichier pdf : '.$pdf_file_path['path']."\n";

        }


    }
    
    private function getEmailValue($declar, $wcocon, $field_name){
        
        if (substr($field_name, 0, 3) == 'wde') {
            $target_email = $this->property_accessor->getValue($declar, $field_name);
        }
        else if (substr($field_name, 0, 3) == 'wco') {
            $target_email = $this->property_accessor->getValue($wcocon, $field_name);
        }
        return str_replace('..','.',$target_email);
    }
    

}
