<?php 
namespace App\Command;

use App\Entity\Wconf;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\Persistence\ManagerRegistry;

use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\Filesystem\Filesystem;
use Twig\Environment;

use App\Entity\Wcocon;
use App\Entity\Wdeclar;
use App\Entity\Wtype;




class SendValidationEmailCommand extends Command
{
    private $nb_thread;
    private $my_argument_name;

    /**
     *  @var ManagerRegistry
     */
    private $doctrine;
    /**
     * @var Environment
     */
    protected $twig;
    /** 
     * @var \Swift_Mailer
     */
    private $mailer;

    public function __construct(ManagerRegistry $doctrine, Environment $twig, \Swift_Mailer $mailer, ParameterBagInterface $params)
    {
        parent::__construct();

        $this->doctrine = $doctrine;
        $this->twig = $twig;
        $this->mailer = $mailer;
        $this->em = $this->doctrine->getManager();
        $this->params = $params;
    }

    protected function configure()
    {
        $this->setName('delef:email:validation')
            ->setDescription('Send validation email when status of primary declaration is F or A')
            ->setHelp('')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->property_accessor = PropertyAccess::createPropertyAccessor();
        $from_email = $this->params->get('delef_from_email');
        $from_name = $this->params->get('delef_from_name');

        $io = new SymfonyStyle($input, $output);
        
        $io->title('Delef Email Validation');
        
        /** @var Wdeclar[] */
        $qb=$this->em->createQueryBuilder();
        $qb->select('d')->from('App\Entity\Wdeclar', 'd')
        ->leftJoin(
            'App\Entity\Wtype',
            't',
            \Doctrine\ORM\Query\Expr\Join::WITH,
            'd.wde_typcont = t.label'
            )
        ->where("d.wde_etat_declar in ('F', 'X') ")
        ->AndWhere('d.wde_email_sent = :wde_email_sent')
        ->setMaxResults(10)
        //->AndWhere('t.priorite = :priorite')
       // ->setParameter('wde_etat_declar', 'F')
        ->setParameter('wde_email_sent', 0);
        //->groupBy('d.wde_dossier');
        //->setParameter('priorite', 0)
        ;
        $declars=$qb->getQuery()->getResult();
        foreach ($declars as $declar) {
            $wcocon = $declar->getWdeDossier();
            /** @var Wtype */
            $wtype = $this->doctrine
                ->getRepository(Wtype::class)
                ->findOneBy(['label' => $declar->getWdeTypcont()])
            ;
            /** @var Wconf[] */
            /*$wconfs = $this->doctrine
                ->getRepository(Wconf::class)
                ->findBy([
                    'wconf_typcont' => $wtype->getId(),
                    'actif'  => TRUE
                ])
            ;*/
            $WcoconRepository=$this->doctrine->getRepository(Wcocon::class);
            $wconfs=$WcoconRepository->getDeclarationConfigurationByWdeclar($declar);
                
                
            $mail_content = $this->twig->render(
                'emails/validation.html.twig', [
                    'signature' => $wtype->getEmailSignature()
                ]
            );
            $primary="";
            $secondary="";
            $secondary_bis="";
            foreach ($wconfs as $wconf) {
                foreach(
                    $wconf as $conf){
                    switch ($conf->getValidationEmail()) {
                        case 1:
                            $primary = $this->getEmailValue($declar, $wcocon,  $conf->getSynchroField());
                            break;
                        case 2:
                            $secondary = $this->getEmailValue($declar, $wcocon, $conf->getSynchroField());
                            break;
                        case 3:
                            $secondary_bis = $this->getEmailValue($declar, $wcocon, $conf->getSynchroField());
                            break;
                    }
                }
                
            }

            $recipients = [];
            //if primary is filled => primary + secondary
            if (!empty($primary)) {
                $recipients[] = $primary;
		if (!empty($secondary)) {
                	$recipients[] = $secondary;
		}
                
            }else{
                //if primary is empty => secondary + secondary bis
                if (!empty($secondary)) {
			$recipients[] = $secondary;
		}
                if(!empty($secondary_bis)){
                    $recipients[] = $secondary_bis;
                }
            }

		//dump($recipients);
            $message = (new \Swift_Message('Declaration CFC - Validation'))
                ->setFrom($from_email, $from_name)
                ->setTo($recipients)
                ->setBcc([
                    //'tbourdin@partitech.com' => 'Thomas Bourdin',
                    'f.zelie@cfcopies.com' => 'Frantz ZELIE',
                    'e.kamm@cfcopies.com' => 'Elise KAMM'
                ])
                ->setBody($mail_content, 'text/html')
            ;
   
            $WcoconRepository = $this->doctrine->getRepository(Wcocon::class);
            $pdf_file_path = $WcoconRepository->GeneratePdf($declar->getWdeDeclar(), true);

            if ($pdf_file_path) {
                $message->attach(\Swift_Attachment::fromPath($pdf_file_path['path']));
            }

            if ($this->mailer->send($message) == 0) {
                $io->error('Error sending email!');
            } else {
                $io->success('Emails sent succesfully.');
                $io->note('wde_contrat : '.$declar->getWdeDeclar());
                $io->note('Type contrat : '.$declar->getWdeTypcont());
                foreach($recipients as $email){
                    $io->note('email : '.$email);
                }
                $cn = $this->em->getConnection();
                $stmt = $cn->prepare("update wdeclar set wde_email_sent=1  where wde_declar='".$declar->getWdeDeclar()."' ;");
                $stmt->execute();
            }
        }
    }
    
    private function getEmailValue($declar, $wcocon, $field_name){
        
        if (substr($field_name, 0, 3) == 'wde') {
            $target_email = $this->property_accessor->getValue($declar, $field_name);
        }
        else if (substr($field_name, 0, 3) == 'wco') {
            $target_email = $this->property_accessor->getValue($wcocon, $field_name);
        }
        $target_email=str_replace("'",'',$target_email);
        return str_replace('..','.',$target_email);
    }
    

}
