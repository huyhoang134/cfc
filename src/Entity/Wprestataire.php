<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Wprestataire
 *
 * @ORM\Table(name="wprestataire")
 * @ORM\Entity
 */
class Wprestataire
{
    /**
     * @var int
     *
     * @ORM\Column(name="wpresta_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $wprestaId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="wpresta_stamp", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $wprestaStamp = 'CURRENT_TIMESTAMP';
    
    /**
     * @var int|null
     *
     * @ORM\Column(name="wpresta_declar", type="integer", nullable=true)
     */
    private $wprestaDeclar;

    /**
     * @var int|null
     *
     * @ORM\Column(name="wpresta_dossier", type="integer", nullable=true)
     */
    private $wprestaDossier;

    /**
     * @var int
     *
     * @ORM\Column(name="wpresta_contrat", type="integer", nullable=false)
     */
    private $wprestaContrat;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="wpresta_prestataire", type="boolean", nullable=true)
     */
    private $wprestaPrestataire;

    /**
     * @var string|null
     *
     * @ORM\Column(name="wpresta_prestataire_label", type="string", length=255, nullable=true)
     */
    private $wprestaPrestataireLabel;
    
    /**
     * @var string|null
     *
     * @ORM\Column(name="wpresta_prestataire_label2", type="string", length=255, nullable=true)
     */
    private $wprestaPrestataireLabel2;

    public function getWprestaId(): ?int
    {
        return $this->wprestaId;
    }

    public function getWprestaStamp(): ?\DateTimeInterface
    {
        return $this->wprestaStamp;
    }
    
    public function setWprestaStamp(\DateTimeInterface $wprestaStamp): self
    {
        $this->wprestaStamp = $wprestaStamp;
    
        return $this;
    }
    
    public function getWprestaDeclar(): ?int
    {
        return $this->wprestaDeclar;
    }

    public function setWprestaDeclar(?int $wprestaDeclar): self
    {
        $this->wprestaDeclar = $wprestaDeclar;

        return $this;
    }

    public function getWprestaDossier(): ?int
    {
        return $this->wprestaDossier;
    }

    public function setWprestaDossier(?int $wprestaDossier): self
    {
        $this->wprestaDossier = $wprestaDossier;

        return $this;
    }

    public function getWprestaContrat(): ?int
    {
        return $this->wprestaContrat;
    }

    public function setWprestaContrat(int $wprestaContrat): self
    {
        $this->wprestaContrat = $wprestaContrat;

        return $this;
    }

    public function getWprestaPrestataire(): ?bool
    {
        return $this->wprestaPrestataire;
    }

    public function setWprestaPrestataire(?bool $wprestaPrestataire): self
    {
        $this->wprestaPrestataire = $wprestaPrestataire;

        return $this;
    }

    public function getWprestaPrestataireLabel(): ?string
    {
        return $this->wprestaPrestataireLabel;
    }

    public function setWprestaPrestataireLabel(?string $wprestaPrestataireLabel): self
    {
        $this->wprestaPrestataireLabel = $wprestaPrestataireLabel;

        return $this;
    }
    
    public function getWprestaPrestataireLabel2(): ?string
    {
        return $this->wprestaPrestataireLabel2;
    }
    
    public function setWprestaPrestataireLabel2(?string $wprestaPrestataireLabel2): self
    {
        $this->wprestaPrestataireLabel2 = $wprestaPrestataireLabel2;
    
        return $this;
    }

}
