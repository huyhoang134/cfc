<?php

namespace App\Entity;

use App\Repository\WcontratRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=WcontratRepository::class)
 */
class Wcontrat
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", nullable=false)
     */
    private $wctContrat;

    /**
     * @ORM\Column(type="datetime")
     */
    private $wctStamp;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $wctSynchro;

    /**
     * @ORM\Column(type="integer")
     */
    private $wctDossier;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $wctTypcont;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $wctNomResp;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $wctTitResp;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $wctFctResp;

    /**
     * @ORM\Column(type="string", length=80, nullable=true)
     */
    private $wctMelResp;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $wctNomSecGen;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $wctTitSecGen;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $wctFctSecGen;

    /**
     * @ORM\Column(type="string", length=80, nullable=true)
     */
    private $wctMelSecGen;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $wctTelResp;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $wctFaxResp;

    /**
     * @ORM\Column(type="smallint")
     */
    private $wctIsChorusPro;

    /**
     * @ORM\Column(type="string", length=14, nullable=true)
     */
    private $wctSiret;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $wctServiceCode;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $wctOrderNumber;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $wctNomFact;

    /**
     * @ORM\Column(type="string", length=35, nullable=true)
     */
    private $wctTitFact;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $wctFctFact;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $wctRaisoc1Fact;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $wctAdresse1Fact;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $wctAdresse2Fact;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $wctAdresse3Fact;

    /**
     * @ORM\Column(type="string", length=16, nullable=true)
     */
    private $wctCoposFact;

    /**
     * @ORM\Column(type="string", length=42, nullable=true)
     */
    private $wctVilleFact;

    /**
     * @ORM\Column(type="string", length=26, nullable=true)
     */
    private $wctTelFact;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wctIdCtFact;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wctIdCtNomSecGen;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wctIdCtResp;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wctIdAdrFact;

    /**
     * @ORM\Column(type="string", length=8, nullable=true)
     */
    private $wctPassword;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $wctIsOrderNumber;

    /**
     * @ORM\Column(type="string", length=80, nullable=true)
     */
    private $wctMailFact;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wctIdCtNomSecGenQuestion;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wctIdCtRespQuestion;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wctIdCtFactQuestion;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wctIdAdrFactQuestion;

    /**
     * @ORM\Column(name="FkSecGenInit", type="integer", nullable=true)
     */
    private $fkSecGenInit;

    /**
     * @ORM\Column(name="FkIdRespInit", type="integer", nullable=true)
     */
    private $fkIdRespInit;

    /**
     * @ORM\Column(name="FkIdCtFactInit", type="integer", nullable=true)
     */
    private $fkIdCtFactInit;

    /**
     * @ORM\Column(name="FkAdrFactInit", type="integer", nullable=true)
     */
    private $fkadrfactinit;

    /**
     * @ORM\Column(name="FkUserDea", type="string", length=3, nullable=true)
     */
    private $fkUserDea;

    /**
     * @ORM\Column(name="FkFacture", type="integer", nullable=true)
     */
    private $fkFacture;

    /**
     * @ORM\Column(name="CodeTypeContrat", type="string", length=2, nullable=true)
     */
    private $codeTypeContrat;

    /**
     * @ORM\Column(name="FkFactType", type="integer", nullable=true)
     */
    private $fkFactType;

    /**
     * @ORM\Column(name="CoordonneesAJour", type="boolean")
     */
    private $coordonneesAJour;

    /**
     * @ORM\Column(name="RaisSocFact1", type="string", length=50, nullable=true)
     */
    private $raisSocFact1;

    /**
     * @ORM\Column(name="RaisSocFact2", type="string", length=50, nullable=true)
     */
    private $raisSocFact2;

    public function getWctContrat(): ?int
    {
        return $this->wctContrat;
    }

    public function getWctStamp(): ?\DateTimeInterface
    {
        return $this->wctStamp;
    }

    public function setWctStamp(\DateTimeInterface $wctStamp): self
    {
        $this->wctStamp = $wctStamp;

        return $this;
    }

    public function getWctSynchro(): ?\DateTimeInterface
    {
        return $this->wctSynchro;
    }

    public function setWctSynchro(?\DateTimeInterface $wctSynchro): self
    {
        $this->wctSynchro = $wctSynchro;

        return $this;
    }

    public function getWctDossier(): ?int
    {
        return $this->wctDossier;
    }

    public function setWctDossier(int $wctDossier): self
    {
        $this->wctDossier = $wctDossier;

        return $this;
    }

    public function getWctTypcont(): ?string
    {
        return $this->wctTypcont;
    }

    public function setWctTypcont(string $wctTypcont): self
    {
        $this->wctTypcont = $wctTypcont;

        return $this;
    }

    public function getWctNomResp(): ?string
    {
        return $this->wctNomResp;
    }

    public function setWctNomResp(?string $wctNomResp): self
    {
        $this->wctNomResp = $wctNomResp;

        return $this;
    }

    public function getWctTitResp(): ?string
    {
        return $this->wctTitResp;
    }

    public function setWctTitResp(?string $wctTitResp): self
    {
        $this->wctTitResp = $wctTitResp;

        return $this;
    }

    public function getWctFctResp(): ?string
    {
        return $this->wctFctResp;
    }

    public function setWctFctResp(?string $wctFctResp): self
    {
        $this->wctFctResp = $wctFctResp;

        return $this;
    }

    public function getWctMelResp(): ?string
    {
        return $this->wctMelResp;
    }

    public function setWctMelResp(?string $wctMelResp): self
    {
        $this->wctMelResp = $wctMelResp;

        return $this;
    }

    public function getWctNomSecGen(): ?string
    {
        return $this->wctNomSecGen;
    }

    public function setWctNomSecGen(?string $wctNomSecGen): self
    {
        $this->wctNomSecGen = $wctNomSecGen;

        return $this;
    }

    public function getWctTitSecGen(): ?string
    {
        return $this->wctTitSecGen;
    }

    public function setWctTitSecGen(?string $wctTitSecGen): self
    {
        $this->wctTitSecGen = $wctTitSecGen;

        return $this;
    }

    public function getWctFctSecGen(): ?string
    {
        return $this->wctFctSecGen;
    }

    public function setWctFctSecGen(?string $wctFctSecGen): self
    {
        $this->wctFctSecGen = $wctFctSecGen;

        return $this;
    }

    public function getWctMelSecGen(): ?string
    {
        return $this->wctMelSecGen;
    }

    public function setWctMelSecGen(?string $wctMelSecGen): self
    {
        $this->wctMelSecGen = $wctMelSecGen;

        return $this;
    }

    public function getWctTelResp(): ?string
    {
        return $this->wctTelResp;
    }

    public function setWctTelResp(?string $wctTelResp): self
    {
        $this->wctTelResp = $wctTelResp;

        return $this;
    }

    public function getWctFaxResp(): ?string
    {
        return $this->wctFaxResp;
    }

    public function setWctFaxResp(?string $wctFaxResp): self
    {
        $this->wctFaxResp = $wctFaxResp;

        return $this;
    }

    public function getWctIsChorusPro(): ?int
    {
        return $this->wctIsChorusPro;
    }

    public function setWctIsChorusPro(int $wctIsChorusPro): self
    {
        $this->wctIsChorusPro = $wctIsChorusPro;

        return $this;
    }

    public function getWctSiret(): ?string
    {
        return $this->wctSiret;
    }

    public function setWctSiret(?string $wctSiret): self
    {
        $this->wctSiret = $wctSiret;

        return $this;
    }

    public function getWctServiceCode(): ?string
    {
        return $this->wctServiceCode;
    }

    public function setWctServiceCode(string $wctServiceCode): self
    {
        $this->wctServiceCode = $wctServiceCode;

        return $this;
    }

    public function getWctOrderNumber(): ?string
    {
        return $this->wctOrderNumber;
    }

    public function setWctOrderNumber(?string $wctOrderNumber): self
    {
        $this->wctOrderNumber = $wctOrderNumber;

        return $this;
    }

    public function getWctNomFact(): ?string
    {
        return $this->wctNomFact;
    }

    public function setWctNomFact(?string $wctNomFact): self
    {
        $this->wctNomFact = $wctNomFact;

        return $this;
    }

    public function getWctTitFact(): ?string
    {
        return $this->wctTitFact;
    }

    public function setWctTitFact(?string $wctTitFact): self
    {
        $this->wctTitFact = $wctTitFact;

        return $this;
    }

    public function getWctFctFact(): ?string
    {
        return $this->wctFctFact;
    }

    public function setWctFctFact(?string $wctFctFact): self
    {
        $this->wctFctFact = $wctFctFact;

        return $this;
    }

    public function getWctRaisoc1Fact(): ?string
    {
        return $this->wctRaisoc1Fact;
    }

    public function setWctRaisoc1Fact(?string $wctRaisoc1Fact): self
    {
        $this->wctRaisoc1Fact = $wctRaisoc1Fact;

        return $this;
    }

    public function getWctAdresse1Fact(): ?string
    {
        return $this->wctAdresse1Fact;
    }

    public function setWctAdresse1Fact(?string $wctAdresse1Fact): self
    {
        $this->wctAdresse1Fact = $wctAdresse1Fact;

        return $this;
    }

    public function getWctAdresse2Fact(): ?string
    {
        return $this->wctAdresse2Fact;
    }

    public function setWctAdresse2Fact(?string $wctAdresse2Fact): self
    {
        $this->wctAdresse2Fact = $wctAdresse2Fact;

        return $this;
    }

    public function getWctAdresse3Fact(): ?string
    {
        return $this->wctAdresse3Fact;
    }

    public function setWctAdresse3Fact(?string $wctAdresse3Fact): self
    {
        $this->wctAdresse3Fact = $wctAdresse3Fact;

        return $this;
    }

    public function getWctCoposFact(): ?string
    {
        return $this->wctCoposFact;
    }

    public function setWctCoposFact(?string $wctCoposFact): self
    {
        $this->wctCoposFact = $wctCoposFact;

        return $this;
    }

    public function getWctVilleFact(): ?string
    {
        return $this->wctVilleFact;
    }

    public function setWctVilleFact(?string $wctVilleFact): self
    {
        $this->wctVilleFact = $wctVilleFact;

        return $this;
    }

    public function getWctTelFact(): ?string
    {
        return $this->wctTelFact;
    }

    public function setWctTelFact(?string $wctTelFact): self
    {
        $this->wctTelFact = $wctTelFact;

        return $this;
    }

    public function getWctIdCtFact(): ?int
    {
        return $this->wctIdCtFact;
    }

    public function setWctIdCtFact(?int $wctIdCtFact): self
    {
        $this->wctIdCtFact = $wctIdCtFact;

        return $this;
    }

    public function getWctIdCtNomSecGen(): ?int
    {
        return $this->wctIdCtNomSecGen;
    }

    public function setWctIdCtNomSecGen(?int $wctIdCtNomSecGen): self
    {
        $this->wctIdCtNomSecGen = $wctIdCtNomSecGen;

        return $this;
    }

    public function getWctIdCtResp(): ?int
    {
        return $this->wctIdCtResp;
    }

    public function setWctIdCtResp(?int $wctIdCtResp): self
    {
        $this->wctIdCtResp = $wctIdCtResp;

        return $this;
    }

    public function getWctIdAdrFact(): ?int
    {
        return $this->wctIdAdrFact;
    }

    public function setWctIdAdrFact(?int $wctIdAdrFact): self
    {
        $this->wctIdAdrFact = $wctIdAdrFact;

        return $this;
    }

    public function getWctPassword(): ?string
    {
        return $this->wctPassword;
    }

    public function setWctPassword(?string $wctPassword): self
    {
        $this->wctPassword = $wctPassword;

        return $this;
    }

    public function getWctIsOrderNumber(): ?int
    {
        return $this->wctIsOrderNumber;
    }

    public function setWctIsOrderNumber(?int $wctIsOrderNumber): self
    {
        $this->wctIsOrderNumber = $wctIsOrderNumber;

        return $this;
    }

    public function getWctMailFact(): ?string
    {
        return $this->wctMailFact;
    }

    public function setWctMailFact(?string $wctMailFact): self
    {
        $this->wctMailFact = $wctMailFact;

        return $this;
    }

    public function getWctIdCtNomSecGenQuestion(): ?int
    {
        return $this->wctIdCtNomSecGenQuestion;
    }

    public function setWctIdCtNomSecGenQuestion(?int $wctIdCtNomSecGenQuestion): self
    {
        $this->wctIdCtNomSecGenQuestion = $wctIdCtNomSecGenQuestion;

        return $this;
    }

    public function getWctIdCtRespQuestion(): ?int
    {
        return $this->wctIdCtRespQuestion;
    }

    public function setWctIdCtRespQuestion(?int $wctIdCtRespQuestion): self
    {
        $this->wctIdCtRespQuestion = $wctIdCtRespQuestion;

        return $this;
    }

    public function getWctIdCtFactQuestion(): ?int
    {
        return $this->wctIdCtFactQuestion;
    }

    public function setWctIdCtFactQuestion(?int $wctIdCtFactQuestion): self
    {
        $this->wctIdCtFactQuestion = $wctIdCtFactQuestion;

        return $this;
    }

    public function getWctIdAdrFactQuestion(): ?int
    {
        return $this->wctIdAdrFactQuestion;
    }

    public function setWctIdAdrFactQuestion(?int $wctIdAdrFactQuestion): self
    {
        $this->wctIdAdrFactQuestion = $wctIdAdrFactQuestion;

        return $this;
    }

    public function getFkSecGenInit(): ?int
    {
        return $this->fkSecGenInit;
    }

    public function setFkSecGenInit(?int $fkSecGenInit): self
    {
        $this->fkSecGenInit = $fkSecGenInit;

        return $this;
    }

    public function getFkIdRespInit(): ?int
    {
        return $this->fkIdRespInit;
    }

    public function setFkIdRespInit(?int $fkIdRespInit): self
    {
        $this->fkIdRespInit = $fkIdRespInit;

        return $this;
    }

    public function getFkIdCtFactInit(): ?int
    {
        return $this->fkIdCtFactInit;
    }

    public function setFkIdCtFactInit(?int $fkIdCtFactInit): self
    {
        $this->fkIdCtFactInit = $fkIdCtFactInit;

        return $this;
    }

    public function getFkAdrFactInit(): ?int
    {
        return $this->fkadrfactinit;
    }

    public function setFkAdrFactInit(?int $fkAdrFactInit): self
    {
        $this->fkAdrFactInit = $fkAdrFactInit;

        return $this;
    }

    public function getFkUserDea(): ?string
    {
        return $this->fkUserDea;
    }

    public function setFkUserDea(?string $fkUserDea): self
    {
        $this->fkUserDea = $fkUserDea;

        return $this;
    }

    public function getFkFacture(): ?int
    {
        return $this->fkFacture;
    }

    public function setFkFacture(?int $fkFacture): self
    {
        $this->fkFacture = $fkFacture;

        return $this;
    }

    public function getCodeTypeContrat(): ?string
    {
        return $this->codeTypeContrat;
    }

    public function setCodeTypeContrat(?string $codeTypeContrat): self
    {
        $this->codeTypeContrat = $codeTypeContrat;

        return $this;
    }

    public function getFkFactType(): ?int
    {
        return $this->fkFactType;
    }

    public function setFkFactType(?int $fkFactType): self
    {
        $this->fkFactType = $fkFactType;

        return $this;
    }

    public function getCoordonneesAJour(): ?bool
    {
        return $this->coordonneesAJour;
    }

    public function setCoordonneesAJour(bool $coordonneesAJour): self
    {
        $this->coordonneesAJour = $coordonneesAJour;

        return $this;
    }

    public function getRaisSocFact1(): ?string
    {
        return $this->raisSocFact1;
    }

    public function setRaisSocFact1(?string $raisSocFact1): self
    {
        $this->raisSocFact1 = $raisSocFact1;

        return $this;
    }

    public function getRaisSocFact2(): ?string
    {
        return $this->raisSocFact2;
    }

    public function setRaisSocFact2(?string $raisSocFact2): self
    {
        $this->raisSocFact2 = $raisSocFact2;

        return $this;
    }
}
