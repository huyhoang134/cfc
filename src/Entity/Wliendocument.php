<?php

namespace App\Entity;

use App\Repository\WliendocumentRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=WliendocumentRepository::class)
 */
class Wliendocument
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(name="PkLienDocument", type="integer", nullable=false)
     */
    private $pkLienDocument;

    /**
     * @ORM\Column(name="FkIndexDocument", type="integer")
     */
    private $fkIndexDocument;

    /**
     * @ORM\Column(name="NomTable", type="string", length=50)
     */
    private $nomTable;

    /**
     * @ORM\Column(name="ClefTable", type="integer")
     */
    private $clefTable;

    /**
     * @ORM\Column(name="Stamp", type="datetime")
     */
    private $stamp;

    /**
     * @ORM\Column(name="Synchro", type="datetime", nullable=true)
     */
    private $synchro;

    public function getPkLienDocument(): ?int
    {
        return $this->pkLienDocument;
    }

    public function getFkIndexDocument(): ?int
    {
        return $this->fkIndexDocument;
    }

    public function setFkIndexDocument(int $fkIndexDocument): self
    {
        $this->fkIndexDocument = $fkIndexDocument;

        return $this;
    }

    public function getNomTable(): ?string
    {
        return $this->nomTable;
    }

    public function setNomTable(string $nomTable): self
    {
        $this->nomTable = $nomTable;

        return $this;
    }

    public function getClefTable(): ?int
    {
        return $this->clefTable;
    }

    public function setClefTable(int $clefTable): self
    {
        $this->clefTable = $clefTable;

        return $this;
    }

    public function getStamp(): ?\DateTimeInterface
    {
        return $this->stamp;
    }

    public function setStamp(\DateTimeInterface $stamp): self
    {
        $this->stamp = $stamp;

        return $this;
    }

    public function getSynchro(): ?\DateTimeInterface
    {
        return $this->synchro;
    }

    public function setSynchro(?\DateTimeInterface $synchro): self
    {
        $this->synchro = $synchro;

        return $this;
    }
}
