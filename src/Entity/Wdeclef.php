<?php

namespace App\Entity;

use App\Repository\WdeclefRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=WdeclefRepository::class)
 */
class Wdeclef
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", nullable=false)
     */
    private $wdfDeclar;

    /**
     * @ORM\Column(type="datetime")
     */
    private $wdfStamp;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $wdfSynchro;

    /**
     * @ORM\Column(type="integer")
     */
    private $wdfDossier;

    /**
     * @ORM\Column(type="integer")
     */
    private $wdfContrat;

    /**
     * @ORM\Column(type="integer")
     */
    private $wdfAnnee;

    /**
     * @ORM\Column(type="string", length=9)
     */
    private $wdfAnneeDecl;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wdfEleves;

    /**
     * @ORM\Column(type="string", length=3, nullable=true)
     */
    private $wdfTranche;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wdfCopieurs;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wdfProfs;

    /**
     * @ORM\Column(type="string", length=1)
     */
    private $wdfEtatDeclar;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $wdfDateValidation;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $wdfEmailSent;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wdfEffectif;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wdfTranche01;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wdfTranche02;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wdfTranche03;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wdfTranche04;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wdfTranche05;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wdfTranche06;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wdfTranche07;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $wdfIntituleDecl;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wdfTranche08;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wdfTranche09;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wdfTranche10;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wdfTranche11;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wdfTranche12;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wdfTranche13;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wdfTranche14;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wdfTranche15;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wdfTranche16;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wdfEffectif2;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wdfEffectif3;

    /**
     * @ORM\Column(name="LibelleTarif", type="string", length=60, nullable=true)
     */
    private $libelleTarif;

    /**
     * @ORM\Column(name="FkProforma", type="integer", nullable=true)
     */
    private $fkproforma;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wdfEffectif4;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wdfTranche17;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wdfTranche18;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wdfTranche19;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wdfTranche20;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wdfTranche21;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wdfTranche22;

    /**
     * @ORM\Column(name="MotifAttente", type="string", length=3, nullable=true)
     */
    private $motifAttente;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $wdfNumperiod;

    public function getWdfDeclar(): ?int
    {
        return $this->wdfDeclar;
    }

    public function getWdfStamp(): ?\DateTimeInterface
    {
        return $this->wdfStamp;
    }

    public function setWdfStamp(\DateTimeInterface $wdfStamp): self
    {
        $this->wdfStamp = $wdfStamp;

        return $this;
    }

    public function getWdfSynchro(): ?\DateTimeInterface
    {
        return $this->wdfSynchro;
    }

    public function setWdfSynchro(?\DateTimeInterface $wdfSynchro): self
    {
        $this->wdfSynchro = $wdfSynchro;

        return $this;
    }

    public function getWdfDossier(): ?int
    {
        return $this->wdfDossier;
    }

    public function setWdfDossier(int $wdfDossier): self
    {
        $this->wdfDossier = $wdfDossier;

        return $this;
    }

    public function getWdfContrat(): ?int
    {
        return $this->wdfContrat;
    }

    public function setWdfContrat(int $wdfContrat): self
    {
        $this->wdfContrat = $wdfContrat;

        return $this;
    }

    public function getWdfAnnee(): ?int
    {
        return $this->wdfAnnee;
    }

    public function setWdfAnnee(int $wdfAnnee): self
    {
        $this->wdfAnnee = $wdfAnnee;

        return $this;
    }

    public function getWdfAnneeDecl(): ?string
    {
        return $this->wdfAnneeDecl;
    }

    public function setWdfAnneeDecl(string $wdfAnneeDecl): self
    {
        $this->wdfAnneeDecl = $wdfAnneeDecl;

        return $this;
    }

    public function getWdfEleves(): ?int
    {
        return $this->wdfEleves;
    }

    public function setWdfEleves(?int $wdfEleves): self
    {
        $this->wdfEleves = $wdfEleves;

        return $this;
    }

    public function getWdfTranche(): ?string
    {
        return $this->wdfTranche;
    }

    public function setWdfTranche(?string $wdfTranche): self
    {
        $this->wdfTranche = $wdfTranche;

        return $this;
    }

    public function getWdfCopieurs(): ?int
    {
        return $this->wdfCopieurs;
    }

    public function setWdfCopieurs(?int $wdfCopieurs): self
    {
        $this->wdfCopieurs = $wdfCopieurs;

        return $this;
    }

    public function getWdfProfs(): ?int
    {
        return $this->wdfProfs;
    }

    public function setWdfProfs(?int $wdfProfs): self
    {
        $this->wdfProfs = $wdfProfs;

        return $this;
    }

    public function getWdfEtatDeclar(): ?string
    {
        return $this->wdfEtatDeclar;
    }

    public function setWdfEtatDeclar(string $wdfEtatDeclar): self
    {
        $this->wdfEtatDeclar = $wdfEtatDeclar;

        return $this;
    }

    public function getWdfDateValidation(): ?\DateTimeInterface
    {
        return $this->wdfDateValidation;
    }

    public function setWdfDateValidation(?\DateTimeInterface $wdfDateValidation): self
    {
        $this->wdfDateValidation = $wdfDateValidation;

        return $this;
    }

    public function getWdfEmailSent(): ?int
    {
        return $this->wdfEmailSent;
    }

    public function setWdfEmailSent(?int $wdfEmailSent): self
    {
        $this->wdfEmailSent = $wdfEmailSent;

        return $this;
    }

    public function getWdfEffectif(): ?int
    {
        return $this->wdfEffectif;
    }

    public function setWdfEffectif(?int $wdfEffectif): self
    {
        $this->wdfEffectif = $wdfEffectif;

        return $this;
    }

    public function getWdfTranche01(): ?int
    {
        return $this->wdfTranche01;
    }

    public function setWdfTranche01(?int $wdfTranche01): self
    {
        $this->wdfTranche01 = $wdfTranche01;

        return $this;
    }

    public function getWdfTranche02(): ?int
    {
        return $this->wdfTranche02;
    }

    public function setWdfTranche02(?int $wdfTranche02): self
    {
        $this->wdfTranche02 = $wdfTranche02;

        return $this;
    }

    public function getWdfTranche03(): ?int
    {
        return $this->wdfTranche03;
    }

    public function setWdfTranche03(?int $wdfTranche03): self
    {
        $this->wdfTranche03 = $wdfTranche03;

        return $this;
    }

    public function getWdfTranche04(): ?int
    {
        return $this->wdfTranche04;
    }

    public function setWdfTranche04(?int $wdfTranche04): self
    {
        $this->wdfTranche04 = $wdfTranche04;

        return $this;
    }

    public function getWdfTranche05(): ?int
    {
        return $this->wdfTranche05;
    }

    public function setWdfTranche05(?int $wdfTranche05): self
    {
        $this->wdfTranche05 = $wdfTranche05;

        return $this;
    }

    public function getWdfTranche06(): ?int
    {
        return $this->wdfTranche06;
    }

    public function setWdfTranche06(?int $wdfTranche06): self
    {
        $this->wdfTranche06 = $wdfTranche06;

        return $this;
    }

    public function getWdfTranche07(): ?int
    {
        return $this->wdfTranche07;
    }

    public function setWdfTranche07(?int $wdfTranche07): self
    {
        $this->wdfTranche07 = $wdfTranche07;

        return $this;
    }

    public function getWdfIntituleDecl(): ?string
    {
        return $this->wdfIntituleDecl;
    }

    public function setWdfIntituleDecl(?string $wdfIntituleDecl): self
    {
        $this->wdfIntituleDecl = $wdfIntituleDecl;

        return $this;
    }

    public function getWdfTranche08(): ?int
    {
        return $this->wdfTranche08;
    }

    public function setWdfTranche08(?int $wdfTranche08): self
    {
        $this->wdfTranche08 = $wdfTranche08;

        return $this;
    }

    public function getWdfTranche09(): ?int
    {
        return $this->wdfTranche09;
    }

    public function setWdfTranche09(?int $wdfTranche09): self
    {
        $this->wdfTranche09 = $wdfTranche09;

        return $this;
    }

    public function getWdfTranche10(): ?int
    {
        return $this->wdfTranche10;
    }

    public function setWdfTranche10(?int $wdfTranche10): self
    {
        $this->wdfTranche10 = $wdfTranche10;

        return $this;
    }

    public function getWdfTranche11(): ?int
    {
        return $this->wdfTranche11;
    }

    public function setWdfTranche11(?int $wdfTranche11): self
    {
        $this->wdfTranche11 = $wdfTranche11;

        return $this;
    }

    public function getWdfTranche12(): ?int
    {
        return $this->wdfTranche12;
    }

    public function setWdfTranche12(?int $wdfTranche12): self
    {
        $this->wdfTranche12 = $wdfTranche12;

        return $this;
    }

    public function getWdfTranche13(): ?int
    {
        return $this->wdfTranche13;
    }

    public function setWdfTranche13(?int $wdfTranche13): self
    {
        $this->wdfTranche13 = $wdfTranche13;

        return $this;
    }

    public function getWdfTranche14(): ?int
    {
        return $this->wdfTranche14;
    }

    public function setWdfTranche14(?int $wdfTranche14): self
    {
        $this->wdfTranche14 = $wdfTranche14;

        return $this;
    }

    public function getWdfTranche15(): ?int
    {
        return $this->wdfTranche15;
    }

    public function setWdfTranche15(?int $wdfTranche15): self
    {
        $this->wdfTranche15 = $wdfTranche15;

        return $this;
    }

    public function getWdfTranche16(): ?int
    {
        return $this->wdfTranche16;
    }

    public function setWdfTranche16(?int $wdfTranche16): self
    {
        $this->wdfTranche16 = $wdfTranche16;

        return $this;
    }

    public function getWdfEffectif2(): ?int
    {
        return $this->wdfEffectif2;
    }

    public function setWdfEffectif2(?int $wdfEffectif2): self
    {
        $this->wdfEffectif2 = $wdfEffectif2;

        return $this;
    }

    public function getWdfEffectif3(): ?int
    {
        return $this->wdfEffectif3;
    }

    public function setWdfEffectif3(?int $wdfEffectif3): self
    {
        $this->wdfEffectif3 = $wdfEffectif3;

        return $this;
    }

    public function getLibelleTarif(): ?string
    {
        return $this->libelleTarif;
    }

    public function setLibelleTarif(?string $libelleTarif): self
    {
        $this->libelleTarif = $libelleTarif;

        return $this;
    }

    public function getFkproforma(): ?int
    {
        return $this->fkproforma;
    }

    public function setFkproforma(?int $fkproforma): self
    {
        $this->fkproforma = $fkproforma;

        return $this;
    }

    public function getWdfEffectif4(): ?int
    {
        return $this->wdfEffectif4;
    }

    public function setWdfEffectif4(?int $wdfEffectif4): self
    {
        $this->wdfEffectif4 = $wdfEffectif4;

        return $this;
    }

    public function getWdfTranche17(): ?int
    {
        return $this->wdfTranche17;
    }

    public function setWdfTranche17(?int $wdfTranche17): self
    {
        $this->wdfTranche17 = $wdfTranche17;

        return $this;
    }

    public function getWdfTranche18(): ?int
    {
        return $this->wdfTranche18;
    }

    public function setWdfTranche18(?int $wdfTranche18): self
    {
        $this->wdfTranche18 = $wdfTranche18;

        return $this;
    }

    public function getWdfTranche19(): ?int
    {
        return $this->wdfTranche19;
    }

    public function setWdfTranche19(?int $wdfTranche19): self
    {
        $this->wdfTranche19 = $wdfTranche19;

        return $this;
    }

    public function getWdfTranche20(): ?int
    {
        return $this->wdfTranche20;
    }

    public function setWdfTranche20(?int $wdfTranche20): self
    {
        $this->wdfTranche20 = $wdfTranche20;

        return $this;
    }

    public function getWdfTranche21(): ?int
    {
        return $this->wdfTranche21;
    }

    public function setWdfTranche21(?int $wdfTranche21): self
    {
        $this->wdfTranche21 = $wdfTranche21;

        return $this;
    }

    public function getWdfTranche22(): ?int
    {
        return $this->wdfTranche22;
    }

    public function setWdfTranche22(?int $wdfTranche22): self
    {
        $this->wdfTranche22 = $wdfTranche22;

        return $this;
    }

    public function getMotifAttente(): ?string
    {
        return $this->motifAttente;
    }

    public function setMotifAttente(?string $motifAttente): self
    {
        $this->motifAttente = $motifAttente;

        return $this;
    }

    public function getWdfNumperiod(): ?string
    {
        return $this->wdfNumperiod;
    }

    public function setWdfNumperiod(?string $wdfNumperiod): self
    {
        $this->wdfNumperiod = $wdfNumperiod;

        return $this;
    }
}
