<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * WrelationPublique
 *
 * @ORM\Table(name="wrelation_publique")
 * @ORM\Entity
 */
class WrelationPublique
{
    /**
     * @var int
     *
     * @ORM\Column(name="wrp_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $wrpId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="wrp_stamp", type="datetime", nullable=false)
     */
    private $wrpStamp;

    /**
     * @var int
     *
     * @ORM\Column(name="wrp_declar", type="integer", nullable=false)
     */
    private $wrpDeclar;

    /**
     * @var int
     *
     * @ORM\Column(name="wrp_dossier", type="integer", nullable=false)
     */
    private $wrpDossier;

    /**
     * @var int
     *
     * @ORM\Column(name="wrp_contrat", type="integer", nullable=false)
     */
    private $wrpContrat;

    /**
     * @var string|null
     *
     * @ORM\Column(name="wrp_label", type="string", length=255, nullable=true)
     */
    private $wrpLabel;

    /**
     * @var string|null
     *
     * @ORM\Column(name="wrp_adresse", type="string", length=255, nullable=true)
     */
    private $wrpAdresse;

    /**
     * @var string|null
     *
     * @ORM\Column(name="wrp_cp", type="string", length=255, nullable=true)
     */
    private $wrpCp;

    /**
     * @var string|null
     *
     * @ORM\Column(name="wrp_ville", type="string", length=255, nullable=true)
     */
    private $wrpVille;

    /**
     * @var int|null
     *
     * @ORM\Column(name="wrp_volume", type="integer", nullable=true)
     */
    private $wrpVolume;
    
    /**
     * @var string|null
     *
     * @ORM\Column(name="wrp_telephone", type="string", length=255, nullable=true)
     */
    private $wrpTelephone;
    
    /**
     * @var string|null
     *
     * @ORM\Column(name="wrp_email", type="string", length=255, nullable=true)
     */
    private $wrpEmail;

    public function getWrpId(): ?int
    {
        return $this->wrpId;
    }

    public function getWrpStamp(): ?\DateTimeInterface
    {
        return $this->wrpStamp;
    }

    public function setWrpStamp(\DateTimeInterface $wrpStamp): self
    {
        $this->wrpStamp = $wrpStamp;

        return $this;
    }

    public function getWrpDeclar(): ?int
    {
        return $this->wrpDeclar;
    }

    public function setWrpDeclar(int $wrpDeclar): self
    {
        $this->wrpDeclar = $wrpDeclar;

        return $this;
    }

    public function getWrpDossier(): ?int
    {
        return $this->wrpDossier;
    }

    public function setWrpDossier(int $wrpDossier): self
    {
        $this->wrpDossier = $wrpDossier;

        return $this;
    }

    public function getWrpContrat(): ?int
    {
        return $this->wrpContrat;
    }

    public function setWrpContrat(int $wrpContrat): self
    {
        $this->wrpContrat = $wrpContrat;

        return $this;
    }

    public function getWrpLabel(): ?string
    {
        return $this->wrpLabel;
    }

    public function setWrpLabel(?string $wrpLabel): self
    {
        $this->wrpLabel = $wrpLabel;

        return $this;
    }

    public function getWrpAdresse(): ?string
    {
        return $this->wrpAdresse;
    }

    public function setWrpAdresse(?string $wrpAdresse): self
    {
        $this->wrpAdresse = $wrpAdresse;

        return $this;
    }

    public function getWrpCp(): ?string
    {
        return $this->wrpCp;
    }

    public function setWrpCp(?string $wrpCp): self
    {
        $this->wrpCp = $wrpCp;

        return $this;
    }

    public function getWrpVille(): ?string
    {
        return $this->wrpVille;
    }

    public function setWrpVille(?string $wrpVille): self
    {
        $this->wrpVille = $wrpVille;

        return $this;
    }

    public function getWrpVolume(): ?int
    {
        return $this->wrpVolume;
    }

    public function setWrpVolume(?int $wrpVolume): self
    {
        $this->wrpVolume = $wrpVolume;

        return $this;
    }

    public function getWrpTelephone(): ?string
    {
        return $this->wrpTelephone;
    }
    
    public function setWrpTelephone(?string $wrpTelephone): self
    {
        $this->wrpTelephone = $wrpTelephone;
    
        return $this;
    }
    
    public function getWrpEmail(): ?string
    {
        return $this->wrpEmail;
    }
    
    public function setWrpEmail(?string $wrpEmail): self
    {
        $this->wrpEmail = $wrpEmail;
    
        return $this;
    }

}
