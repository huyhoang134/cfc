<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * WpanoramapressListe
 *
 * @ORM\Table(name="wpanoramapress_liste", indexes={@ORM\Index(name="fk_media_panorama_press", columns={"wppl_fichier"}), @ORM\Index(name="fk_wpanoramapress_liste_wpanoramapress1_idx", columns={"wppl_wpp_id"})})
 * @ORM\Entity
 */
class WpanoramapressListe
{
    /**
     * @var int
     *
     * @ORM\Column(name="wppl_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $wpplId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="wppl_stamp", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $wpplStamp = 'CURRENT_TIMESTAMP';
    
    /**
     * @var int|null
     *
     * @ORM\Column(name="wppl_declar", type="integer", nullable=true)
     */
    private $wpplDeclar;
    
    /**
     * @var int|null
     *
     * @ORM\Column(name="wppl_dossier", type="integer", nullable=true)
     */
    private $wpplDossier;
    
    /**
     * @var int|null
     *
     * @ORM\Column(name="wppl_contrat", type="integer", nullable=true)
     */
    private $wpplContrat;
    
    /**
     * @var string|null
     *
     * @ORM\Column(name="wppl_label", type="string", length=255, nullable=true)
     */
    private $wpplLabel;

    /**
     * @var string|null
     *
     * @ORM\Column(name="wppl_periode", type="string", length=255, nullable=true)
     */
    private $wpplPeriode;
    
    /**
     * @var int|null
     *
     * @ORM\Column(name="wppl_periode_nombre", type="integer", nullable=true)
     */
    private $wpplPeriodeNombre;
    
    /**
     * @var int|null
     *
     * @ORM\Column(name="wppl_moyenne_repro", type="integer", nullable=true)
     */
    private $wpplMoyenneRepro;
    
    /**
     * @var int|null
     *
     * @ORM\Column(name="wppl_moyenne_exemplaire", type="integer", nullable=true)
     */
    private $wpplMoyenneExemplaire;
    
    /**
     * @var int|null
     *
     * @ORM\Column(name="wppl_total", type="integer", nullable=true)
     */
    private $wpplTotal;

    /**
     * @var int|null
     *
     * @ORM\Column(name="wppl_is_chorus_pro", type="integer", nullable=true)
     */
    private $wpplIsChorusPro;

    /**
     * @var string|null
     *
     * @ORM\Column(name="wppl_siret", type="string", length=255, nullable=true)
     */
    private $wpplSiret;

    /**
     * @var string|null
     *
     * @ORM\Column(name="wppl_service_code", type="string", length=255, nullable=true)
     */
    private $wpplServiceCode;

    /**
     * @var int|null
     *
     * @ORM\Column(name="wppl_has_order", type="integer", nullable=true)
     */
    private $wpplHasOrder;

    /**
     * @var string|null
     *
     * @ORM\Column(name="wppl_order_number", type="string", length=11, nullable=true)
     */
    private $wpplOrderNumber;

    /**
     * @var int|null
     *
     * @ORM\Column(name="wppl_fichier", type="integer", nullable=true)
     */
    private $wpplFichier;

    /**
     * @var int|null
     *
     * @ORM\Column(name="wppl_wpp_id", type="integer", nullable=false)
     */
    private $wpplWpp;
    

    public function getWpplId(): ?int
    {
        return $this->wpplId;
    }

    public function getWpplStamp(): ?\DateTimeInterface
    {
        return $this->wpplStamp;
    }
    
    public function setWpplStamp(\DateTimeInterface $wpplStamp): self
    {
        $this->wpplStamp = $wpplStamp;
    
        return $this;
    }
    
    public function getWpplDeclar(): ?int
    {
        return $this->wpplDeclar;
    }
    
    public function setWpplDeclar(?int $wpplDeclar): self
    {
        $this->wpplDeclar = $wpplDeclar;
    
        return $this;
    }
    
    public function getWpplDossier(): ?int
    {
        return $this->wpplDossier;
    }
    
    public function setWpplDossier(?int $wpplDossier): self
    {
        $this->wpplDossier = $wpplDossier;
    
        return $this;
    }
    
    public function getWpplContrat(): ?int
    {
        return $this->wpplContrat;
    }
    
    public function setWpplContrat(?int $wpplContrat): self
    {
        $this->wpplContrat = $wpplContrat;
    
        return $this;
    }
    
    public function getWpplLabel(): ?string
    {
        return $this->wpplLabel;
    }

    public function setWpplLabel(?string $wpplLabel): self
    {
        $this->wpplLabel = $wpplLabel;

        return $this;
    }

    public function getWpplPeriode(): ?string
    {
        return $this->wpplPeriode;
    }

    public function setWpplPeriode(?string $wpplPeriode): self
    {
        $this->wpplPeriode = $wpplPeriode;

        return $this;
    }

    public function getWpplPeriodeNombre(): ?int
    {
        return $this->wpplPeriodeNombre;
    }
    
    public function setWpplPeriodeNombre(?int $wpplPeriodeNombre): self
    {
        $this->wpplPeriodeNombre = $wpplPeriodeNombre;
    
        return $this;
    }
    
    public function getWpplMoyenneRepro(): ?int
    {
        return $this->wpplMoyenneRepro;
    }
    
    public function setWpplMoyenneRepro(?int $wpplMoyenneRepro): self
    {
        $this->wpplMoyenneRepro = $wpplMoyenneRepro;
    
        return $this;
    }
    
    public function getWpplMoyenneExemplaire(): ?int
    {
        return $this->wpplMoyenneExemplaire;
    }
    
    public function setWpplMoyenneExemplaire(?int $wpplMoyenneExemplaire): self
    {
        $this->wpplMoyenneExemplaire = $wpplMoyenneExemplaire;
    
        return $this;
    }    
    
    public function getWpplTotal(): ?int
    {
        return $this->wpplTotal;
    }

    public function setWpplTotal(?int $wpplTotal): self
    {
        $this->wpplTotal = $wpplTotal;

        return $this;
    }

    public function getWpplIsChorusPro(): ?int
    {
        return $this->wpplIsChorusPro;
    }

    public function setWpplIsChorusPro(?int $wpplIsChorusPro): self
    {
        $this->wpplIsChorusPro = $wpplIsChorusPro;

        return $this;
    }

    public function getWpplSiret(): ?string
    {
        return $this->wpplSiret;
    }

    public function setWpplSiret(?string $wpplSiret): self
    {
        $this->wpplSiret = $wpplSiret;

        return $this;
    }

    public function getWpplServiceCode(): ?string
    {
        return $this->wpplServiceCode;
    }

    public function setWpplServiceCode(?string $wpplServiceCode): self
    {
        $this->wpplServiceCode = $wpplServiceCode;

        return $this;
    }

    public function getWpplHasOrder(): ?int
    {
        return $this->wpplHasOrder;
    }

    public function setWpplHasOrder(?int $wpplHasOrder): self
    {
        $this->wpplHasOrder = $wpplHasOrder;

        return $this;
    }

    public function getWpplOrderNumber(): ?string
    {
        return $this->wpplOrderNumber;
    }

    public function setWpplOrderNumber(?string $wpplOrderNumber): self
    {
        $this->wpplOrderNumber = $wpplOrderNumber;

        return $this;
    }

    public function getWpplFichier(): ?int
    {
        return $this->wpplFichier;
    }

    public function setWpplFichier(?int $wpplFichier): self
    {
        $this->wpplFichier = $wpplFichier;

        return $this;
    }

    public function getWpplWpp(): ?Wpanoramapress
    {
        return $this->wpplWpp;
    }

    public function setWpplWpp(?Wpanoramapress $wpplWpp): self
    {
        $this->wpplWpp = $wpplWpp;

        return $this;
    }


}
