<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Index;
use App\Admin\WcoconAdmin;
use Symfony\Component\Security\Core\User\UserInterface;
/**
 * @ORM\Entity(repositoryClass="App\Repository\WdeclarRepository")
 * @Table(name="wdeclar",indexes={@Index(name="uk_wdeclar", columns={"wde_contrat", "wde_annee"}),@Index(name="fk_wdeclar_cocon", columns={"wde_dossier"})})
 */
class Wdeclar implements UserInterface
{
 
    /**
     * @ORM\Column(type="datetime", nullable=false, options={"default": "CURRENT_TIMESTAMP"})
     * @ORM\Version
     */
    private $wde_stamp;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $wde_synchro;

    
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     */
    private $wde_declar;


    /**
     * @ORM\Column(type="integer")
     */
    private $wde_contrat;
    
    /**
     * @ORM\Column(type="string", length=8)
     */
    private $wde_password;
    

    /**
     * @ORM\Column(type="integer")
     */
    private $wde_annee;

    /**
     * @ORM\Column(type="string", length=8)
     */
    private $wde_numperiod;
    
    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wde_eleves;

    /**
     * @ORM\Column(type="string", length=3, nullable=true)
     */
    private $wde_tranche;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wde_copieurs;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wde_profs;

    /**
     * @ORM\Column(type="string", length=1)
     */
    private $wde_etat_declar;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $wde_date_validation;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $wde_email_sent;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $wde_nom_resp;

    /**
     * @ORM\Column(type="string", length=35, nullable=true)
     */
    private $wde_tit_resp;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $wde_fct_resp;

    /**
     * @ORM\Column(type="string", length=90, nullable=true)
     */
    private $wde_mel_resp;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wde_effectif;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wde_tranche01;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wde_tranche02;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wde_tranche03;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wde_tranche04;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wde_tranche05;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wde_tranche06;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wde_tranche07;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $wde_nom_sec_gen;

    /**
     * @ORM\Column(type="string", length=35, nullable=true)
     */
    private $wde_tit_sec_gen;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $wde_fct_sec_gen;

    /**
     * @ORM\Column(type="string", length=90, nullable=true)
     */
    private $wde_mel_sec_gen;
    
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $wde_intitule_decl;
    

    /**
     * @ORM\Column(type="string", length=9)
     */
    private $wde_annee_decl;
    

    

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wde_tranche08;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wde_tranche09;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wde_tranche10;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wde_tranche11;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wde_tranche12;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wde_tranche13;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wde_tranche14;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wde_tranche15;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wde_tranche16;
    
    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wde_tranche17;
    
    
    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wde_tranche18;
    
    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wde_tranche19;
    
    
    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wde_tranche20;
    
    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wde_tranche21;
    
    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wde_tranche22;
    


    /**
     * @ORM\Column(type="string", length=26, nullable=true)
     */
    private $wde_tel_resp;

    /**
     * @ORM\Column(type="string", length=26, nullable=true)
     */
    private $wde_fax_resp;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wde_effectif2;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wde_effectif3;
    
    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wde_effectif4;

    /**
     * @ORM\Column(type="smallint")
     */
    private $wde_is_chorus_pro;//nullable=true,

    /**
     * @ORM\Column(type="string", length=14)
     */
    private $wde_siret;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $wde_service_code;

    /**
     * @ORM\Column(type="smallint")
     */
    private $wde_is_order_number;
    
    /**
     * @ORM\Column(type="string", length=50)
     */
    private $wde_order_number;
    
    
    
    /**
    * @ORM\Column(type="integer")
    */
    private $wde_id_ct_fact;
    /**
    
    * @ORM\Column(type="integer")
    */
    private $wde_id_ct_nom_sec_gen;
    /**
    
    * @ORM\Column(type="integer")
    */
    private $wde_id_ct_resp;
    /**
    
    * @ORM\Column(type="integer")
    */
    private $wde_id_adr_fact;
    
    
    
    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $wde_raisoc1_fact;
    
    
    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $wde_nom_fact;
    
    /**
     * @ORM\Column(type="string", length=35, nullable=true)
     */
    private $wde_tit_fact;
    
    
    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $wde_fct_fact;    
    
    
    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $wde_adresse1_fact;
    
    
    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $wde_adresse2_fact;
    
    
    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $wde_adresse3_fact;
    
    /**
     * @ORM\Column(type="string", length=16, nullable=true)
     */
    private $wde_copos_fact;
    
    /**
     * @ORM\Column(type="string", length=42, nullable=true)
     */
    private $wde_ville_fact;
    
    
    /**
     * @ORM\Column(type="string", length=26, nullable=true)
     */
    private $wde_tel_fact;
    
    
    /**
     * @ORM\Column(type="string", length=26, nullable=true)
     */
    private $wde_mail_fact;    
    
    
    
    

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Wcocon", inversedBy="wdeclars")
     * @ORM\JoinColumn( name="wde_dossier", referencedColumnName="wco_dossier", nullable=true, onDelete="SET NULL"))
     */
    private $wde_dossier;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $wde_typcont;




    public function __toString()
    {
        return "Declaration ".$this->getWdeDeclar();
    }

    public function __construct()
    {
        //$this->wco_dossier = new Wtype();
        //$this->wdeclars = new ArrayCollection();
    }

    /*
     * Use this to know if the starting year date is after summer (scolare : 1 september), 
     * or the first day of the year (civile: 01 January)
     * the function return the name of the field to use in wtype.
     * Then the value of those fields are used in texte when the keyword [activation_date] is found.
     * */
    public function getActivationType()
    {
        
        if(strlen($this->wde_annee_declar)>4 ){
            return "activation_scolaire";
        }else{
            return "activation_civile";
        }
    }
    public function getWdeStamp(): ?\DateTimeInterface
    {
        return $this->wde_stamp;
    }

    public function setWdeStamp(\DateTimeInterface $wde_stamp): self
    {
        $this->wde_stamp = $wde_stamp;

        return $this;
    }

    public function getWdeSynchro(): ?\DateTimeInterface
    {
        return $this->wde_synchro;
    }

    public function setWdeSynchro(?\DateTimeInterface $wde_synchro): self
    {
        $this->wde_synchro = $wde_synchro;

        return $this;
    }

   

    

    public function getWdeContrat(): ?int
    {
        return $this->wde_contrat;
    }

    public function setWdeContrat(int $wde_contrat): self
    {
        $this->wde_contrat = $wde_contrat;

        return $this;
    }
    
    
    public function getWdePassword(): ?string
    {
        return $this->wde_password;
    }
    
    public function setWdePassword(string $wde_password): self
    {
        $this->wde_password = $wde_password;
    
        return $this;
    }

    public function getWdeAnnee(): ?int
    {
        return $this->wde_annee;
    }

    public function setWdeAnnee(int $wde_annee): self
    {
        $this->wde_annee = $wde_annee;

        return $this;
    }

    public function getWdeNumperiod(): ?string
    {
        return $this->wde_numperiod;
    }
    
    public function setWdeNumperiod(string $wde_numperiod): self
    {
        $this->wde_numperiod = $wde_numperiod;
    
        return $this;
    }
    

    public function getWdeEleves(): ?int
    {
        return $this->wde_eleves;
    }

    public function setWdeEleves(?int $wde_eleves): self
    {
        $this->wde_eleves = $wde_eleves;

        return $this;
    }

    public function getWdeTranche(): ?string
    {
        return $this->wde_tranche;
    }

    public function setWdeTranche(?string $wde_tranche): self
    {
        $this->wde_tranche = $wde_tranche;

        return $this;
    }

    public function getWdeCopieurs(): ?int
    {
        return $this->wde_copieurs;
    }

    public function setWdeCopieurs(?int $wde_copieurs): self
    {
        $this->wde_copieurs = $wde_copieurs;

        return $this;
    }

    public function getWdeProfs(): ?int
    {
        return $this->wde_profs;
    }

    public function setWdeProfs(?int $wde_profs): self
    {
        $this->wde_profs = $wde_profs;

        return $this;
    }

    public function getWdeEtatDeclar(): ?string
    {
        return $this->wde_etat_declar;
    }

    public function setWdeEtatDeclar(string $wde_etat_declar): self
    {
        $this->wde_etat_declar = $wde_etat_declar;

        return $this;
    }

    public function getWdeDateValidation(): ?\DateTimeInterface
    {
        return $this->wde_date_validation;
    }

    public function setWdeDateValidation(?\DateTimeInterface $wde_date_validation): self
    {
        $this->wde_date_validation = $wde_date_validation;

        return $this;
    }

    public function getWdeEmailSent(): ?int
    {
        return $this->wde_email_sent;
    }

    public function setWdeEmailSent(?int $wde_email_sent): self
    {
        $this->wde_email_sent = $wde_email_sent;

        return $this;
    }

    public function getWdeNomResp(): ?string
    {
        return $this->wde_nom_resp;
    }

    public function setWdeNomResp(?string $wde_nom_resp): self
    {
        $this->wde_nom_resp = $wde_nom_resp;

        return $this;
    }

    public function getWdeTitResp(): ?string
    {
        return $this->wde_tit_resp;
    }

    public function setWdeTitResp(?string $wde_tit_resp): self
    {
        $this->wde_tit_resp = $wde_tit_resp;

        return $this;
    }

    public function getWdeFctResp(): ?string
    {
        return $this->wde_fct_resp;
    }

    public function setWdeFctResp(?string $wde_fct_resp): self
    {
        $this->wde_fct_resp = $wde_fct_resp;

        return $this;
    }

    public function getWdeMelResp(): ?string
    {
        return $this->wde_mel_resp;
    }

    public function setWdeMelResp(?string $wde_mel_resp): self
    {
        $this->wde_mel_resp = $wde_mel_resp;

        return $this;
    }

    public function getWdeEffectif(): ?int
    {
        return $this->wde_effectif;
    }

    public function setWdeEffectif(?int $wde_effectif): self
    {
        $this->wde_effectif = $wde_effectif;

        return $this;
    }

    public function getWdeTranche01(): ?int
    {
        return $this->wde_tranche01;
    }

    public function setWdeTranche01(?int $wde_tranche01): self
    {
        $this->wde_tranche01 = $wde_tranche01;

        return $this;
    }

    public function getWdeTranche02(): ?int
    {
        return $this->wde_tranche02;
    }

    public function setWdeTranche02(?int $wde_tranche02): self
    {
        $this->wde_tranche02 = $wde_tranche02;

        return $this;
    }

    public function getWdeTranche03(): ?int
    {
        return $this->wde_tranche03;
    }

    public function setWdeTranche03(?int $wde_tranche03): self
    {
        $this->wde_tranche03 = $wde_tranche03;

        return $this;
    }

    public function getWdeTranche04(): ?int
    {
        return $this->wde_tranche04;
    }

    public function setWdeTranche04(?int $wde_tranche04): self
    {
        $this->wde_tranche04 = $wde_tranche04;

        return $this;
    }

    public function getWdeTranche05(): ?int
    {
        return $this->wde_tranche05;
    }

    public function setWdeTranche05(?int $wde_tranche05): self
    {
        $this->wde_tranche05 = $wde_tranche05;

        return $this;
    }

    public function getWdeTranche06(): ?int
    {
        return $this->wde_tranche06;
    }

    public function setWdeTranche06(?int $wde_tranche06): self
    {
        $this->wde_tranche06 = $wde_tranche06;

        return $this;
    }

    public function getWdeTranche07(): ?int
    {
        return $this->wde_tranche07;
    }

    public function setWdeTranche07(?int $wde_tranche07): self
    {
        $this->wde_tranche07 = $wde_tranche07;

        return $this;
    }

    public function getWdeNomSecGen(): ?string
    {
        return $this->wde_nom_sec_gen;
    }

    public function setWdeNomSecGen(?string $wde_nom_sec_gen): self
    {
        $this->wde_nom_sec_gen = $wde_nom_sec_gen;

        return $this;
    }

    public function getWdeTitSecGen(): ?string
    {
        return $this->wde_tit_sec_gen;
    }

    public function setWdeTitSecGen(?string $wde_tit_sec_gen): self
    {
        $this->wde_tit_sec_gen = $wde_tit_sec_gen;

        return $this;
    }

    public function getWdeFctSecGen(): ?string
    {
        return $this->wde_fct_sec_gen;
    }

    public function setWdeFctSecGen(?string $wde_fct_sec_gen): self
    {
        $this->wde_fct_sec_gen = $wde_fct_sec_gen;

        return $this;
    }

    public function getWdeMelSecGen(): ?string
    {
        return $this->wde_mel_sec_gen;
    }

    public function setWdeMelSecGen(?string $wde_mel_sec_gen): self
    {
        $this->wde_mel_sec_gen = $wde_mel_sec_gen;

        return $this;
    }

    public function getWdeAnneeDecl(): ?string
    {
        return $this->wde_annee_decl;
    }
    
    public function setWdeIntituleDecl(string $wde_intitule_decl): self
    {
        $this->wde_intitule_decl = $wde_intitule_decl;
    
        return $this;
    }
    
    
    
    public function getWdeIntituleDecl(): ?string
    {
        return $this->wde_intitule_decl;
    }

    public function setWdeAnneeDecl(string $wde_annee_decl): self
    {
        $this->wde_annee_decl = $wde_annee_decl;

        return $this;
    }

    public function getWdeTranche08(): ?int
    {
        return $this->wde_tranche08;
    }

    public function setWdeTranche08(?int $wde_tranche08): self
    {
        $this->wde_tranche08 = $wde_tranche08;

        return $this;
    }

    public function getWdeTranche09(): ?int
    {
        return $this->wde_tranche09;
    }

    public function setWdeTranche09(?int $wde_tranche09): self
    {
        $this->wde_tranche09 = $wde_tranche09;

        return $this;
    }

    public function getWdeTranche10(): ?int
    {
        return $this->wde_tranche10;
    }

    public function setWdeTranche10(?int $wde_tranche10): self
    {
        $this->wde_tranche10 = $wde_tranche10;

        return $this;
    }

    public function getWdeTranche11(): ?int
    {
        return $this->wde_tranche11;
    }

    public function setWdeTranche11(?int $wde_tranche11): self
    {
        $this->wde_tranche11 = $wde_tranche11;

        return $this;
    }

    public function getWdeTranche12(): ?int
    {
        return $this->wde_tranche12;
    }

    public function setWdeTranche12(?int $wde_tranche12): self
    {
        $this->wde_tranche12 = $wde_tranche12;

        return $this;
    }

    public function getWdeTranche13(): ?int
    {
        return $this->wde_tranche13;
    }

    public function setWdeTranche13(?int $wde_tranche13): self
    {
        $this->wde_tranche13 = $wde_tranche13;

        return $this;
    }

    public function getWdeTranche14(): ?int
    {
        return $this->wde_tranche14;
    }

    public function setWdeTranche14(?int $wde_tranche14): self
    {
        $this->wde_tranche14 = $wde_tranche14;

        return $this;
    }

    public function getWdeTranche15(): ?int
    {
        return $this->wde_tranche15;
    }

    public function setWdeTranche15(?int $wde_tranche15): self
    {
        $this->wde_tranche15 = $wde_tranche15;

        return $this;
    }

    public function getWdeTranche16(): ?int
    {
        return $this->wde_tranche16;
    }

    public function setWdeTranche16(?int $wde_tranche16): self
    {
        $this->wde_tranche16 = $wde_tranche16;

        return $this;
    }
    
    
    public function getWdeTranche17(): ?int
    {
        return $this->wde_tranche17;
    }
    
    public function setWdeTranche17(?int $wde_tranche17): self
    {
        $this->wde_tranche17 = $wde_tranche17;
    
        return $this;
    }
    
    
    public function getWdeTranche18(): ?int
    {
        return $this->wde_tranche18;
    }
    
    public function setWdeTranche18(?int $wde_tranche18): self
    {
        $this->wde_tranche18 = $wde_tranche18;
    
        return $this;
    }    
    
    public function getWdeTranche19(): ?int
    {
        return $this->wde_tranche19;
    }
    
    public function setWdeTranche19(?int $wde_tranche19): self
    {
        $this->wde_tranche19 = $wde_tranche19;
    
        return $this;
    }    
    
    
    public function getWdeTranche20(): ?int
    {
        return $this->wde_tranche20;
    }
    
    public function setWdeTranche20(?int $wde_tranche20): self
    {
        $this->wde_tranche20 = $wde_tranche20;
    
        return $this;
    }    
    
    
    public function getWdeTranche21(): ?int
    {
        return $this->wde_tranche21;
    }
    
    public function setWdeTranche21(?int $wde_tranche21): self
    {
        $this->wde_tranche21 = $wde_tranche21;
    
        return $this;
    }   
    
    
    
    
    public function getWdeTranche22(): ?int
    {
        return $this->wde_tranche22;
    }
    
    public function setWdeTranche22(?int $wde_tranche22): self
    {
        $this->wde_tranche22 = $wde_tranche22;
    
        return $this;
    }   
    
    

    public function getWdeTelResp(): ?string
    {
        return $this->wde_tel_resp;
    }

    public function setWdeTelResp(?string $wde_tel_resp): self
    {
        $this->wde_tel_resp = $wde_tel_resp;

        return $this;
    }

    public function getWdeFaxResp(): ?string
    {
        return $this->wde_fax_resp;
    }

    public function setWdeFaxResp(?string $wde_fax_resp): self
    {
        $this->wde_fax_resp = $wde_fax_resp;

        return $this;
    }

    public function getWdeEffectif2(): ?int
    {
        return $this->wde_effectif2;
    }

    public function setWdeEffectif2(?int $wde_effectif2): self
    {
        $this->wde_effectif2 = $wde_effectif2;

        return $this;
    }

    public function getWdeEffectif3(): ?int
    {
        return $this->wde_effectif3;
    }

    public function setWdeEffectif3(?int $wde_effectif3): self
    {
        $this->wde_effectif3 = $wde_effectif3;

        return $this;
    }
    
    public function getWdeEffectif4(): ?int
    {
        return $this->wde_effectif4;
    }
    
    public function setWdeEffectif4(?int $wde_effectif4): self
    {
        $this->wde_effectif4 = $wde_effectif4;
    
        return $this;
    }
    

    public function getWdeIsChorusPro(): ?int
    {
        return $this->wde_is_chorus_pro;
    }

    public function setWdeIsChorusPro(int $wde_is_chorus_pro): self
    {
        $this->wde_is_chorus_pro = $wde_is_chorus_pro;

        return $this;
    }

    public function getWdeSiret(): ?string
    {
        return $this->wde_siret;
    }

    public function setWdeSiret(string $wde_siret): self
    {
        $this->wde_siret = $wde_siret;

        return $this;
    }

    public function getWdeServiceCode(): ?string
    {
        return $this->wde_service_code;
    }

    public function setWdeServiceCode(string $wde_service_code): self
    {
        $this->wde_service_code = $wde_service_code;

        return $this;
    }


    public function getWdeIsServiceCode(): ?int
    {
        return $this->wde_is_order_number;
    }
    
    public function setWdeIsServiceCode(int $wde_is_order_number): self
    {
        $this->wde_is_order_number = $wde_is_order_number;
    
        return $this;
    }
    
    
    public function getWdeOrderNumber(): ?string
    {
        return $this->wde_order_number;
    }

    public function setWdeOrderNumber(string $wde_order_number): self
    {
        $this->wde_order_number = $wde_order_number;

        return $this;
    }

    public function getWdeDeclar(): ?string
    {
        return $this->wde_declar;
    }

    public function setWdeDeclar(string $wde_declar): self
    {
        $this->wde_declar = $wde_declar;

        return $this;
    }

    public function getWdeDossier(): ?Wcocon
    {
        return $this->wde_dossier;
    }

    public function setWdeDossier(?Wcocon $wde_dossier): self
    {
        $this->wde_dossier = $wde_dossier;

        return $this;
    }

    public function getWdeTypcont(): ?string
    {
        return $this->wde_typcont;
    }

    public function setWdeTypcont(?string $wde_typcont): self
    {
        $this->wde_typcont = $wde_typcont;

        return $this;
    }
    
    
    public function getWdeIdCtFact(): ?string
    {
        return $this->wde_id_ct_fact;
    }
    
    public function setWdeIdCtFact(?string $wde_id_ct_fact): self
    {
        $this->wde_id_ct_fact = $wde_id_ct_fact;
        return $this;
    }
    
    public function getWdeIdCtNomSecGen(): ?string
    {
        return $this->wde_id_ct_nom_sec_gen;
    }
    
    public function setWdeIdCtNomSecGen(?string $wde_id_ct_nom_sec_gen): self
    {
        $this->wde_id_ct_nom_sec_gen = $wde_id_ct_nom_sec_gen;
        return $this;
    }
    
    public function getWdeIdCtResp(): ?string
    {
        return $this->wde_id_ct_resp;
    }
    
    public function setWdeIdCtResp(?string $wde_id_ct_resp): self
    {
        $this->wde_id_ct_resp = $wde_id_ct_resp;
        return $this;
    }
    
    
    
    public function getWdeIdAdrFact(): ?string
    {
        return $this->wde_id_adr_fact;
    }
    
    public function setWdeIdAdrFact(?string $wde_id_adr_fact): self
    {
        $this->wde_id_adr_fact = $wde_id_adr_fact;
        return $this;
    }

    public function getWdeRaisoc1Fact(): ?string
    {
        return $this->wde_raisoc1_fact;
    }
    
    public function setWdeRaisoc1Fact(?string $wde_raisoc1_fact): self
    {
        $this->wde_raisoc1_fact = $wde_raisoc1_fact;
        return $this;
    }
    
    public function getWdeNomFact(): ?string
    {
        return $this->wde_nom_fact;
    }
    
    public function setWdeNomFact(?string $wde_nom_fact): self
    {
        $this->wde_nom_fact = $wde_nom_fact;
        return $this;
    }
    
    
    public function getWdeTitFact(): ?string
    {
        return $this->wde_tit_fact;
    }
    
    public function setWdeTitFact(?string $wde_tit_fact): self
    {
        $this->wde_tit_fact = $wde_tit_fact;
        return $this;
    }    
    
    

    public function getWdeFctFact(): ?string
    {
        return $this->wde_fct_fact;
    }
    
    public function setWdeFctFact(?string $wde_fct_fact): self
    {
        $this->wde_tit_fact = $wde_fct_fact;
        return $this;
    }   
    
    
    public function getWdeAdresse1Fact(): ?string
    {
        return $this->wde_adresse1_fact;
    }
    
    public function setWdeAdresse1Fact(?string $wde_adresse1_fact): self
    {
        $this->wde_adresse1_fact = $wde_adresse1_fact;
        return $this;
    }    
    
    
    public function getWdeAdresse2Fact(): ?string
    {
        return $this->wde_adresse2_fact;
    }
    
    public function setWdeAdresse2Fact(?string $wde_adresse1_fact): self
    {
        $this->wde_adresse2_fact = $wde_adresse2_fact;
        return $this;
    }   
    
    
    public function getWdeAdresse3Fact(): ?string
    {
        return $this->wde_adresse3_fact;
    }
    
    public function setWdeAdresse3Fact(?string $wde_adresse3_fact): self
    {
        $this->wde_adresse3_fact = $wde_adresse3_fact;
        return $this;
    }   
    
    
    public function getWdeCoposFact(): ?string
    {
        return $this->wde_copos_fact;
    }
    
    public function setWdeCoposFact(?string $wde_copos_fact): self
    {
        $this->wde_copos_fact = $wde_copos_fact;
        return $this;
    }
        
    
    public function getWdeVilleFact(): ?string
    {
        return $this->wde_ville_fact;
    }
    
    public function setWdeVilleFact(?string $wde_ville_fact): self
    {
        $this->wde_ville_fact = $wde_ville_fact;
        return $this;
    }
        
    public function getWdeTelFact(): ?string
    {
        return $this->wde_tel_fact;
    }
    
    public function setWdeTelFact(?string $wde_tel_fact): self
    {
        $this->wde_tel_fact = $wde_tel_fact;
        return $this;
        
    }
    
    public function getWdeMailFact(): ?string
    {
        return $this->wde_mail_fact;
    }
    
    public function setWdeMailFact(?string $wde_mail_fact): self
    {
        $this->wde_mail_fact = $wde_mail_fact;
        return $this;
    
    }  
    
    /*Authentication méthods implements interface User*/
    public function getRoles(){
        return ['ROLE_USER'];
    }
    
    
    public function getPassword(){
        return $this->getWdePassword();
    }
    
    public function getSalt() {
        return false;
    }
    
    
    public function getUsername(){
        return $this->getWdeDossier()->getWcoDossier();
    }
    
    
    public function eraseCredentials(){
    
    }
    
    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt,
        ));
    }
    
    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt
            ) = unserialize($serialized);
    }
    
    
}
