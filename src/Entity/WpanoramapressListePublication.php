<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * WpanoramapressListePublication
 *
 * @ORM\Table(name="wpanoramapress_liste_publication", indexes={@ORM\Index(name="fk_wpanorama_liste_publication_wpanorama_liste1_idx", columns={"wpplp_wppl_id"})})
 * @ORM\Entity
 */
class WpanoramapressListePublication
{
    /**
     * @var int
     *
     * @ORM\Column(name="wpplp_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $wpplpId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="wpplp_label", type="string", length=255, nullable=true)
     */
    private $wpplpLabel;

    /**
     * @var int|null
     *
     * @ORM\Column(name="wpplp_total", type="integer", nullable=true)
     */
    private $wpplpTotal;

    /**
     * @var \WpanoramapressListe
     *
     * @ORM\ManyToOne(targetEntity="WpanoramapressListe")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="wpplp_wppl_id", referencedColumnName="wppl_id")
     * })
     */
    private $wpplpWppl;


}
