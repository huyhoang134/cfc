<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RepertoirePrestataire
 *
 * @ORM\Table(name="repertoire_prestataire")
 * @ORM\Entity
 */
class RepertoirePrestataire
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=255, nullable=false)
     */
    private $label;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function __toString()
    {
        return $this->getLabel();
    }
    
    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }


}
