<?php

namespace App\Entity;

use App\Repository\WdocumentRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=WdocumentRepository::class)
 */
class Wdocument
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(name="PkIndexDocument", type="integer", nullable=false)
     */
    private $pkIndexDocument;

    /**
     * @ORM\Column(name="FkDocument", type="integer")
     */
    private $fkDocument;

    /**
     * @ORM\Column(name="NomFichier", type="string", length=200, nullable=true)
     */
    private $nomFichier;

    /**
     * @ORM\Column(name="Titre", type="string", length=200, nullable=true)
     */
    private $titre;

    /**
     * @ORM\Column(name="MotsClefs", type="string", length=200, nullable=true)
     */
    private $motsClefs;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    private $docSource;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private $docDossierFtp;

    /**
     * @ORM\Column(type="datetime")
     */
    private $docStamp;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $docSynchro;

    public function getPkIndexDocument(): ?int
    {
        return $this->pkIndexDocument;
    }

    public function getFkDocument(): ?int
    {
        return $this->fkDocument;
    }

    public function setFkDocument(int $fkDocument): self
    {
        $this->fkDocument = $fkDocument;

        return $this;
    }

    public function getNomFichier(): ?string
    {
        return $this->nomFichier;
    }

    public function setNomFichier(?string $nomFichier): self
    {
        $this->nomFichier = $nomFichier;

        return $this;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(?string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getMotsClefs(): ?string
    {
        return $this->motsClefs;
    }

    public function setMotsClefs(?string $motsClefs): self
    {
        $this->motsClefs = $motsClefs;

        return $this;
    }

    public function getDocSource(): ?string
    {
        return $this->docSource;
    }

    public function setDocSource(?string $docSource): self
    {
        $this->docSource = $docSource;

        return $this;
    }

    public function getDocDossierFtp(): ?string
    {
        return $this->docDossierFtp;
    }

    public function setDocDossierFtp(?string $docDossierFtp): self
    {
        $this->docDossierFtp = $docDossierFtp;

        return $this;
    }

    public function getDocStamp(): ?\DateTimeInterface
    {
        return $this->docStamp;
    }

    public function setDocStamp(\DateTimeInterface $docStamp): self
    {
        $this->docStamp = $docStamp;

        return $this;
    }

    public function getDocSynchro(): ?\DateTimeInterface
    {
        return $this->docSynchro;
    }

    public function setDocSynchro(?\DateTimeInterface $docSynchro): self
    {
        $this->docSynchro = $docSynchro;

        return $this;
    }
}
