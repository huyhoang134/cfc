<?php

namespace App\Security;

use App\Entity\Wdeclar;
use App\Entity\Wcocon;
use App\Services\CustomerService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Security\Guard\Authenticator\AbstractFormLoginAuthenticator;
use Symfony\Component\Security\Guard\PasswordAuthenticatedInterface;
use Symfony\Component\Security\Http\Util\TargetPathTrait;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class LoginAuthenticator extends AbstractFormLoginAuthenticator implements PasswordAuthenticatedInterface
{
    use TargetPathTrait;

    private const LOGIN_ROUTE = 'index';

    private $entityManager;
    private $urlGenerator;
    private $csrfTokenManager;
    private $passwordEncoder;
    /**
     * @var CustomerService
     */
    private $customerService;
    /**
     * @var mixed
     */
    private $appKey;
    /**
     * @var mixed
     */
    private $appSecret;
    /**
     * @var mixed
     */
    private $ssoServerAddress;
    /**
     * @var mixed
     */
    private $ssoServerCacheCheck;
    
    private $authorizationChecker;

    public function __construct(
        EntityManagerInterface $entityManager,
        UrlGeneratorInterface $urlGenerator,
        CsrfTokenManagerInterface $csrfTokenManager,
        UserPasswordEncoderInterface $passwordEncoder,
        ManagerRegistry $registry,
        TokenStorageInterface $tokenStorage,
        CustomerService $customerService,
        ParameterBagInterface $params,
        AuthenticationManagerInterface $authManager,
        AuthorizationCheckerInterface $authorizationChecker,
        RequestStack $requestStack
        
    ) {
        $this->entityManager       = $entityManager;
        $this->urlGenerator        = $urlGenerator;
        $this->csrfTokenManager    = $csrfTokenManager;
        $this->passwordEncoder     = $passwordEncoder;
        $this->customerService     = $customerService;
        $this->appKey              = $params->get('webapp_key');
        $this->appSecret           = $params->get('webapp_secret');
        $this->ssoServerAddress    = $params->get('sso_server_address');
        $this->ssoServerCacheCheck = $params->get('sso_server_cache_check');
        $this->authManager     = $authManager;
        $this->authorizationChecker = $authorizationChecker;
        $this->request         = $requestStack->getCurrentRequest();
        $this->tokenStorage    = $tokenStorage;
    }
    
    
    public function authenticate(Request $request): PassportInterface
    {
        
        $credentials = [
            '_username'  => $request->request->get('_username'),
            '_password'  => $request->request->get('_password'),
            'csrf_token' => $request->request->get('_csrf_token'),
        ];

    }
    
    
    

    public function supports(Request $request)
    {

        return self::LOGIN_ROUTE === $request->attributes->get('_route')
               && $request->isMethod('POST');
    }

    public function getCredentials(Request $request)
    {

        //$auth_checker = $this->get('security.authorization_checker');
        //$isRoleUser = $this->authorizationChecker->isGranted('ROLE_USER');


        $credentials = [
            '_username'  => $request->request->get('_username'),
            '_password'  => $request->request->get('_password'),
            'csrf_token' => $request->request->get('_csrf_token'),
        ];
        $request->getSession()->set(
            Security::LAST_USERNAME,
            $credentials['_username']
        );
        $request->getSession()->set(
            'sso_token',
            $this->customerService->encryptParameter(md5($credentials['_username'].$credentials['_password']))
            );
       
        return $credentials;
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        
        
        $token = new CsrfToken('authenticate', $credentials['csrf_token']);
        
        if ( ! $this->csrfTokenManager->isTokenValid($token)) {
            throw new InvalidCsrfTokenException();
        }
        

       
        /** @var Response $loginCheck */
        $loginCheck = $this->customerService->loginCheckAction($credentials);
        $response   = $loginCheck->getContent();

        if (strpos($response, 'Username or password don')) {
            throw new UsernameNotFoundException(
                sprintf('Username "%s" does not exist.', '$username')
            );
        }

        $response = json_decode($response, true);

        return $this->customerService->syncUser($response['name']);
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        return true;
        return !empty($user)?true:false;
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        
        $postAppKeyParam      = 'appKey=' . $this->customerService->encryptParameter($this->appKey);
        $postAppSecretParam   = '&appSecret=' . $this->customerService->encryptParameter($this->appSecret);
        $postUsernameParam    = '&username=' .  $request->getSession()->get('sso_token');
        $postUrlBackLinkParam = '&urlBackLink=' . $this->customerService->encryptParameter($this->urlGenerator->generate('index',
                [], UrlGeneratorInterface::ABSOLUTE_URL));

        return new RedirectResponse($this->ssoServerCacheCheck . '?' . $postAppKeyParam . $postAppSecretParam . $postUsernameParam . $postUrlBackLinkParam,
            Response::HTTP_MOVED_PERMANENTLY);
    }

    protected function getLoginUrl()
    {
        return $this->urlGenerator->generate(self::LOGIN_ROUTE);
    }

    public function getPassword($credentials): ?string
    {
        return $credentials['_password'];
    }

    public function supportsRememberMe(): bool
    {
        return true;
    }
}
