<?php

namespace App\Security;

use App\Services\CustomerService;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Http\HttpUtils;
use Symfony\Component\Security\Http\Logout\DefaultLogoutSuccessHandler;

class CustomLogoutSuccessHandler extends DefaultLogoutSuccessHandler
{
    /**
     * @var CustomerService
     */
    private $customerService;
    /**
     * @var UrlGeneratorInterface
     */
    private $urlGenerator;
    /**
     * @var ParameterBagInterface
     */
    private $params;
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    public function __construct(
        HttpUtils $httpUtils,
        string $targetUrl = '/',
        CustomerService $customerService,
        UrlGeneratorInterface $urlGenerator,
        ParameterBagInterface $params,
        TokenStorageInterface $tokenStorage
    ) {
        parent::__construct($httpUtils, $targetUrl);

        $this->customerService = $customerService;
        $this->urlGenerator    = $urlGenerator;
        $this->params    = $params;
        $this->tokenStorage    = $tokenStorage;
    }

    public function onLogoutSuccess(Request $request)
    {
        $logoutChecked = $request->get('logoutChecked', null);

        if (!$logoutChecked) {
            $postUrlBackLinkParam = 'urlBackLink=' . $this->customerService->encryptParameter($this->urlGenerator->generate('security_logout',
                    [], UrlGeneratorInterface::ABSOLUTE_URL));

            return new RedirectResponse($this->params->get('sso_server_remove_cache') . '?' . $postUrlBackLinkParam,
                Response::HTTP_MOVED_PERMANENTLY);
        }

        $this->tokenStorage->setToken(null);
        $request->getSession()->invalidate();
        
        return $this->httpUtils->createRedirectResponse($request, $this->targetUrl);
    }
}
